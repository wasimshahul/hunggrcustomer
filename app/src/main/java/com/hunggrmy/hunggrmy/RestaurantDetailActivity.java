package com.hunggrmy.hunggrmy;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.hunggrmy.hunggrmy.fragments.RestaurantCouponFragment;
import com.hunggrmy.hunggrmy.fragments.RestaurantInfoFragment;
import com.hunggrmy.hunggrmy.fragments.RestaurantMenuFragment;
import com.hunggrmy.hunggrmy.fragments.RestaurantOffersFragment;
import com.hunggrmy.hunggrmy.fragments.RestaurantReviewFragment;
import com.hunggrmy.hunggrmy.models.RestaurantBasicDetailsModel;
import com.hunggrmy.hunggrmy.utils.Constant;
import com.hunggrmy.hunggrmy.utils.FirebaseFunctions;
import com.hunggrmy.hunggrmy.utils.Utility;

import java.util.ArrayList;
import java.util.List;

public class RestaurantDetailActivity extends AppCompatActivity {

    private static final String TAG = "RestaurantDetail";
    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    public String restaurantId;
    public TextView restaurantname, ratingTitle;
    public ImageView bookingimg, backImg;
    public String setselection = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restaurant_detail);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);



        try {
            restaurantId = getIntent().getStringExtra("restaurantId");
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            setselection = getIntent().getStringExtra("setselection");
        } catch (Exception e) {
            setselection = "";
            e.printStackTrace();
        }


        restaurantname = (TextView) findViewById(R.id.restaurantname);
        ratingTitle = (TextView) findViewById(R.id.ratingTitle);
        bookingimg = (ImageView) findViewById(R.id.bookingimg);
        backImg = (ImageView) findViewById(R.id.backImg);
        bookingimg.setVisibility(View.GONE);

        try {
            FirebaseFunctions.restaurant.child(restaurantId).child("isBookingAvailable").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    boolean isBookingAvailable = false;
                    try {
                        isBookingAvailable = dataSnapshot.getValue(Boolean.class);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if(isBookingAvailable){
                        bookingimg.setVisibility(View.VISIBLE);
                    } else {
                        bookingimg.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            bookingimg.setVisibility(View.VISIBLE);
        }

        bookingimg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                showCartAlertDialog("Asd", "asdas");
                Intent intent = new Intent(RestaurantDetailActivity.this, BookTableActivity.class);
                intent.putExtra("restaurantId", restaurantId);
                startActivity(intent);
            }
        });
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        try {
            setupViewPager(viewPager);
        } catch (Exception e) {
            e.printStackTrace();
        }

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        try {
            if(setselection.equals("offers")){
                TabLayout.Tab tab = tabLayout.getTabAt(2);
                tab.select();
            } else {
                TabLayout.Tab tab = tabLayout.getTabAt(1);
                tab.select();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            getRestaurantDetails();
            getRestaurantRating(getRestauranntId());
        } catch (Exception e) {
            e.printStackTrace();
        }

        backImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    public void getRestaurantRating(String restaurantIdNo){
        //For Getting ratings of a restaurant
        try {
            FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseRestaurantNode).child(restaurantIdNo).child(Constant.rating).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    try {
                        ArrayList<Float> ratinngList = new ArrayList<>();
                        for (DataSnapshot ratingDataSnapShot :
                                dataSnapshot.getChildren()) {
                            ratinngList.add(ratingDataSnapShot.getValue(Float.class));
                        }
                        if (ratinngList.isEmpty()) {
                            ratingTitle.setVisibility(View.INVISIBLE);
                        } else {
                            ratingTitle.setVisibility(View.VISIBLE);
                            float totRating = 0;
                            for (Float rating :
                                    ratinngList) {
                                totRating = rating + totRating;
                            }
                            ratingTitle.setText(  String.format("%.2f", totRating / ratinngList.size() + "") );
                        }
                    } catch (Exception e) {

                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getRestaurantDetails() {
        try {
            FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseRestaurantNode).child(restaurantId).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    String restaurantName = null;
                    try {
                        RestaurantBasicDetailsModel restaurantBasicDetailsModel = dataSnapshot.getValue(RestaurantBasicDetailsModel.class);
                        assert restaurantBasicDetailsModel != null;
                        toolbar.setTitle(restaurantBasicDetailsModel.restaurantName);
                        restaurantname.setText(restaurantBasicDetailsModel.restaurantName);
                        if(restaurantBasicDetailsModel.cuisines.equals("") || restaurantBasicDetailsModel.cuisines.isEmpty()){
                        } else {
//                            Utility.showDialog(RestaurantDetailActivity.this, "Cuisine(s) available in this restaurant \n\n" + restaurantBasicDetailsModel.cuisines);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new RestaurantMenuFragment(), "MENU");
        adapter.addFragment(new RestaurantInfoFragment(), "INFO");
        adapter.addFragment(new RestaurantOffersFragment(), "OFFERS");
        adapter.addFragment(new RestaurantCouponFragment(), "COUPONS");
        adapter.addFragment(new RestaurantReviewFragment(), "REVIEWS");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    public String getRestauranntId() {
//        return "-LCsJ1I9NB1CcAplncO8";
        return restaurantId;
    }


}
