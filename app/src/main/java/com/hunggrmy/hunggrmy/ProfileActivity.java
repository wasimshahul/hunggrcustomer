package com.hunggrmy.hunggrmy;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.UploadTask.TaskSnapshot;
import com.vlk.multimager.activities.GalleryActivity;
import com.vlk.multimager.utils.Constants;
import com.vlk.multimager.utils.Image;
import com.vlk.multimager.utils.Params;
import com.hunggrmy.hunggrmy.models.UserModel;
import com.hunggrmy.hunggrmy.utils.Constant;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;

public class ProfileActivity extends AppCompatActivity {
    boolean isUploadingImage = false;
    SharedPreferences pref;
    LinearLayout submit_btn;
    String userId = "";
    String userImage = "";
    EditText user_email_input;
    ImageView user_img_input;
    EditText user_name_input;
    EditText user_phone_input;
    EditText user_pwd_input;
    CheckBox user_supreme_input;

    class C04271 implements OnClickListener {
        C04271() {
        }

        public void onClick(View v) {
            Intent intent = new Intent(ProfileActivity.this, GalleryActivity.class);
            Params params = new Params();
            params.setCaptureLimit(1);
            params.setPickerLimit(1);
            params.setToolbarColor(0);
            params.setActionButtonColor(0);
            params.setButtonTextColor(ViewCompat.MEASURED_SIZE_MASK);
            intent.putExtra(Constants.KEY_PARAMS, params);
            ProfileActivity.this.startActivityForResult(intent, 3);
        }
    }

    class C04282 implements OnClickListener {
        C04282() {
        }

        public void onClick(View v) {
            if (!ProfileActivity.this.isUploadingImage) {
                ProfileActivity.this.createUser(ProfileActivity.this.user_name_input.getText().toString(), ProfileActivity.this.user_pwd_input.getText().toString(), ProfileActivity.this.user_phone_input.getText().toString(), ProfileActivity.this.user_email_input.getText().toString(), ProfileActivity.this.user_supreme_input.isChecked());
            }
        }
    }

    class C05143 implements ValueEventListener {
        C05143() {
        }

        public void onDataChange(DataSnapshot dataSnapshot) {
            UserModel userModel = (UserModel) dataSnapshot.getValue(UserModel.class);
            Glide.with(ProfileActivity.this).load(userModel.userImage).into(ProfileActivity.this.user_img_input);
            ProfileActivity.this.user_name_input.setText(userModel.userName);
            ProfileActivity.this.user_pwd_input.setText(userModel.userPwd);
            ProfileActivity.this.user_phone_input.setText(userModel.userPhone);
            ProfileActivity.this.user_email_input.setText(userModel.userEmail);
        }

        public void onCancelled(DatabaseError databaseError) {
        }
    }

    class C05176 implements OnFailureListener {
        C05176() {
        }

        public void onFailure(@NonNull Exception exception) {
            ProfileActivity.this.isUploadingImage = false;
        }
    }

    class C05187 implements OnSuccessListener<TaskSnapshot> {
        C05187() {
        }

        public void onSuccess(TaskSnapshot taskSnapshot) {
            Uri downloadUrl = taskSnapshot.getDownloadUrl();
            ProfileActivity.this.userImage = downloadUrl + "";
            ProfileActivity.this.isUploadingImage = false;
            Toast.makeText(ProfileActivity.this, "Image Uploaded Successfully", Toast.LENGTH_SHORT).show();
        }
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_user);
        this.user_img_input = (ImageView) findViewById(R.id.user_img_input);
        this.user_name_input = (EditText) findViewById(R.id.user_name_input);
        this.user_pwd_input = (EditText) findViewById(R.id.user_pwd_input);
        this.user_phone_input = (EditText) findViewById(R.id.user_phone_input);
        this.user_email_input = (EditText) findViewById(R.id.user_email_input);
        this.user_supreme_input = (CheckBox) findViewById(R.id.user_supreme_input);
        this.submit_btn = (LinearLayout) findViewById(R.id.submit_btn);
        this.pref = getApplicationContext().getSharedPreferences("HungrrMobile", 0);
        this.userId = this.pref.getString(Constant.userId, null);
        this.user_img_input.setOnClickListener(new C04271());
        this.submit_btn.setOnClickListener(new C04282());

        user_phone_input.setEnabled(false);

        showUserData();
    }

    public void showUserData() {
        try {
            FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseUserNode).child(this.userId).addListenerForSingleValueEvent(new C05143());
        } catch (Exception e) {
        }
    }

    public void createUser(String userName, String userPwd, String userPhone, String userEmail, boolean isSupreme) {
        try {
            this.userId = userPhone;
            if (this.userId.equals("")) {
                this.user_phone_input.setError("Mobile Number cant be empty");
            } else if (userPwd.equals("")) {
                this.user_pwd_input.setError("Password cant be empty");
            } else {
                final UserModel userModel = new UserModel(this.userId, this.userImage, userName, userPwd, userPhone, userEmail);
                FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseUserNode).child(this.userId).setValue(userModel).addOnSuccessListener(new OnSuccessListener<Void>() {
                    public void onSuccess(Void aVoid) {
                        Editor editor = ProfileActivity.this.getApplicationContext().getSharedPreferences("HungrrMobile", 0).edit();
                        editor.putString(Constant.userId, userModel.userId);
                        editor.putBoolean(Constant.isLoggedIn, true);
                        editor.commit();
                        Toast.makeText(ProfileActivity.this, "Process Succcessful", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        } catch (Exception e) {
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if (resultCode == -1) {
            switch (requestCode) {
                case 3:
                    ArrayList<Image> imagesList = intent.getParcelableArrayListExtra(Constants.KEY_BUNDLE_LIST);
                    if (this.userId.equals("")) {
                        this.userId = FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseUserNode).push().getKey();
                    }
                    Iterator it = imagesList.iterator();
                    while (it.hasNext()) {
                        Image image = (Image) it.next();
                        this.isUploadingImage = true;
                        final Uri file = Uri.fromFile(new File(image.imagePath));
                        String path = image.imagePath;
                        String extension = path.substring(path.lastIndexOf("."));
                        FirebaseStorage.getInstance().getReference().child("/users/" + (new Random().nextInt(1000) + "" + System.currentTimeMillis())).putFile(file).addOnSuccessListener(new C05187()).addOnFailureListener(new C05176()).addOnProgressListener(new OnProgressListener<TaskSnapshot>() {
                            public void onProgress(TaskSnapshot taskSnapshot) {
                                Glide.with(ProfileActivity.this).load(file).into(ProfileActivity.this.user_img_input);
                                Log.e("CreateBasicRestaurant", ((100.0d * ((double) taskSnapshot.getBytesTransferred())) / ((double) taskSnapshot.getTotalByteCount())) + "%");
                            }
                        });
                    }
                    return;
                default:
                    return;
            }
        }
    }

    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}