package com.hunggrmy.hunggrmy;

import android.Manifest;
import android.animation.ValueAnimator;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.location.LocationManager;
import android.os.Handler;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.billing.mobilebillingsystem.main.Utils.LogUtils;
import com.google.android.play.core.appupdate.AppUpdateInfo;
import com.google.android.play.core.appupdate.AppUpdateManager;
import com.google.android.play.core.appupdate.AppUpdateManagerFactory;
import com.google.android.play.core.install.InstallState;
import com.google.android.play.core.install.InstallStateUpdatedListener;
import com.google.android.play.core.install.model.InstallStatus;
import com.google.android.play.core.install.model.UpdateAvailability;
import com.google.android.play.core.tasks.OnSuccessListener;
import com.google.android.play.core.tasks.Task;
import com.google.gson.Gson;
import com.hunggrmy.hunggrmy.listeners.GetRestaurants;
import com.hunggrmy.hunggrmy.models.RestaurantBasicDetailsModel;
import com.hunggrmy.hunggrmy.service.RegistrationIntentService;
import com.hunggrmy.hunggrmy.utils.Constant;
import com.hunggrmy.hunggrmy.utils.GPSTracker;
import com.hunggrmy.hunggrmy.utils.Utility;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.nabinbhandari.android.permissions.PermissionHandler;
import com.nabinbhandari.android.permissions.Permissions;

import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.List;

import eightbitlab.com.blurview.BlurView;
import eightbitlab.com.blurview.RenderScriptBlur;

import static com.google.android.play.core.install.model.AppUpdateType.IMMEDIATE;
import static com.google.firebase.crash.FirebaseCrash.log;

public class SplashScreenActivity extends AppCompatActivity implements InstallStateUpdatedListener {

    private static final int END_WIDTH = 800;
    private static final long DURATION = 2000;
    GPSTracker gps;
    Double latitude, longitude;
    boolean isSettingsOpen = false;
    boolean isMainPageOpen = false;
    LinearLayout grant_permission_btn;
    ImageView logoIV;
    private int MY_REQUEST_CODE = 11111;
    AppUpdateManager appUpdateManager;
    BlurView blurView;
    TextView text1, text2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        logoIV = (ImageView) findViewById(R.id.logoIV);
        text1 = (TextView) findViewById(R.id.text1);
        text2 = (TextView) findViewById(R.id.text2);

        gps = new GPSTracker(SplashScreenActivity.this);
        grant_permission_btn = (LinearLayout) findViewById(R.id.grant_permission_btn);
        grant_permission_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                permissionCheck();
            }
        });

        blurView = (BlurView) findViewById(R.id.blurView);
        animate(blurView);
        permissionCheck();

        float radius = 10f;

        View decorView = getWindow().getDecorView();
        //ViewGroup you want to start blur from. Choose root as close to BlurView in hierarchy as possible.
        ViewGroup rootView = (ViewGroup) decorView.findViewById(android.R.id.content);
        //Set drawable to draw in the beginning of each blurred frame (Optional).
        //Can be used in case your layout has a lot of transparent space and your content
        //gets kinda lost after after blur is applied.
        Drawable windowBackground = decorView.getBackground();

        blurView.setupWith(rootView)
                .setFrameClearDrawable(windowBackground)
                .setBlurAlgorithm(new RenderScriptBlur(this))
                .setBlurRadius(radius)
                .setHasFixedTransformationMatrix(true);


        Intent intent = new Intent(this, RegistrationIntentService.class);
        startService(intent);
    }

    public void loadAllRestaurants() {
        GetRestaurants getRestaurants = new GetRestaurants();

        getRestaurants.setRestaurantsListener(new GetRestaurants.GetRestaurantsListener() {
            @Override
            public void onObjectReady(String title) {

            }

            @Override
            public void onDataLoaded(JSONObject restaurants) {
//                LogUtils.Log("restaurants : " + restaurants);
                JSONObject jsonObject1 = new JSONObject();
                JSONArray jsonArray1 = new JSONArray();

                try {
                    JSONObject jsonObject = new JSONObject(restaurants + "");
                    JSONArray jsonArray = jsonObject.getJSONArray("restaurants");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        Gson g = new Gson();
                        RestaurantBasicDetailsModel restaurantBasicDetailsModel = null;
                        try {
                            restaurantBasicDetailsModel = g.fromJson(jsonArray.get(i) + "", RestaurantBasicDetailsModel.class);
                            if (!restaurantBasicDetailsModel.restaurantLatLng.isEmpty()) {

                                String[] splitted = restaurantBasicDetailsModel.restaurantLatLng.split(",");

                                if (Utility.isWithinDesiredMeters(5000, Double.parseDouble(splitted[0]), Double.parseDouble(splitted[1]), latitude, longitude)) {
                                    Gson gson = new Gson();
                                    jsonArray1.put(gson.toJson(restaurantBasicDetailsModel));
                                }

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        if (i == jsonArray.length() - 1) {
                            jsonObject1.put("restaurants", jsonArray1);

                            //Save the restaurants
                            SharedPreferences.Editor editor = SplashScreenActivity.this.getApplicationContext().getSharedPreferences("HungrrMobile", 0).edit();
                            editor.putString(Constant.restautantsList, jsonObject1 + "");
                            editor.commit();

                            checkForUpdates();

                            movetoMapsActivity();
                        }
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Something went wrong. Please reopen the app.", Toast.LENGTH_SHORT).show();
                }


            }
        });

    }

    public void SlideToAbove() {
        Animation animation;
        animation = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.slide_in_top);

        animation.setDuration(1000);
        logoIV.startAnimation(animation);

        animation.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {

                logoIV.clearAnimation();

//                RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(
//                        rl_footer.getWidth(), rl_footer.getHeight());
//                // lp.setMargins(0, 0, 0, 0);
//                lp.addRule(RelativeLayout.ALIGN_PARENT_TOP);
//                rl_footer.setLayoutParams(lp);

            }

        });

    }

    private void permissionCheck() {
        Permissions.check(this, Manifest.permission.ACCESS_FINE_LOCATION, null,
                new PermissionHandler() {
                    @Override
                    public void onGranted() {
                        LogUtils.Log("Permission granted");
                        if (gps.canGetLocation()) {
                            LogUtils.Log("location can get");
                            latitude = gps.getLatitude();
                            longitude = gps.getLongitude();
                            if (!isMainPageOpen) {
                                isMainPageOpen = true;
//                                loadAllRestaurants();
                                checkForUpdates();
//                                movetoMapsActivity();
                            }
                        } else {
                            Toast.makeText(SplashScreenActivity.this, R.string.cant_access_location, Toast.LENGTH_SHORT).show();
                            checkIfGpsIsEnabled();
                        }
                    }
                });
    }

    public void askForPermissions() {
        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.READ_SMS,
                        Manifest.permission.READ_CONTACTS,
                        Manifest.permission.CAMERA,
                        Manifest.permission.CALL_PHONE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted()) {
                            // do you work now
                        }

                        // check for permanent denial of any permission
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            // permission is denied permenantly, navigate user to app settings
//                            Toast.makeText(SplashActivity.this, "We are needing some permissions that you denied, for providing you a smooth experience", Toast.LENGTH_SHORT).show();
//                            askForPermissions();
                        }

                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }

                })
                .onSameThread()
                .check();
    }

    private void checkIfGpsIsEnabled() {

        final Handler ha = new Handler();
        ha.postDelayed(new Runnable() {

            @Override
            public void run() {
                //call function
                LocationManager service = (LocationManager) getSystemService(LOCATION_SERVICE);
                boolean enabled = service.isProviderEnabled(LocationManager.GPS_PROVIDER);
                if (enabled) {
                    if (gps.canGetLocation()) {
                        latitude = gps.getLatitude();
                        longitude = gps.getLongitude();
                        if (!isMainPageOpen) {
                            isMainPageOpen = true;
//                            loadAllRestaurants();
                            movetoMapsActivity();
                        }
                    } else {
                        if (gps.canGetLocation()) {
                            latitude = gps.getLatitude();
                            longitude = gps.getLongitude();
                            if (!isMainPageOpen) {
                                isMainPageOpen = true;
//                                loadAllRestaurants();
                                movetoMapsActivity();
                            }
                        }
                    }
                } else {
                    if (!isSettingsOpen) {
                        Toast.makeText(SplashScreenActivity.this, "Please Turn On GPS", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(intent);
                        isSettingsOpen = true;
                    }
                }
                ha.postDelayed(this, 1000);
            }
        }, 000);

    }

    private void movetoMapsActivity() {
        final Handler ha = new Handler();
        ha.postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(SplashScreenActivity.this, MainActivity.class));
                finish();
            }
        }, 2000);
    }

    private void checkForUpdates() {
        // Creates instance of the manager.
        appUpdateManager = AppUpdateManagerFactory.create(SplashScreenActivity.this);
        appUpdateManager.registerListener(this);
// Returns an intent object that you use to check for an update.
        Task<AppUpdateInfo> appUpdateInfo = appUpdateManager.getAppUpdateInfo();

        appUpdateInfo.addOnSuccessListener(new OnSuccessListener<AppUpdateInfo>() {
            @Override
            public void onSuccess(AppUpdateInfo appUpdateInfo) {
                // Checks that the platform will allow the specified type of update.
                if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE
                        // For a flexible update, use AppUpdateType.FLEXIBLE
                        && appUpdateInfo.isUpdateTypeAllowed(IMMEDIATE)) {
                    // Request the update.

                    try {
                        appUpdateManager.startUpdateFlowForResult(
                                // Pass the intent that is returned by 'getAppUpdateInfo()'.
                                appUpdateInfo,
                                // Or 'AppUpdateType.FLEXIBLE' for flexible updates.
                                IMMEDIATE,
                                // The current activity making the update request.
                                SplashScreenActivity.this,
                                // Include a request code to later monitor this update request.
                                MY_REQUEST_CODE);
                    } catch (Exception e) {
                        e.printStackTrace();
                        movetoMapsActivity();
                    }

                } else {
                    movetoMapsActivity();
                }
            }
        });


    }
    
    private void animate(BlurView v){
        ValueAnimator anim = ValueAnimator.ofInt(v.getMeasuredWidth(), END_WIDTH);
        anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                int val = (Integer) valueAnimator.getAnimatedValue();
                ViewGroup.LayoutParams layoutParams = v.getLayoutParams();
                layoutParams.width = val;
                v.setLayoutParams(layoutParams);
            }
        });
        anim.setDuration(DURATION);
        anim.start();


        final Animation in = new AlphaAnimation(0.0f, 1.0f);
        in.setDuration(1000);

        final Animation out = new AlphaAnimation(1.0f, 0.0f);
        out.setDuration(3000);

        text1.startAnimation(in);
        text2.setAnimation(in);

    }

    @Override
    public void onStateUpdate(InstallState installState) {
        if (installState.installStatus() == InstallStatus.DOWNLOADED) {
            // After the update is downloaded, show a notification
            // and request user confirmation to restart the app.
            Toast.makeText(SplashScreenActivity.this, "Downloaded", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == MY_REQUEST_CODE) {
            if (requestCode != RESULT_OK) {
                log("Update flow failed! Result code: " + resultCode);
                // If the update is cancelled or fails,
                // you can request to start the update again.
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

//        appUpdateManager
//                .getAppUpdateInfo()
//                .addOnSuccessListener(
//                        appUpdateInfo -> {
//                            if (appUpdateInfo.updateAvailability()
//                                    == UpdateAvailability.DEVELOPER_TRIGGERED_UPDATE_IN_PROGRESS) {
//                                // If an in-app update is already running, resume the update.
//                                try {
//                                    appUpdateManager.startUpdateFlowForResult(
//                                            appUpdateInfo,
//                                            IMMEDIATE,
//                                            this,
//                                            MY_REQUEST_CODE);
//                                } catch (Exception e) {
//                                    e.printStackTrace();
//                                }
//                            }
//                        });
    }

}
