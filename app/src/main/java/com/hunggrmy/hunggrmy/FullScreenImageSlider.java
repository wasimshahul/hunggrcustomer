package com.hunggrmy.hunggrmy;

import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.hunggrmy.hunggrmy.adapters.FullScreenImageAdapter;
import com.hunggrmy.hunggrmy.models.ImageModel;
import com.hunggrmy.hunggrmy.utils.Constant;

import java.util.ArrayList;

public class FullScreenImageSlider extends AppCompatActivity {

    private FullScreenImageAdapter adapter;
    private ViewPager viewPager;
    ArrayList<String> imageList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_screen_image_slider);

        viewPager = (ViewPager) findViewById(R.id.pager);

        Intent i = getIntent();
        int position = 0;
        try {
            position = i.getIntExtra("position", 0);
        } catch (Exception e) {
            e.printStackTrace();
        }

        String restaurantId = null;
        String type = "";

        adapter = new FullScreenImageAdapter(FullScreenImageSlider.this,
                imageList);

        viewPager.setAdapter(adapter);

        try {
            restaurantId = i.getStringExtra("restaurantId");
            type = i.getStringExtra("type");

            if(type.equals("restaurant")){
                FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseRestaurantNode).child(restaurantId).child(Constant.FirebaseRestaurantImagesNode).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        imageList.clear();
                        for (DataSnapshot datasnapshot1 :
                                dataSnapshot.getChildren()) {
                            ImageModel imagemodel = datasnapshot1.getValue(ImageModel.class);
                            imageList.add(imagemodel.getImageLink());
                        }
                        adapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            } else {
                FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseRestaurantNode).child(restaurantId).child(Constant.FirebaseMenuImagesNode).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        imageList.clear();
                        for (DataSnapshot datasnapshot1 :
                                dataSnapshot.getChildren()) {
                            ImageModel imagemodel = datasnapshot1.getValue(ImageModel.class);
                            imageList.add(imagemodel.getImageLink());
                        }
                        adapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }


        } catch (Exception e) {
            e.printStackTrace();
        }

        // displaying selected image first
        viewPager.setCurrentItem(position);

    }
}
