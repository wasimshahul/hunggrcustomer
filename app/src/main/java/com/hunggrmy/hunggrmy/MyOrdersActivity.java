package com.hunggrmy.hunggrmy;

import android.content.SharedPreferences;
import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.hunggrmy.hunggrmy.adapters.BookingAdapter;
import com.hunggrmy.hunggrmy.adapters.OrdersAdapter;
import com.hunggrmy.hunggrmy.utils.Constant;

import java.util.ArrayList;
import java.util.Collections;

public class MyOrdersActivity extends AppCompatActivity {

    TextView noresultTV;
    RecyclerView recycler_view;
    ImageView backIcon;
    private ArrayList<String> dataList = new ArrayList<>();
    private OrdersAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_orders);

        recycler_view = (RecyclerView) findViewById(R.id.recycler_view);
        noresultTV = (TextView) findViewById(R.id.noresultTV);
        backIcon = (ImageView) findViewById(R.id.backIcon);

        //All Data List
        adapter = new OrdersAdapter(dataList);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(MyOrdersActivity.this, LinearLayoutManager.VERTICAL, false);
        recycler_view.setLayoutManager(mLayoutManager);
        recycler_view.setItemAnimator(new DefaultItemAnimator());
        recycler_view.setAdapter(adapter);

        backIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        getAllIds();
    }

    public void getAllIds() {
        try {
            final SharedPreferences pref = getSharedPreferences("HungrrMobile", MODE_PRIVATE);

            FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseUserNode).child(pref.getString(Constant.userId, null)).child(Constant.OrderNode).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    dataList.clear();
                    for (DataSnapshot datasnapshot1 :
                            dataSnapshot.getChildren()) {
                        final String id = datasnapshot1.getValue(String.class);
                        String key = datasnapshot1.getKey();
                        if (key.equals("restaurantId")) {
                            continue;
                        }
                        dataList.add(id);
                    }
                    Collections.reverse(dataList);
                    adapter.notifyDataSetChanged();
                    if(dataList.size()<1){
                        noresultTV.setVisibility(View.VISIBLE);
                        recycler_view.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        } catch (Exception e) {

        }
    }
}
