package com.hunggrmy.hunggrmy.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.hunggrmy.hunggrmy.R;
import com.hunggrmy.hunggrmy.RestaurantDetailActivity;
import com.hunggrmy.hunggrmy.models.ImageModel;
import com.hunggrmy.hunggrmy.models.RestaurantBasicDetailsModel;

import java.util.List;

/**
 * Created by Wasim on 5/14/2018.
 */

public class RestaurantBoxAdapter extends Adapter<RestaurantBoxAdapter.MyViewHolder> {
    private Context context;
    private List<RestaurantBasicDetailsModel> menuList;

    public class MyViewHolder extends ViewHolder {
        public TextView isRcmdTV;
        public TextView itemTypeTV;
        public ImageView menuImg;
        public TextView menuNameTV;
        public TextView menuOffTV;
        public TextView menuPriceTV;
        public ImageView menuTypeImg;
        private RelativeLayout rest_card_box;

        public MyViewHolder(View view) {
            super(view);
            this.menuNameTV = (TextView) view.findViewById(R.id.title);
            this.menuPriceTV = (TextView) view.findViewById(R.id.subTitle);
            this.menuOffTV = (TextView) view.findViewById(R.id.menuOffTV);
            this.isRcmdTV = (TextView) view.findViewById(R.id.isRcmdTV);
            this.itemTypeTV = (TextView) view.findViewById(R.id.itemTypeTV);
            this.menuImg = (ImageView) view.findViewById(R.id.mainImg);
            this.menuTypeImg = (ImageView) view.findViewById(R.id.menuTypeImg);
            this.rest_card_box = (RelativeLayout) view.findViewById(R.id.rest_card_box);
        }
    }

    public RestaurantBoxAdapter(List<RestaurantBasicDetailsModel> menuList) {
        this.menuList = menuList;
    }

    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.menu_box_view, parent, false);
        this.context = itemView.getContext();
        return new MyViewHolder(itemView);
    }

    public void onBindViewHolder(MyViewHolder holder, int position) {
        final RestaurantBasicDetailsModel menu = (RestaurantBasicDetailsModel) this.menuList.get(position);
        try {
            Glide.with(this.context).load(((ImageModel) menu.restaurantImages.get(0)).getImageLink()).placeholder(R.drawable.hugrrlogo).error(R.drawable.restaurant).into(holder.menuImg);
        } catch (Exception e) {
            Glide.with(this.context).load(R.drawable.restaurant).placeholder(R.drawable.restaurant).error(R.drawable.restaurant).into(holder.menuImg);
        }
        try {
            holder.menuNameTV.setText(menu.restaurantName);
            holder.rest_card_box.setOnClickListener(new OnClickListener() {
                public void onClick(View v) {
                    Intent intent = new Intent(RestaurantBoxAdapter.this.context, RestaurantDetailActivity.class);
                    intent.putExtra("restaurantId", menu.restaurantId);
                    intent.putExtra("setselection", "");
                    RestaurantBoxAdapter.this.context.startActivity(intent);
                }
            });
        } catch (Exception e2) {
        }
    }

    public int getItemCount() {
        return this.menuList.size();
    }
}