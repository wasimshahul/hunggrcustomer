package com.hunggrmy.hunggrmy.adapters;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.design.widget.BottomSheetDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.hunggrmy.hunggrmy.MapsActivity;
import com.hunggrmy.hunggrmy.ProfileMenuActivity;
import com.hunggrmy.hunggrmy.R;
import com.hunggrmy.hunggrmy.WebPageActivity;
import com.hunggrmy.hunggrmy.models.HunggrUpdatesModel;

import java.util.ArrayList;

/**
 * Created by Wasim on 5/14/2018.
 */

public class HunggrUpdatesAdapter extends RecyclerView.Adapter<HunggrUpdatesAdapter.MyViewHolder> {

    private ArrayList<HunggrUpdatesModel> hunggrUpdatesModels;
    private Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView titleTv, line1Tv, line2Tv, line3Tv;
        private Button knowMoreBtn;

        public MyViewHolder(View view) {
            super(view);
            titleTv = (TextView) view.findViewById(R.id.titleTv);
            line1Tv = (TextView) view.findViewById(R.id.line1Tv);
            line2Tv = (TextView) view.findViewById(R.id.line2Tv);
            line3Tv = (TextView) view.findViewById(R.id.line3Tv);
            knowMoreBtn = (Button) view.findViewById(R.id.knowMoreBtn);
        }
    }


    public HunggrUpdatesAdapter(ArrayList<HunggrUpdatesModel> hunggrUpdatesModels) {
        this.hunggrUpdatesModels = hunggrUpdatesModels;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.hunggr_updates, parent, false);
        context = itemView.getContext();

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {

        final HunggrUpdatesModel hunggrUpdatesModel = hunggrUpdatesModels.get(position);

        if (hunggrUpdatesModel.hasKnowMore) {
            holder.knowMoreBtn.setVisibility(View.VISIBLE);

        } else {
            holder.knowMoreBtn.setVisibility(View.INVISIBLE);
        }

        holder.knowMoreBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showBottomAlert(hunggrUpdatesModel);
            }
        });

        holder.titleTv.setText(hunggrUpdatesModel.title);
        holder.line1Tv.setText(hunggrUpdatesModel.line1);
        holder.line2Tv.setText(hunggrUpdatesModel.line2);
        holder.line3Tv.setText(hunggrUpdatesModel.line3);

    }

    private void showBottomAlert(final HunggrUpdatesModel hunggrUpdatesModel) {
        final BottomSheetDialog dialog = new BottomSheetDialog(context);
        dialog.setContentView(R.layout.bottom_notification_alert_dialog);
        TextView title = (TextView) dialog.findViewById(R.id.titleTv);
        TextView line1 = (TextView) dialog.findViewById(R.id.line1Tv);
        TextView line2 = (TextView) dialog.findViewById(R.id.line2Tv);
        TextView line3 = (TextView) dialog.findViewById(R.id.line3Tv);
        LinearLayout buttonLL = (LinearLayout) dialog.findViewById(R.id.buttonLL);

        if (title != null) {
            title.setText(hunggrUpdatesModel.title);
        }
        assert line1 != null;
        line1.setText(hunggrUpdatesModel.line1);
        assert line2 != null;
        line2.setText(hunggrUpdatesModel.line2);
        assert line3 != null;
        line3.setText(hunggrUpdatesModel.line3);

        buttonLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (hunggrUpdatesModel.url != null) {
                    String urlString = hunggrUpdatesModel.url;
//                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(urlString));
//                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                    intent.setPackage("com.android.chrome");

                    Intent intent = new Intent( context, WebPageActivity.class);
//                intent.putExtra("url", "https://www.androidhive.info/2016/12/android-working-with-webview-building-a-simple-in-app-browser/");
                    intent.putExtra("url", urlString);

                    try {
                        context.startActivity(intent);
                    } catch (ActivityNotFoundException ex) {
                        // Chrome browser presumably not installed so allow user to choose instead
                        intent.setPackage(null);
                        try {
                            context.startActivity(intent);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        });

        dialog.show();
    }

    @Override
    public int getItemCount() {
        return hunggrUpdatesModels.size();
    }
}