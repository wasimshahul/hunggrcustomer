package com.hunggrmy.hunggrmy.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.hunggrmy.hunggrmy.R;
import com.hunggrmy.hunggrmy.fragments.RestaurantOffersFragment;
import com.hunggrmy.hunggrmy.models.RestaurantOffersDetailsModel;
import java.util.List;

/**
 * Created by Wasim on 5/14/2018.
 */

public class OfferAdapter extends Adapter<OfferAdapter.MyViewHolder> {
    private List<RestaurantOffersDetailsModel> OfferList;
    private RestaurantOffersFragment restaurantOffersFragment;
    private Context context;

    public class MyViewHolder extends ViewHolder {
        public ImageView offerImg;
        public TextView id;

        public MyViewHolder(View view) {
            super(view);
            this.offerImg = (ImageView) view.findViewById(R.id.offerImg);
            this.id = (TextView) view.findViewById(R.id.id);

        }
    }

    public OfferAdapter(List<RestaurantOffersDetailsModel> OfferList, RestaurantOffersFragment restaurantOffersFragment) {
        this.OfferList = OfferList;
        this.restaurantOffersFragment = restaurantOffersFragment;
    }

    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.offer_box_view, parent, false);
        this.context = itemView.getContext();
        return new MyViewHolder(itemView);
    }

    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final RestaurantOffersDetailsModel menu = OfferList.get(position);
        try {
            Glide.with(this.context).load(((RestaurantOffersDetailsModel) this.OfferList.get(position)).offerImage).into(holder.offerImg);
        } catch (Exception e) {
            Toast.makeText(this.context, e.getMessage() + "", Toast.LENGTH_SHORT).show();
        }
        try {
            holder.id.setText(menu.offerId);
        } catch (Exception e) {
            e.printStackTrace();
        }

        holder.offerImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    restaurantOffersFragment.setOffer(holder.id.getText().toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }

    public int getItemCount() {
        return this.OfferList.size();
    }
}