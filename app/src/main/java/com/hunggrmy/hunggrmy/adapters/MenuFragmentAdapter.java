package com.hunggrmy.hunggrmy.adapters;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.hunggrmy.hunggrmy.LoginActivity;
import com.hunggrmy.hunggrmy.R;
import com.hunggrmy.hunggrmy.fragments.RestaurantMenuFragment;
import com.hunggrmy.hunggrmy.models.RestaurantMenuModel;
import com.hunggrmy.hunggrmy.utils.Constant;

import java.util.List;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Wasim on 5/14/2018.
 */

public class MenuFragmentAdapter extends RecyclerView.Adapter<MenuFragmentAdapter.MyViewHolder> {

    private List<String> menuList;
    private Context context;
    private RestaurantMenuFragment restaurantMenuFragment;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView menuNameTV, menuPriceTV, menuOffTV, isRcmdTV, itemTypeTV, addBtn, minusTV, countTV, addTV, restID;
        public ImageView menuImg, menuTypeImg;
        public LinearLayout counterLL;

        public MyViewHolder(View view) {
            super(view);
            menuNameTV = (TextView) view.findViewById(R.id.title);
            menuPriceTV = (TextView) view.findViewById(R.id.subTitle);
            menuOffTV = (TextView) view.findViewById(R.id.menuOffTV);
            isRcmdTV = (TextView) view.findViewById(R.id.isRcmdTV);
            itemTypeTV = (TextView) view.findViewById(R.id.itemTypeTV);
            addBtn = (TextView) view.findViewById(R.id.addBtn);
            minusTV = (TextView) view.findViewById(R.id.minusTV);
            countTV = (TextView) view.findViewById(R.id.countTV);
            restID = (TextView) view.findViewById(R.id.restID);
            addTV = (TextView) view.findViewById(R.id.addTV);
            counterLL = (LinearLayout) view.findViewById(R.id.counterLL);
            menuImg = (ImageView) view.findViewById(R.id.mainImg);
            menuTypeImg = (ImageView) view.findViewById(R.id.menuTypeImg);
        }
    }


    public MenuFragmentAdapter(List<String> menuList, RestaurantMenuFragment restaurantMenuFragment) {
        this.menuList = menuList;
        this.restaurantMenuFragment = restaurantMenuFragment;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.menu_list_view, parent, false);
        context = itemView.getContext();

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final String menuId = menuList.get(position);
        final SharedPreferences pref = context.getSharedPreferences("HungrrMobile", MODE_PRIVATE);
        try {
            FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseUserNode).child(pref.getString(Constant.userId, null)).child(Constant.FirebaseCartNode).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    for (DataSnapshot dataSnapshot1 :
                            dataSnapshot.getChildren()) {
                        String menuId2 = dataSnapshot1.getKey();
                        final String menuNos = dataSnapshot1.getValue(String.class);
                        if (menuId.equals(menuId2)) {
                            holder.addBtn.setVisibility(View.GONE);
                            holder.counterLL.setVisibility(View.VISIBLE);
                            holder.countTV.setText(menuNos);
                        }
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseRestaurantNode).child(Constant.FirebaseMenuNode).child(menuId).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    final RestaurantMenuModel menu = dataSnapshot.getValue(RestaurantMenuModel.class);

                    holder.restID.setText(menu.restaurantId);

                    try {
                        Glide.with(context)
                                .load(menu.itemImage)
                                .placeholder(R.drawable.dinner)
                                .error(R.drawable.dinner)
                                .into(holder.menuImg);
                    } catch (Exception e) {

                    }

                    try {

                        holder.addTV.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                final SharedPreferences pref = context.getSharedPreferences("HungrrMobile", MODE_PRIVATE);
                                if (pref.getBoolean(Constant.isLoggedIn, Boolean.parseBoolean(null))) {
                                    int numberOfItems = Integer.parseInt(holder.countTV.getText().toString()) + 1;
                                    holder.countTV.setText(numberOfItems + "");
                                    FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseUserNode).child(pref.getString(Constant.userId, null)).child(Constant.FirebaseCartNode).child(menu.itemId).setValue(holder.countTV.getText().toString());
                                    FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseUserNode).child(pref.getString(Constant.userId, null)).child(Constant.OrderNode).child(Constant.restautanyId).setValue(restaurantMenuFragment.getMenuRestaurantId());
                                }
                            }
                        });
                        holder.minusTV.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                final SharedPreferences pref = context.getSharedPreferences("HungrrMobile", MODE_PRIVATE);
                                if (pref.getBoolean(Constant.isLoggedIn, Boolean.parseBoolean(null))) {
                                    if (!holder.countTV.getText().toString().equals("1")) {
                                        int numberOfItems = Integer.parseInt(holder.countTV.getText().toString()) - 1;
                                        holder.countTV.setText(numberOfItems + "");
                                        FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseUserNode).child(pref.getString(Constant.userId, null)).child(Constant.FirebaseCartNode).child(menu.itemId).setValue(holder.countTV.getText().toString());
                                        FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseUserNode).child(pref.getString(Constant.userId, null)).child(Constant.OrderNode).child(Constant.restautanyId).setValue(restaurantMenuFragment.getMenuRestaurantId());
                                    } else {
                                        FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseUserNode).child(pref.getString(Constant.userId, null)).child(Constant.FirebaseCartNode).child(menu.itemId).removeValue();
                                        FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseUserNode).child(pref.getString(Constant.userId, null)).child(Constant.OrderNode).child(Constant.restautanyId).removeValue();
                                        holder.counterLL.setVisibility(View.GONE);
                                        holder.addBtn.setVisibility(View.VISIBLE);
                                    }
                                }
                            }
                        });

                        holder.addBtn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                final SharedPreferences pref = context.getSharedPreferences("HungrrMobile", MODE_PRIVATE);
                                if (pref.getBoolean(Constant.isLoggedIn, Boolean.parseBoolean(null))) {

                                    try {
                                        if (restaurantMenuFragment.getMenuRestaurantId().equals(menu.restaurantId) || restaurantMenuFragment.getMenuRestaurantId().equals("")) {
//                                            Log.e("menu", "menu: "+menu.restaurantId+" : ->"+restaurantMenuFragment.getMenuRestaurantId());
                                            FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseUserNode).child(pref.getString(Constant.userId, null)).child(Constant.FirebaseCartNode).child(menu.itemId).setValue(holder.countTV.getText().toString());
//                                            Toast.makeText(context, "res ID: "+restaurantMenuFragment.getMenuRestaurantId() + " res ID2 : "+menu.restaurantId, Toast.LENGTH_SHORT).show();
                                            FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseUserNode).child(pref.getString(Constant.userId, null)).child(Constant.OrderNode).child(Constant.restautanyId).setValue(menu.restaurantId);
//                                            FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseUserNode).child(pref.getString(Constant.userId, null)).child(Constant.OrderNode).child(Constant.restautanyId).setValue(restaurantMenuFragment.getMenuRestaurantId());
                                            holder.addBtn.setVisibility(View.GONE);
                                            holder.counterLL.setVisibility(View.VISIBLE);
                                        } else {
                                            //Show alert
                                            // custom dialog
                                            final Dialog dialog = new Dialog(context);
                                            dialog.setContentView(R.layout.cart_alert);

                                            // set the custom dialog components - text, image and button
                                            TextView yes = (TextView) dialog.findViewById(R.id.yesTv);
                                            TextView no = (TextView) dialog.findViewById(R.id.noTv);

                                            yes.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View view) {
                                                    final SharedPreferences pref = context.getSharedPreferences("HungrrMobile", MODE_PRIVATE);
                                                    FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseUserNode).child(pref.getString(Constant.userId, null)).child(Constant.OrderNode).child(Constant.restautanyId).setValue(holder.restID.getText().toString()+"");

                                                    FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseUserNode).child(pref.getString(Constant.userId, null)).child(Constant.FirebaseCartNode).removeValue().addOnSuccessListener(new OnSuccessListener<Void>() {
                                                        @Override
                                                        public void onSuccess(Void aVoid) {
//                                                            Toast.makeText(context, "setting menu", Toast.LENGTH_SHORT).show();
//                                                            Toast.makeText(context, restaurantMenuFragment.getMenuRestaurantId(), Toast.LENGTH_SHORT).show();
                                                            Log.e("Logged", "Log " + "Menu Added");
                                                            FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseUserNode).child(pref.getString(Constant.userId, null)).child(Constant.FirebaseCartNode).child(menu.itemId).setValue(holder.countTV.getText().toString());
                                                            FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseUserNode).child(pref.getString(Constant.userId, null)).child(Constant.OrderNode).child(Constant.restautanyId).setValue(holder.restID.getText().toString()).addOnSuccessListener(new OnSuccessListener<Void>() {
                                                                @Override
                                                                public void onSuccess(Void aVoid) {
                                                                    dialog.dismiss();
                                                                }
                                                            });
                                                        }
                                                    });
                                                    holder.addBtn.setVisibility(View.GONE);
                                                    holder.counterLL.setVisibility(View.VISIBLE);
                                                }
                                            });

                                            no.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View view) {
                                                    dialog.dismiss();
                                                }
                                            });

                                            dialog.show();

                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                } else {
                                    Toast.makeText(context, "Please login to user your cart", Toast.LENGTH_SHORT).show();
                                    context.startActivity(new Intent(context, LoginActivity.class));
                                }

                            }
                        });
                    } catch (Exception e) {

                    }

                    try {
                        holder.menuNameTV.setText(menu.itemName);
                        holder.menuPriceTV.setText("RM " + menu.itemPrice);
                        if (menu.isRcmd) {
                            holder.isRcmdTV.setVisibility(View.VISIBLE);
                        }
                        holder.itemTypeTV.setText("In " + menu.itemCategory);
                        if (menu.isVeg) {
                            Glide.with(context)
                                    .load(R.drawable.vicon)
                                    .into(holder.menuTypeImg);
                        } else {
                            Glide.with(context)
                                    .load(R.drawable.nvicon)
                                    .into(holder.menuTypeImg);
                        }
                    } catch (Exception e) {

                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return menuList.size();
    }

}