package com.hunggrmy.hunggrmy.adapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.hunggrmy.hunggrmy.R;
import com.hunggrmy.hunggrmy.models.RestaurantMenuModel;
import com.hunggrmy.hunggrmy.utils.Constant;

import java.util.List;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Wasim on 5/14/2018.
 */

public class MenuActivityAdapter extends RecyclerView.Adapter<MenuActivityAdapter.MyViewHolder> {

    private List<String> menuList;
    private Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView menuNameTV, menuPriceTV, menuOffTV, isRcmdTV, itemTypeTV, addBtn, minusTV, countTV, addTV;
        public ImageView menuImg, menuTypeImg;
        public LinearLayout counterLL;

        public MyViewHolder(View view) {
            super(view);
            menuNameTV = (TextView) view.findViewById(R.id.title);
            menuPriceTV = (TextView) view.findViewById(R.id.subTitle);
            menuOffTV = (TextView) view.findViewById(R.id.menuOffTV);
            isRcmdTV = (TextView) view.findViewById(R.id.isRcmdTV);
            itemTypeTV = (TextView) view.findViewById(R.id.itemTypeTV);
            addBtn = (TextView) view.findViewById(R.id.addBtn);
            minusTV = (TextView) view.findViewById(R.id.minusTV);
            countTV = (TextView) view.findViewById(R.id.countTV);
            addTV = (TextView) view.findViewById(R.id.addTV);
            counterLL = (LinearLayout) view.findViewById(R.id.counterLL);
            menuImg = (ImageView) view.findViewById(R.id.mainImg);
            menuTypeImg = (ImageView) view.findViewById(R.id.menuTypeImg);
        }
    }


    public MenuActivityAdapter(List<String> menuList) {
        this.menuList = menuList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.menu_list_view, parent, false);
        context = itemView.getContext();

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final String menuId = menuList.get(position);
        final SharedPreferences pref = context.getSharedPreferences("HungrrMobile", MODE_PRIVATE);
        try {
            FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseUserNode).child(pref.getString(Constant.userId, null)).child(Constant.FirebaseCartNode).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    for (DataSnapshot dataSnapshot1 :
                            dataSnapshot.getChildren()) {
                        String menuId2 = dataSnapshot1.getKey();
                        final String menuNos = dataSnapshot1.getValue(String.class);
                        if (menuId.equals(menuId2)) {
                            holder.addBtn.setVisibility(View.GONE);
                            holder.counterLL.setVisibility(View.VISIBLE);
                            holder.countTV.setText(menuNos);
                        }
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseRestaurantNode).child(Constant.FirebaseMenuNode).child(menuId).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    final RestaurantMenuModel menu = dataSnapshot.getValue(RestaurantMenuModel.class);
                    try {
                        Glide.with(context)
                                .load(menu.itemImage)
                                .placeholder(R.drawable.dinner)
                                .error(R.drawable.dinner)
                                .into(holder.menuImg);
                    } catch (Exception e) {

                    }

                    try {

                        holder.addTV.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                final SharedPreferences pref = context.getSharedPreferences("HungrrMobile", MODE_PRIVATE);
                                if (pref.getBoolean(Constant.isLoggedIn, Boolean.parseBoolean(null))) {
                                    int numberOfItems = Integer.parseInt(holder.countTV.getText().toString()) + 1;
                                    holder.countTV.setText(numberOfItems + "");
                                    FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseUserNode).child(pref.getString(Constant.userId, null)).child(Constant.FirebaseCartNode).child(menu.itemId).setValue(holder.countTV.getText().toString());
                                }
                            }
                        });
                        holder.minusTV.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                final SharedPreferences pref = context.getSharedPreferences("HungrrMobile", MODE_PRIVATE);
                                if (pref.getBoolean(Constant.isLoggedIn, Boolean.parseBoolean(null))) {
                                    if (!holder.countTV.getText().toString().equals("1")) {
                                        int numberOfItems = Integer.parseInt(holder.countTV.getText().toString()) - 1;
                                        holder.countTV.setText(numberOfItems + "");
                                        FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseUserNode).child(pref.getString(Constant.userId, null)).child(Constant.FirebaseCartNode).child(menu.itemId).setValue(holder.countTV.getText().toString());
                                    } else {
                                        FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseUserNode).child(pref.getString(Constant.userId, null)).child(Constant.FirebaseCartNode).child(menu.itemId).removeValue();
                                        holder.counterLL.setVisibility(View.GONE);
                                        holder.addBtn.setVisibility(View.VISIBLE);
                                    }
                                }
                            }
                        });

                        holder.addBtn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                final SharedPreferences pref = context.getSharedPreferences("HungrrMobile", MODE_PRIVATE);
                                if (pref.getBoolean(Constant.isLoggedIn, Boolean.parseBoolean(null))) {

                                    try {
//                                            Log.e("menu", "menu: "+menu.restaurantId+" : ->"+restaurantMenuFragment.getMenuRestaurantId());
                                        FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseUserNode).child(pref.getString(Constant.userId, null)).child(Constant.FirebaseCartNode).child(menu.itemId).setValue(holder.countTV.getText().toString());
                                        holder.addBtn.setVisibility(View.GONE);
                                        holder.counterLL.setVisibility(View.VISIBLE);

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                } else {

                                }

                            }
                        });
                    } catch (Exception e) {

                    }

                    try {
                        holder.menuNameTV.setText(menu.itemName);
                        holder.menuPriceTV.setText("RM " + menu.itemPrice);
                        if (menu.isRcmd) {
                            holder.isRcmdTV.setVisibility(View.VISIBLE);
                        }
                        holder.itemTypeTV.setText("In " + menu.itemCategory);
                        if (menu.isVeg) {
                            Glide.with(context)
                                    .load(R.drawable.vicon)
                                    .into(holder.menuTypeImg);
                        } else {
                            Glide.with(context)
                                    .load(R.drawable.nvicon)
                                    .into(holder.menuTypeImg);
                        }
                    } catch (Exception e) {

                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return menuList.size();
    }

}