package com.hunggrmy.hunggrmy.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.hunggrmy.hunggrmy.BookTableActivity;
import com.hunggrmy.hunggrmy.R;
import com.hunggrmy.hunggrmy.models.BookTableModel;
import com.hunggrmy.hunggrmy.utils.Constant;

import java.util.List;

/**
 * Created by Wasim on 5/14/2018.
 */

public class BookingAdapter extends Adapter<BookingAdapter.MyViewHolder> {
    private List<String> idList;
    private Context context;

    public class MyViewHolder extends ViewHolder {
        public TextView bookingId, title, time, nop, ctno, status;
        public RelativeLayout bookingCard;

        public MyViewHolder(View view) {
            super(view);
            bookingId = (TextView) view.findViewById(R.id.bookingId);
            title = (TextView) view.findViewById(R.id.title);
            time = (TextView) view.findViewById(R.id.time);
            nop = (TextView) view.findViewById(R.id.nop);
            ctno = (TextView) view.findViewById(R.id.ctno);
            status = (TextView) view.findViewById(R.id.status);
            bookingCard = (RelativeLayout) view.findViewById(R.id.bookingCard);
        }
    }

    public BookingAdapter(List<String> idList) {
        this.idList = idList;
    }

    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.book_table_adapter, parent, false);
        this.context = itemView.getContext();
        return new MyViewHolder(itemView);
    }

    public void onBindViewHolder(final MyViewHolder holder, int position) {
        try {
            FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseBookingNode).child(idList.get(position)).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    final BookTableModel menu = dataSnapshot.getValue(BookTableModel.class);

                    holder.bookingId.setText("Booking id : "+menu.bookingId);
                    FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseRestaurantNode).child(menu.restId).child("restaurantName").addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            holder.title.setText(dataSnapshot.getValue(String.class));
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                    try {
                        holder.time.setText(menu.bookingDate+" "+menu.bookingTime);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try {
                        holder.nop.setText("Number of people : "+menu.noOfPeople);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try {
                        holder.ctno.setText("Contact Number : "+menu.contactNumber);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try {
                        String status = menu.status;
                        if(status.equals(Constant.bookingApproved)){
                            holder.status.setTextColor(Color.parseColor("#008000"));
                        } else if(status.equals(Constant.bookingDenied)){
                            holder.status.setTextColor(Color.parseColor("#orangered"));
                        } else if(status.equals(Constant.bookingPending)){
                            holder.status.setTextColor(Color.parseColor("#FFD700"));
                        }
                        holder.status.setText(menu.status);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    holder.bookingCard.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intent = new Intent(context, BookTableActivity.class);
                            intent.putExtra("bookingId", menu.bookingId);
                            context.startActivity(intent);
                        }
                    });

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

        } catch (Exception e) {
        }
    }

    public int getItemCount() {
        return this.idList.size();
    }
}