package com.hunggrmy.hunggrmy.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.hunggrmy.hunggrmy.R;
import com.hunggrmy.hunggrmy.fragments.RestaurantCouponFragment;
import com.hunggrmy.hunggrmy.models.RestaurantCouponDetailsModel;
import com.hunggrmy.hunggrmy.utils.Constant;

import java.util.List;

/**
 * Created by Wasim on 5/14/2018.
 */

public class CouponAdapter extends Adapter<CouponAdapter.MyViewHolder> {
    private List<String> OfferList;
    private RestaurantCouponFragment restaurantCouponFragment;

    private Context context;

    public class MyViewHolder extends ViewHolder {
        public ImageView offerImg;
        public TextView id;

        public MyViewHolder(View view) {
            super(view);
            offerImg = (ImageView) view.findViewById(R.id.offerImg);
            id = (TextView) view.findViewById(R.id.id);
        }
    }

    public CouponAdapter(List<String> OfferList, RestaurantCouponFragment restaurantCouponFragment) {
        this.OfferList = OfferList;
        this.restaurantCouponFragment = restaurantCouponFragment;
    }

    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.offer_box_view, parent, false);
        this.context = itemView.getContext();
        return new MyViewHolder(itemView);
    }

    public void onBindViewHolder(final MyViewHolder holder, int position) {
        try {
            FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseRestaurantNode).child(Constant.FirebaseCouponNode).child(OfferList.get(position)).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    final RestaurantCouponDetailsModel menu = dataSnapshot.getValue(RestaurantCouponDetailsModel.class);
                    Glide.with(context).load(menu.offerImage).into(holder.offerImg);
                    holder.id.setText(menu.offerId);

                    holder.offerImg.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            restaurantCouponFragment.setCoupon(holder.id.getText().toString());
                        }
                    });

                    holder.offerImg.setOnLongClickListener(new View.OnLongClickListener() {
                        @Override
                        public boolean onLongClick(View v) {
                            Toast.makeText(context, "Coupon downloaded successfully", Toast.LENGTH_SHORT).show();
                            return false;
                        }
                    });
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

        } catch (Exception e) {
        }
    }

    public int getItemCount() {
        return this.OfferList.size();
    }
}