package com.hunggrmy.hunggrmy.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.hunggrmy.hunggrmy.R;
import com.hunggrmy.hunggrmy.RestaurantDetailActivity;
import com.hunggrmy.hunggrmy.models.RestaurantBasicDetailsModel;
import com.hunggrmy.hunggrmy.models.RestaurantOffersDetailsModel;
import com.hunggrmy.hunggrmy.utils.FirebaseFunctions;

import java.util.List;

/**
 * Created by Wasim on 5/14/2018.
 */

public class MapsPageOfferAdapter extends Adapter<MapsPageOfferAdapter.MyViewHolder> {
    private List<RestaurantOffersDetailsModel> OfferList;
    private Context context;

    public class MyViewHolder extends ViewHolder {
        public ImageView offerImg;
        public TextView id, restNameTV, addressTV, cuisinesTV;

        public MyViewHolder(View view) {
            super(view);
            this.offerImg = (ImageView) view.findViewById(R.id.offerImg);
            this.id = (TextView) view.findViewById(R.id.id);
            this.restNameTV = (TextView) view.findViewById(R.id.restNameTV);
            this.addressTV = (TextView) view.findViewById(R.id.addressTV);
            this.cuisinesTV = (TextView) view.findViewById(R.id.cuisinesTV);

        }
    }

    public MapsPageOfferAdapter(List<RestaurantOffersDetailsModel> OfferList) {
        this.OfferList = OfferList;
    }

    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.maps_page_offer_box_view, parent, false);
        this.context = itemView.getContext();
        return new MyViewHolder(itemView);
    }

    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final RestaurantOffersDetailsModel menu = OfferList.get(position);
        try {
            Glide.with(this.context).load(((RestaurantOffersDetailsModel) this.OfferList.get(position)).offerImage).into(holder.offerImg);
        } catch (Exception e) {
            Toast.makeText(this.context, e.getMessage() + "", Toast.LENGTH_SHORT).show();
        }
        try {
            holder.id.setText(menu.offerId);
            FirebaseFunctions.restaurant.child(menu.restaurantId).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    RestaurantBasicDetailsModel restaurantBasicDetailsModel = dataSnapshot.getValue(RestaurantBasicDetailsModel.class);
                    holder.restNameTV.setText(restaurantBasicDetailsModel.restaurantName);
                    holder.addressTV.setText(restaurantBasicDetailsModel.restaurantAddress);
                    holder.cuisinesTV.setText(restaurantBasicDetailsModel.cuisines);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        } catch (Exception e) {
            Toast.makeText(context, "Error : "+ e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }

        holder.offerImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MapsPageOfferAdapter.this.context, RestaurantDetailActivity.class);
                intent.putExtra("restaurantId", menu.restaurantId);
                intent.putExtra("setselection", "offers");
                MapsPageOfferAdapter.this.context.startActivity(intent);
            }
        });

    }

    public int getItemCount() {
        return this.OfferList.size();
    }
}