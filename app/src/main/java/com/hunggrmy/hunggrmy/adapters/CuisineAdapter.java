package com.hunggrmy.hunggrmy.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.hunggrmy.hunggrmy.R;
import com.hunggrmy.hunggrmy.SearchActivity;

import java.util.List;

/**
 * Created by Wasim on 5/14/2018.
 */

public class CuisineAdapter extends Adapter<CuisineAdapter.MyViewHolder> {

    private List<String> cuisineList;
    private Context context;

    public class MyViewHolder extends ViewHolder {
        public TextView cuisineTitle;
        public CheckBox cuisineCheckBox;

        public MyViewHolder(View view) {
            super(view);
            cuisineTitle = (TextView) view.findViewById(R.id.cuisineTitle);
            cuisineCheckBox = (CheckBox) view.findViewById(R.id.cuisineCheckBox);
        }
    }


    public CuisineAdapter(List<String> cuisineList) {
        this.cuisineList = cuisineList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cuisine_list_view, parent, false);
        context = itemView.getContext();

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        String cuisineName = cuisineList.get(position);
        try {
            holder.cuisineTitle.setText(cuisineName);
        } catch (Exception e) {
            e.printStackTrace();
        }

        holder.cuisineCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    ((SearchActivity) context).addFilterCuisine(holder.cuisineTitle.getText().toString());
                    holder.cuisineTitle.setTextColor(context.getResources().getColor(R.color.colorPrimary));
                } else {
                    ((SearchActivity) context).removeFilterCuisine(holder.cuisineTitle.getText().toString());
                    holder.cuisineTitle.setTextColor(context.getResources().getColor(R.color.black));
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return cuisineList.size();
    }
}