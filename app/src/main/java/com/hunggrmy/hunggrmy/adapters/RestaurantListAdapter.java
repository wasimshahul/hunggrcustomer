package com.hunggrmy.hunggrmy.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.hunggrmy.hunggrmy.R;
import com.hunggrmy.hunggrmy.RestaurantDetailActivity;
import com.hunggrmy.hunggrmy.models.ImageModel;
import com.hunggrmy.hunggrmy.models.RestaurantBasicDetailsModel;
import com.hunggrmy.hunggrmy.utils.Constant;
import com.hunggrmy.hunggrmy.utils.Utility;
import com.skyfishjy.library.RippleBackground;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Wasim on 5/14/2018.
 */

public class RestaurantListAdapter extends Adapter<RestaurantListAdapter.MyViewHolder> {
    private Context context;
    private List<RestaurantBasicDetailsModel> menuList;

    public class MyViewHolder extends ViewHolder {
        public TextView isRcmdTV;
        public TextView itemTypeTV;
        public ImageView menuImg;
        public TextView menuNameTV;
        public TextView menuOffTV;
        public TextView menuPriceTV;
        public ImageView menuTypeImg;
        private RelativeLayout rest_card;
        private LinearLayout ratingLL;
        private TextView ratingTxt;
        private RippleBackground ripple;

        public MyViewHolder(View view) {
            super(view);
            this.menuNameTV = (TextView) view.findViewById(R.id.title);
            this.menuPriceTV = (TextView) view.findViewById(R.id.subTitle);
            this.menuOffTV = (TextView) view.findViewById(R.id.menuOffTV);
            this.isRcmdTV = (TextView) view.findViewById(R.id.isRcmdTV);
            this.itemTypeTV = (TextView) view.findViewById(R.id.itemTypeTV);
            this.menuImg = (ImageView) view.findViewById(R.id.mainImg);
            this.menuTypeImg = (ImageView) view.findViewById(R.id.menuTypeImg);
            this.rest_card = (RelativeLayout) view.findViewById(R.id.rest_card);
            this.ratingLL = (LinearLayout) view.findViewById(R.id.ratingLL);
            this.ratingTxt = (TextView) view.findViewById(R.id.ratingTxt);
            this.ripple = (RippleBackground) view.findViewById(R.id.ripple);
        }
    }

    public RestaurantListAdapter(List<RestaurantBasicDetailsModel> menuList) {
        this.menuList = menuList;
    }

    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.rest_list_view, parent, false);
        this.context = itemView.getContext();
        return new MyViewHolder(itemView);
    }

    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final RestaurantBasicDetailsModel menu = (RestaurantBasicDetailsModel) this.menuList.get(position);
        try {
            Glide.with(this.context).load(((ImageModel) menu.restaurantImages.get(0)).getImageLink()).placeholder(R.drawable.bghunggr).error(R.drawable.bghunggr).into(holder.menuImg);
        } catch (Exception e) {
            Glide.with(this.context).load(R.drawable.bghunggr).placeholder(R.drawable.bghunggr).error(R.drawable.bghunggr).into(holder.menuImg);
        }
        try {
            holder.rest_card.setOnClickListener(new OnClickListener() {
                public void onClick(View v) {
                    Intent intent = new Intent(RestaurantListAdapter.this.context, RestaurantDetailActivity.class);
                    intent.putExtra("restaurantId", menu.restaurantId);
                    intent.putExtra("setselection", "");
                    RestaurantListAdapter.this.context.startActivity(intent);
                }
            });
            holder.menuNameTV.setText(menu.restaurantName);
            String restType;
            if(menu.restaurantType != null){
                restType = menu.restaurantType + "\n";
            } else {
                restType = "";
            }
            holder.menuPriceTV.setText(restType+menu.avgPersonCost + " (Avg. cost per person)");
            if (menu.isRecommended.booleanValue()) {
                holder.isRcmdTV.setVisibility(View.VISIBLE);
                holder.ripple.setVisibility(View.VISIBLE);
                holder.ripple.startRippleAnimation();
            } else {
                holder.isRcmdTV.setVisibility(View.GONE);
                holder.ripple.setVisibility(View.GONE);
            }
            holder.itemTypeTV.setText(menu.cuisines);
            holder.menuTypeImg.setVisibility(View.GONE);
        } catch (Exception e2) {

        }

        if(holder.isRcmdTV.getVisibility() == View.VISIBLE){
            LinearLayout.LayoutParams layoutParams = null;
            try {
                int newHeight = (int) Utility.convertDipToPixel(context, 100); // New height in pixels
                int newWidth = (int) Utility.convertDipToPixel(context, 100); // New width in pixels
                holder.menuImg.requestLayout();
                holder.menuImg.getLayoutParams().height = newHeight;
                holder.menuImg.getLayoutParams().width = newWidth;
                holder.menuImg.setScaleType(ImageView.ScaleType.FIT_XY);
                holder.rest_card.setBackgroundColor(Color.parseColor("#0DF26722"));
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            LinearLayout.LayoutParams layoutParams = null;
            try {
                int newHeight = (int) Utility.convertDipToPixel(context, 100); // New height in pixels
                int newWidth = (int) Utility.convertDipToPixel(context, 100); // New width in pixels
                holder.menuImg.requestLayout();
                holder.menuImg.getLayoutParams().height = newHeight;
                holder.menuImg.getLayoutParams().width = newWidth;
                holder.menuImg.setScaleType(ImageView.ScaleType.FIT_XY);
                holder.rest_card.setBackgroundColor(Color.parseColor("#ffffff"));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        try {
            FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseRestaurantNode).child(menu.restaurantId).child(Constant.rating).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    try {
                        ArrayList<Float> ratinngList = new ArrayList<>();
                        for (DataSnapshot ratingDataSnapShot :
                                dataSnapshot.getChildren()) {
                            ratinngList.add(ratingDataSnapShot.getValue(Float.class));
                        }
                        if (ratinngList.isEmpty()) {
                            holder.ratingLL.setVisibility(View.INVISIBLE);
                        } else {
                            holder.ratingLL.setVisibility(View.VISIBLE);
                            float totRating = 0;
                            for (Float rating :
                                    ratinngList) {
                                totRating = rating + totRating;
                            }
                            holder.ratingTxt.setText(  String.format("%.2f", totRating / ratinngList.size() + "") );
                        }
                    } catch (Exception e) {

                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int getItemCount() {
        return this.menuList.size();
    }
}