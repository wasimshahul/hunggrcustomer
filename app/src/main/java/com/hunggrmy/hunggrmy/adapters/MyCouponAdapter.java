package com.hunggrmy.hunggrmy.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.hunggrmy.hunggrmy.R;
import com.hunggrmy.hunggrmy.models.CouponModel;
import com.hunggrmy.hunggrmy.models.RestaurantCouponDetailsModel;
import com.hunggrmy.hunggrmy.utils.Constant;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Wasim on 5/14/2018.
 */

public class MyCouponAdapter extends Adapter<MyCouponAdapter.MyViewHolder> {
    private ArrayList<CouponModel> idList;
    private Context context;

    public class MyViewHolder extends ViewHolder {
        public RelativeLayout rest_card;
        public ImageView mainImg, bigImage;
        public TextView title, maintitle, subTitle;

        public MyViewHolder(View view) {
            super(view);
            rest_card = (RelativeLayout) view.findViewById(R.id.rest_card);
            mainImg = (ImageView) view.findViewById(R.id.mainImg);
            bigImage = (ImageView) view.findViewById(R.id.bigImage);
            title = (TextView) view.findViewById(R.id.title);
            maintitle = (TextView) view.findViewById(R.id.maintitle);
            subTitle = (TextView) view.findViewById(R.id.subTitle);
        }
    }

    public MyCouponAdapter(ArrayList<CouponModel> idList) {
        this.idList = idList;
    }

    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.my_coupons_adapter, parent, false);
        this.context = itemView.getContext();
        return new MyViewHolder(itemView);
    }

    public void onBindViewHolder(final MyViewHolder holder, int position) {
        try {

            final String id = idList.get(position).getId();
            final String imageLink = idList.get(position).getImageLink();
            Picasso.with(context)
                    .load(imageLink)
                    .into(holder.bigImage);
            FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseRestaurantNode).child(Constant.FirebaseCouponNode).child(id).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    final RestaurantCouponDetailsModel menu = dataSnapshot.getValue(RestaurantCouponDetailsModel.class);
                    try {
                        Picasso.with(context)
                                .load(menu.offerImage)
                                .into(holder.mainImg);
                        holder.title.setText(menu.offerImageTitle);
                        holder.subTitle.setText("Valid upto : "+menu.offerEndDate);
                        FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseRestaurantNode).child(menu.restaurantId).child("restaurantName").addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                holder.maintitle.setText(dataSnapshot.getValue(String.class));
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });
                        holder.rest_card.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                            }
                        });

                    } catch (Exception e) {
                        Toast.makeText(context, e.getMessage()+"", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

        } catch (Exception e) {
        }
    }

    public int getItemCount() {
        return this.idList.size();
    }
}