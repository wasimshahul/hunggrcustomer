package com.hunggrmy.hunggrmy.adapters;

import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.hunggrmy.hunggrmy.R;
import com.hunggrmy.hunggrmy.RestaurantDetailActivity;
import com.hunggrmy.hunggrmy.models.ImageModel;
import com.hunggrmy.hunggrmy.models.RestaurantBasicDetailsModel;
import com.hunggrmy.hunggrmy.utils.Constant;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by Wasim on 5/14/2018.
 */

public class SearchRestaurantListAdapter extends Adapter<SearchRestaurantListAdapter.MyViewHolder> {
    private Context context;
    private List<RestaurantBasicDetailsModel> menuList;

    public class MyViewHolder extends ViewHolder {
        public TextView isRcmdTV;
        public TextView itemTypeTV;
        public ImageView menuImg;
        public TextView menuNameTV;
        public TextView menuOffTV;
        public TextView menuPriceTV;
        public ImageView menuTypeImg;
        private RelativeLayout rest_card;
        private LinearLayout ratingLL;
        private TextView ratingTxt;

        public MyViewHolder(View view) {
            super(view);
            this.menuNameTV = (TextView) view.findViewById(R.id.title);
            this.menuPriceTV = (TextView) view.findViewById(R.id.subTitle);
            this.menuOffTV = (TextView) view.findViewById(R.id.menuOffTV);
            this.isRcmdTV = (TextView) view.findViewById(R.id.isRcmdTV);
            this.itemTypeTV = (TextView) view.findViewById(R.id.itemTypeTV);
            this.menuImg = (ImageView) view.findViewById(R.id.mainImg);
            this.menuTypeImg = (ImageView) view.findViewById(R.id.menuTypeImg);
            this.rest_card = (RelativeLayout) view.findViewById(R.id.rest_card);
            this.ratingLL = (LinearLayout) view.findViewById(R.id.ratingLL);
            this.ratingTxt = (TextView) view.findViewById(R.id.ratingTxt);
        }
    }

    public SearchRestaurantListAdapter(List<RestaurantBasicDetailsModel> menuList) {
        this.menuList = menuList;
    }

    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.rest_list_view, parent, false);
        this.context = itemView.getContext();
        return new MyViewHolder(itemView);
    }

    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final RestaurantBasicDetailsModel menu = (RestaurantBasicDetailsModel) this.menuList.get(position);
        try {
            Glide.with(this.context).load(((ImageModel) menu.restaurantImages.get(0)).getImageLink()).placeholder(R.drawable.hugrrlogo).error(R.drawable.restaurant).into(holder.menuImg);
        } catch (Exception e) {
            Glide.with(this.context).load(R.drawable.restaurant).placeholder(R.drawable.restaurant).error(R.drawable.restaurant).into(holder.menuImg);
        }
        try {
            holder.rest_card.setOnClickListener(new OnClickListener() {
                public void onClick(View v) {
                    Intent intent = new Intent(SearchRestaurantListAdapter.this.context, RestaurantDetailActivity.class);
                    intent.putExtra("restaurantId", menu.restaurantId);
                    SearchRestaurantListAdapter.this.context.startActivity(intent);
                }
            });
            holder.menuNameTV.setText(menu.restaurantName);
//            holder.menuPriceTV.setText(menu.avgPersonCost + " (Avg. cost per person)");
            holder.menuPriceTV.setText(getAddress(menu.restaurantLatLng));

            if (menu.isRecommended.booleanValue()) {
                holder.isRcmdTV.setVisibility(View.VISIBLE);
            } else {
                holder.isRcmdTV.setVisibility(View.GONE);
            }
            holder.itemTypeTV.setText(menu.cuisines);
            holder.menuTypeImg.setVisibility(View.GONE);
        } catch (Exception e2) {

        }
        try {
            FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseRestaurantNode).child(menu.restaurantId).child(Constant.rating).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    try {
                        ArrayList<Float> ratinngList = new ArrayList<>();
                        for (DataSnapshot ratingDataSnapShot :
                                dataSnapshot.getChildren()) {
                            ratinngList.add(ratingDataSnapShot.getValue(Float.class));
                        }
                        if (ratinngList.isEmpty()) {
                            holder.ratingLL.setVisibility(View.INVISIBLE);
                        } else {
                            holder.ratingLL.setVisibility(View.VISIBLE);
                            float totRating = 0;
                            for (Float rating :
                                    ratinngList) {
                                totRating = rating + totRating;
                            }
                            holder.ratingTxt.setText(totRating / ratinngList.size() + "");
                        }
                    } catch (Exception e) {

                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int getItemCount() {
        return this.menuList.size();
    }

    public String getAddress(String latlng) {
        String result = "";

        try {
            String[] latlong = latlng.split(",");
            List<Address> addresses = null;
            try {
                addresses = new Geocoder(context, Locale.getDefault()).getFromLocation(Double.parseDouble(latlong[0]), Double.parseDouble(latlong[1]), 1);
            } catch (IOException e) {
                e.printStackTrace();
            }
            String address = null;
            if (addresses != null) {
                try{
                    address = ((Address) addresses.get(0)).getAddressLine(0);
                } catch (Exception e){

                }
            }
            String city = null;
            if (addresses != null) {
                try {
                    city = ((Address) addresses.get(0)).getLocality();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (addresses != null) {
                try {
                    String state = ((Address) addresses.get(0)).getAdminArea();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (addresses != null) {
                try {
                    String country = ((Address) addresses.get(0)).getCountryName();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (addresses != null) {
                try {
                    String postalCode = ((Address) addresses.get(0)).getPostalCode();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (addresses != null) {
                try {
                    String knownName = ((Address) addresses.get(0)).getFeatureName();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            result = address + "," + city;
        } catch (ArrayIndexOutOfBoundsException e) {
            e.printStackTrace();
        }
        return result;
    }
}