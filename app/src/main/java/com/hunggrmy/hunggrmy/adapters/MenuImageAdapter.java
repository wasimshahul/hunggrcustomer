package com.hunggrmy.hunggrmy.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.hunggrmy.hunggrmy.FullScreenImageSlider;
import com.hunggrmy.hunggrmy.R;
import com.hunggrmy.hunggrmy.RestaurantDetailActivity;
import com.hunggrmy.hunggrmy.models.ImageModel;

import java.util.List;

/**
 * Created by Wasim on 5/14/2018.
 */

public class MenuImageAdapter extends Adapter<MenuImageAdapter.MyViewHolder> {

    private List<ImageModel> imageModelUrlList;
    private Context context;

    public class MyViewHolder extends ViewHolder {
        public TextView menuUrl;
        public ImageView menuImg;

        public MyViewHolder(View view) {
            super(view);
            menuUrl = (TextView) view.findViewById(R.id.menuUrlTV);
            menuImg = (ImageView) view.findViewById(R.id.mainImg);
        }
    }


    public MenuImageAdapter(List<ImageModel> imageModelUrlList) {
        this.imageModelUrlList = imageModelUrlList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.rest_image_list, parent, false);
        context = itemView.getContext();

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        ImageModel movie = imageModelUrlList.get(position);
        try {
            Glide.with(context)
                    .load(movie.getImageLink())
                    .into(holder.menuImg);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            holder.menuUrl.setText(movie.getImageLink());
            holder.menuImg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, FullScreenImageSlider.class);
                    intent.putExtra("restaurantId", ((RestaurantDetailActivity) context).restaurantId);
                    intent.putExtra("position", position);
                    context.startActivity(intent);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return imageModelUrlList.size();
    }
}