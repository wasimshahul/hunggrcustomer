package com.hunggrmy.hunggrmy;

import android.content.Intent;
import android.content.SharedPreferences.Editor;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.UploadTask.TaskSnapshot;
import com.vlk.multimager.activities.GalleryActivity;
import com.vlk.multimager.utils.Constants;
import com.vlk.multimager.utils.Image;
import com.vlk.multimager.utils.Params;
import com.hunggrmy.hunggrmy.models.UserModel;
import com.hunggrmy.hunggrmy.utils.Constant;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;

public class CreateUserActivity extends AppCompatActivity {
    boolean isUploadingImage = false;
    LinearLayout submit_btn;
    String userId = "";
    String userImage = "";
    EditText user_email_input;
    ImageView user_img_input;
    EditText user_name_input;
    EditText user_phone_input;
    EditText user_pwd_input;
    CheckBox user_supreme_input;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_user);
        this.user_img_input = (ImageView) findViewById(R.id.user_img_input);
        this.user_name_input = (EditText) findViewById(R.id.user_name_input);
        this.user_pwd_input = (EditText) findViewById(R.id.user_pwd_input);
        this.user_phone_input = (EditText) findViewById(R.id.user_phone_input);
        this.user_email_input = (EditText) findViewById(R.id.user_email_input);
        this.user_supreme_input = (CheckBox) findViewById(R.id.user_supreme_input);
        this.submit_btn = (LinearLayout) findViewById(R.id.submit_btn);

        this.user_img_input.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CreateUserActivity.this, GalleryActivity.class);
                Params params = new Params();
                params.setCaptureLimit(1);
                params.setPickerLimit(1);
                params.setToolbarColor(0);
                params.setActionButtonColor(0);
                params.setButtonTextColor(ViewCompat.MEASURED_SIZE_MASK);
                intent.putExtra(Constants.KEY_PARAMS, params);
                CreateUserActivity.this.startActivityForResult(intent, 3);
            }
        });
        this.submit_btn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!CreateUserActivity.this.isUploadingImage) {
                    try {
                        FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseUserNode).addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                if (dataSnapshot.hasChild(user_phone_input.getText().toString())) {
                                    user_phone_input.setError("Seems like this number is already registered...");
                                } else {
                                    CreateUserActivity.this.createUser(CreateUserActivity.this.user_name_input.getText().toString(), CreateUserActivity.this.user_pwd_input.getText().toString(), CreateUserActivity.this.user_phone_input.getText().toString(), CreateUserActivity.this.user_email_input.getText().toString(), CreateUserActivity.this.user_supreme_input.isChecked());
                                }
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }
        });
    }

    public void CheckIfUserIsAlreadyPresent() {
    }

    public void createUser(String userName, String userPwd, String userPhone, String userEmail, boolean isSupreme) {
        try {
            this.userId = userPhone;
            if (this.userId.equals("")) {
                this.user_phone_input.setError("Mobile Number cant be empty");
            } else if (userPwd.equals("")) {
                this.user_pwd_input.setError("Password cant be empty");
            } else if (user_name_input.equals("")) {
                this.user_pwd_input.setError("Please enter your name");
            } else {
                final UserModel userModel = new UserModel(this.userId, this.userImage, userName, userPwd, userPhone, userEmail);
                try {
                    FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseUserNode).child(this.userId).setValue(userModel).addOnSuccessListener(new OnSuccessListener<Void>() {
                        public void onSuccess(Void aVoid) {
                            Editor editor = CreateUserActivity.this.getApplicationContext().getSharedPreferences("HungrrMobile", 0).edit();
                            editor.putString(Constant.userId, userModel.userId);
                            editor.putBoolean(Constant.isLoggedIn, true);
                            editor.commit();
                            Toast.makeText(CreateUserActivity.this, "User Logged In Succcessfully", Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(CreateUserActivity.this, LoginActivity.class));
                            finish();
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if (resultCode == -1) {
            switch (requestCode) {
                case 3:
                    ArrayList<Image> imagesList = intent.getParcelableArrayListExtra(Constants.KEY_BUNDLE_LIST);
                    if (this.userId.equals("")) {
                        this.userId = FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseUserNode).push().getKey();
                    }
                    Iterator it = imagesList.iterator();
                    while (it.hasNext()) {
                        Image image = (Image) it.next();
                        this.isUploadingImage = true;
                        final Uri file = Uri.fromFile(new File(image.imagePath));
                        String path = image.imagePath;
                        String extension = path.substring(path.lastIndexOf("."));
                        FirebaseStorage.getInstance().getReference().child("/users/" + (new Random().nextInt(1000) + "" + System.currentTimeMillis())).putFile(file).addOnSuccessListener(new OnSuccessListener<TaskSnapshot>() {
                            @Override
                            public void onSuccess(TaskSnapshot taskSnapshot) {
                                Uri downloadUrl = taskSnapshot.getDownloadUrl();
                                CreateUserActivity.this.userImage = downloadUrl + "";
                                CreateUserActivity.this.isUploadingImage = false;
                                Toast.makeText(CreateUserActivity.this, "Image Uploaded Successfully", Toast.LENGTH_SHORT).show();
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                CreateUserActivity.this.isUploadingImage = false;
                            }
                        }).addOnProgressListener(new OnProgressListener<TaskSnapshot>() {
                            @Override
                            public void onProgress(TaskSnapshot taskSnapshot) {
                                Glide.with(CreateUserActivity.this).load(file).into(CreateUserActivity.this.user_img_input);
                                Log.e("CreateBasicRestaurant", ((100.0d * ((double) taskSnapshot.getBytesTransferred())) / ((double) taskSnapshot.getTotalByteCount())) + "%");
                            }

                        });
                    }
                    return;
                default:
                    return;
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(CreateUserActivity.this, LoginActivity.class));
        finish();
    }
}