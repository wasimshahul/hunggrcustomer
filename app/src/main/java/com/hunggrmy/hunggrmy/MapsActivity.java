package com.hunggrmy.hunggrmy;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.clans.fab.FloatingActionButton;
import com.google.android.gms.cast.TextTrackStyle;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.hunggrmy.hunggrmy.adapters.HunggrUpdatesAdapter;
import com.hunggrmy.hunggrmy.adapters.MapsPageOfferAdapter;
import com.hunggrmy.hunggrmy.adapters.RestaurantBoxAdapter;
import com.hunggrmy.hunggrmy.adapters.RestaurantFlatBoxAdapter;
import com.hunggrmy.hunggrmy.adapters.RestaurantListAdapter;
import com.hunggrmy.hunggrmy.fragments.CentralListFragment;
import com.hunggrmy.hunggrmy.models.HunggrUpdatesModel;
import com.hunggrmy.hunggrmy.models.RestaurantBasicDetailsModel;
import com.hunggrmy.hunggrmy.models.RestaurantOffersDetailsModel;
import com.hunggrmy.hunggrmy.utils.AnimationUtils;
import com.hunggrmy.hunggrmy.utils.Constant;
import com.hunggrmy.hunggrmy.utils.FirebaseFunctions;
import com.hunggrmy.hunggrmy.utils.GPSTracker;
import com.hunggrmy.hunggrmy.utils.Utility;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;
import com.squareup.picasso.Picasso;
import com.yarolegovich.discretescrollview.DiscreteScrollView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import static com.hunggrmy.hunggrmy.utils.Utility.getDate;
import static com.hunggrmy.hunggrmy.utils.Utility.isDateExpired;


public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, GoogleApiClient.OnConnectionFailedListener, LocationListener {
    private int PLACE_PICKER_REQUEST = 1;
    LatLng currectLocation;
    private GPSTracker gps;
    ImageView gps_Icon;
    TextView locationTV, restarant_nos, filter;
    SlidingUpPanelLayout sliding_layout;
    private GoogleApiClient mGoogleApiClient;
    CardView user_card, searchCard;
    private GoogleMap mMap;
    private RestaurantFlatBoxAdapter mRestaurantAdapter, mRestaurantAdapter2;
    private RestaurantBoxAdapter mTopRestaurantAdapter;
    private MapsPageOfferAdapter mDealsRestaurantAdapter;
    public static Double preferredLatitude;
    public static Double preferredLongitude;
    private LinearLayout selectedRestLL;
    private TextView restNameTV;
    private ScrollView scrollView;
    private LinearLayout pullUpBar;
    public static boolean dialogIsShowing = false;
    FloatingActionButton profile, food_estimation, full_menu, orders_menu;
    LinearLayout restLL, dealsLL, notiLL, excLL;

    private RelativeLayout top_rl;
    private LinearLayout deals_title_ll, top_title_ll;

    RecyclerView rest_recycler_view, exclusive_rest_recycler_view, hunggrUpdatesRv;
    private List<RestaurantBasicDetailsModel> restaurantList = new ArrayList();
    private List<RestaurantBasicDetailsModel> exclusiveRestaurantList = new ArrayList();
    private List<RestaurantBasicDetailsModel> rcmdrestaurantList = new ArrayList();
    private List<RestaurantBasicDetailsModel> notrcmdrestaurantList = new ArrayList();
    ImageView shop_img;

    private List<RestaurantBasicDetailsModel> topRestaurantsList = new ArrayList();
    RecyclerView top_rest_recycler_view;

    private ArrayList<HunggrUpdatesModel> hunggrUpdatesModelArrayList = new ArrayList<>();
    private HunggrUpdatesAdapter hunggrUpdatesAdapter;

    public static List<RestaurantOffersDetailsModel> dealsRestaurantsList = new ArrayList();
    public static List<RestaurantBasicDetailsModel> ResRestaurantsList = new ArrayList();
    private List<RestaurantOffersDetailsModel> dealsRestaurantsListCloned = new ArrayList();
    private List<RestaurantBasicDetailsModel> ResRestaurantsListCloned = new ArrayList();
    DiscreteScrollView deals_recycler_view;

    ImageView user_img, search_img;
    public String selectedmarkerName;
    public String selectedmarkerId;
    LatLng midLatLng, lastKnownLocation;
    boolean isDriveModeEnabled = false;
    CheckBox driving_mode_input;

    protected PowerManager.WakeLock mWakeLock;
    private String radiusValue = "15";
    public static String coverageValue = "2000";

    @Override
    public void onLocationChanged(Location location) {
        if (isDriveModeEnabled) {
            Log.e("Logged", "Logged : location changed " + location.getLatitude() + ":" + location.getLatitude());
            preferredLatitude = location.getLatitude();
            preferredLongitude = location.getLongitude();
            getAddress();
            getAllRestaurants();
        }
    }

    public void initiateZoomingFeature() {
        FirebaseFunctions.mapZoomingRadius.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                radiusValue = dataSnapshot.getValue(String.class);
                setZoomingLevel(Float.valueOf(radiusValue));
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        FirebaseFunctions.mapAreaCoverageRadius.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                coverageValue = dataSnapshot.getValue(String.class);
                getAddress();
                getAllRestaurants();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    class C04231 implements View.OnClickListener {
        C04231() {
        }

        public void onClick(View v) {
//            startActivity(new Intent(MapsActivity.this, AddressSearchActivity.class));
            Exception e;
            try {
                Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY)
                        .build(MapsActivity.this);
                startActivityForResult(intent, PLACE_PICKER_REQUEST);
//                MapsActivity.this.startActivityForResult(new PlacePicker.IntentBuilder().build(MapsActivity.this), MapsActivity.this.PLACE_PICKER_REQUEST);
                return;
            } catch (GooglePlayServicesRepairableException e2) {
                e = e2;
            } catch (GooglePlayServicesNotAvailableException e3) {
                e = e3;
            }
            e.printStackTrace();
        }
    }

    class C04242 implements View.OnClickListener {
        C04242() {
        }

        public void onClick(View v) {
            MapsActivity.this.checkIfUserIsLoggedIn();
        }
    }

    class C05133 implements ValueEventListener {
        C05133() {
        }

        public void onDataChange(DataSnapshot dataSnapshot) {

            MapsActivity.this.mMap.clear();
            MapsActivity.this.restaurantList.clear();
            MapsActivity.this.exclusiveRestaurantList.clear();
            MapsActivity.this.rcmdrestaurantList.clear();
            MapsActivity.this.notrcmdrestaurantList.clear();
            MapsActivity.this.topRestaurantsList.clear();
            MapsActivity.this.dealsRestaurantsList.clear();
            MapsActivity.this.ResRestaurantsList.clear();
            mRestaurantAdapter.notifyDataSetChanged();
            mRestaurantAdapter2.notifyDataSetChanged();
            mTopRestaurantAdapter.notifyDataSetChanged();
            mDealsRestaurantAdapter.notifyDataSetChanged();

            if (dataSnapshot.hasChildren()) {
//                Toast.makeText(MapsActivity.this, "Has children", Toast.LENGTH_SHORT).show();
                for (DataSnapshot datasnapshot1 : dataSnapshot.getChildren()) {
                    try {
                        final RestaurantBasicDetailsModel restModel = datasnapshot1.getValue(RestaurantBasicDetailsModel.class);
                        final String[] latlng = restModel.restaurantLatLng.split(",");
                        if (MapsActivity.this.isWithin2Km(Double.valueOf(latlng[0]), Double.valueOf(latlng[1]).doubleValue())) {
                            int height = 80;
                            int width = 80;

                            int topHeight = 80;
                            int topWidth = 80;

                            BitmapDrawable bitmapdrawTop = (BitmapDrawable) getResources().getDrawable(R.drawable.top);
                            BitmapDrawable bitmapdrawRcmd = (BitmapDrawable) getResources().getDrawable(R.drawable.rcmd);

                            Bitmap bT = bitmapdrawTop.getBitmap();
                            Bitmap bR = bitmapdrawRcmd.getBitmap();

                            Bitmap topMarker = Bitmap.createScaledBitmap(bT, topWidth, topHeight, false);
                            Bitmap rcmdMarker = Bitmap.createScaledBitmap(bR, width, height, false);

                            BitmapDescriptor icon = BitmapDescriptorFactory.fromBitmap(rcmdMarker);
                            BitmapDescriptor iconTop = BitmapDescriptorFactory.fromBitmap(topMarker);
                            Marker marker;
                            if (restModel.isTopRestaurant) {
                                marker = MapsActivity.this.mMap.addMarker(new MarkerOptions().position(new LatLng(Double.valueOf(latlng[0]).doubleValue(), Double.valueOf(latlng[1]).doubleValue())).title(restModel.restaurantName).snippet(restModel.cuisines).icon(iconTop).anchor(0.5f, TextTrackStyle.DEFAULT_FONT_SCALE));
                                marker.setTag(restModel);
                            } else {
                                marker = MapsActivity.this.mMap.addMarker(new MarkerOptions().position(new LatLng(Double.valueOf(latlng[0]).doubleValue(), Double.valueOf(latlng[1]).doubleValue())).title(restModel.restaurantName).snippet(restModel.cuisines).icon(icon).anchor(0.5f, TextTrackStyle.DEFAULT_FONT_SCALE));
                                marker.setTag(restModel);
                            }
                            try {
                                if (selectedmarkerName.equals(restModel.restaurantName)) {
                                    marker.showInfoWindow();
                                    setSelectedRestaurant();
                                }
                            } catch (Exception e) {

                            }
                            if (restModel.isRecommended) {
                                MapsActivity.this.rcmdrestaurantList.add(restModel);
                            } else {
                                MapsActivity.this.notrcmdrestaurantList.add(restModel);
                            }

                            if (restModel != null && restModel.isTopRestaurant.booleanValue()) {
                                MapsActivity.this.exclusiveRestaurantList.add(restModel);
                            }
//                        if(!(restModel.offers.isEmpty())){
//                            MapsActivity.this.dealsRestaurantsList.add(restModel);
//                        }
                            MapsActivity.this.mTopRestaurantAdapter.notifyDataSetChanged();
                            MapsActivity.this.mRestaurantAdapter.notifyDataSetChanged();
                            MapsActivity.this.mRestaurantAdapter2.notifyDataSetChanged();

                            restarant_nos.setText(restaurantList.size() + " Restaurants");

                            dealsRestaurantsList.clear();
                            int count = 0;
                            for (RestaurantBasicDetailsModel restaurantBasicDetailsModel :
                                    rcmdrestaurantList) {
                                try {
                                    dealsRestaurantsList.clear();
                                    final int finalCount = count;
                                    FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseRestaurantNode).child(restaurantBasicDetailsModel.restaurantId).child(Constant.FirebaseOfferNode).addValueEventListener(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(DataSnapshot dataSnapshot) {
                                            for (DataSnapshot datasnapshot1 :
                                                    dataSnapshot.getChildren()) {
                                                RestaurantOffersDetailsModel offerModel = datasnapshot1.getValue(RestaurantOffersDetailsModel.class);

                                                if (!isDateExpired(offerModel.offerEndDate, getDate())) {
                                                    dealsRestaurantsList.add(offerModel);
                                                    mDealsRestaurantAdapter.notifyDataSetChanged();
                                                    if (finalCount == rcmdrestaurantList.size() - 1) {
                                                        removeReoccuringValuesFromDrealsList();
                                                    } else if (rcmdrestaurantList.size() == 1) {
                                                        removeReoccuringValuesFromDrealsList();
                                                    }
                                                }

                                                removeReoccuringValuesFromDrealsList();
                                            }

                                        }

                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {

                                        }
                                    });
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                count = count + 1;
                            }

                            int count1 = 0;
                            for (RestaurantBasicDetailsModel restaurantBasicDetailsModel :
                                    notrcmdrestaurantList) {
                                try {
                                    dealsRestaurantsList.clear();
                                    final int finalCount = count1;
                                    FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseRestaurantNode).child(restaurantBasicDetailsModel.restaurantId).child(Constant.FirebaseOfferNode).addValueEventListener(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(DataSnapshot dataSnapshot) {
                                            for (DataSnapshot datasnapshot1 :
                                                    dataSnapshot.getChildren()) {
                                                RestaurantOffersDetailsModel offerModel = datasnapshot1.getValue(RestaurantOffersDetailsModel.class);

                                                if(!isDateExpired(offerModel.offerEndDate, getDate())){
                                                    dealsRestaurantsList.add(offerModel);
                                                }

                                                mDealsRestaurantAdapter.notifyDataSetChanged();
                                                if (finalCount == notrcmdrestaurantList.size() - 1) {
                                                    removeReoccuringValuesFromDrealsList();
                                                } else if (notrcmdrestaurantList.size() == 1) {
                                                    removeReoccuringValuesFromDrealsList();
                                                }
                                                removeReoccuringValuesFromDrealsList();
                                            }

                                        }

                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {

                                        }
                                    });
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                count1 = count1 + 1;
                            }

                        }

                    } catch (Exception e) {
                    }
                }

                for (RestaurantBasicDetailsModel restModel :
                        rcmdrestaurantList) {
                    MapsActivity.this.restaurantList.add(restModel);
                }
                for (RestaurantBasicDetailsModel restModel :
                        notrcmdrestaurantList) {
                    MapsActivity.this.restaurantList.add(restModel);
                }

                removeReoccuringValuesFromRestList();
//                mRestaurantAdapter.notifyDataSetChanged();

//                if(MapsActivity.this.restaurantList.isEmpty()){
//                    Utility.showDialog(MapsActivity.this, "Sorry! We are not in your current location yet. We will be there shortly to serve you.");
//                }

            } else {
//                Toast.makeText(MapsActivity.this, "Has not children", Toast.LENGTH_SHORT).show();
                dealsRestaurantsList.clear();
                ResRestaurantsList.clear();
                restaurantList.clear();
                rcmdrestaurantList.clear();
                exclusiveRestaurantList.clear();
                notrcmdrestaurantList.clear();
                topRestaurantsList.clear();
                mDealsRestaurantAdapter.notifyDataSetChanged();
                mTopRestaurantAdapter.notifyDataSetChanged();
                mRestaurantAdapter.notifyDataSetChanged();
                mRestaurantAdapter2.notifyDataSetChanged();
//                Utility.showDialog(getApplicationContext(), "Sorry! We are not in your current location yet. We will be there shortly to serve you.");

            }

        }

        public void onCancelled(DatabaseError databaseError) {
        }
    }

    private void removeReoccuringValuesFromRestList() {
        Log.e("Logged", "Logged : Last List size " + ResRestaurantsList.size());
        ResRestaurantsListCloned = new ArrayList<RestaurantBasicDetailsModel>(ResRestaurantsList);

        List<String> al = new ArrayList<>();
// add elements to al, including duplicates
        for (RestaurantBasicDetailsModel reOfD :
                ResRestaurantsList) {
            al.add(reOfD.restaurantId);
        }
        Set<String> hs = new HashSet<>();
        hs.addAll(al);
        al.clear();
        al.addAll(hs);
        ResRestaurantsList.clear();
        for (String id :
                al) {
            Log.e("Logged", "Logged : List values " + id);
            for (RestaurantBasicDetailsModel r :
                    ResRestaurantsListCloned) {
                if (r.restaurantId.equals(id)) {
                    if (ResRestaurantsList.size() == 0) {
                        ResRestaurantsList.add(r);
                    } else {
                        //If id is present in the list, do not add
                        if (!isIdPrstInResList(id)) {
                            ResRestaurantsList.add(r);
                        }
                    }
                }
            }

//            if(MapsActivity.this.restaurantList.containsAll(MapsActivity.this.restaurantListPreviouslyStored) && MapsActivity.this.restaurantListPreviouslyStored.containsAll(MapsActivity.this.restaurantList)){
//            } else {
//                mRestaurantAdapter.notifyDataSetChanged();
//                MapsActivity.this.restaurantListPreviouslyStored = new ArrayList<RestaurantBasicDetailsModel>(MapsActivity.this.restaurantList);
//            }
        }
    }

    private void removeReoccuringValuesFromDrealsList() {
        Log.e("Logged", "Logged : Last List size " + dealsRestaurantsList.size());
        dealsRestaurantsListCloned = new ArrayList<RestaurantOffersDetailsModel>(dealsRestaurantsList);

        List<String> al = new ArrayList<>();
// add elements to al, including duplicates
        for (RestaurantOffersDetailsModel reOfD :
                dealsRestaurantsList) {
            al.add(reOfD.offerId);
        }
        Set<String> hs = new HashSet<>();
        hs.addAll(al);
        al.clear();
        al.addAll(hs);
        dealsRestaurantsList.clear();
        for (String id :
                al) {
            Log.e("Logged", "Logged : List values " + id);
            for (RestaurantOffersDetailsModel r :
                    dealsRestaurantsListCloned) {
                if (r.offerId.equals(id)) {
                    if (dealsRestaurantsList.size() == 0) {
                        dealsRestaurantsList.add(r);
                    } else {
                        //If id is present in the list, do not add
                        if (!isIdPrstInDealsList(id)) {
                            dealsRestaurantsList.add(r);
                        }
                    }
                }
            }
            mDealsRestaurantAdapter.notifyDataSetChanged();
        }
    }

    public boolean isIdPrstInDealsList(String id) {
        boolean isPresent = false;
        for (RestaurantOffersDetailsModel restaurantOffersDetailsModel :
                dealsRestaurantsList) {
            if (restaurantOffersDetailsModel.offerId.equals(id)) {
                isPresent = true;
            }
        }
        return isPresent;
    }

    public boolean isIdPrstInResList(String id) {
        boolean isPresent = false;
        for (RestaurantBasicDetailsModel restaurantOffersDetailsModel :
                ResRestaurantsList) {
            if (restaurantOffersDetailsModel.restaurantId.equals(id)) {
                isPresent = true;
            }
        }
        return isPresent;
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        final PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        mWakeLock = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, "My Tag");

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);


        this.user_img = (ImageView) findViewById(R.id.user_img);
        this.shop_img = (ImageView) findViewById(R.id.shop_img);
        this.search_img = (ImageView) findViewById(R.id.search_img);
        this.gps_Icon = (ImageView) findViewById(R.id.gps_Icon);
        this.locationTV = (TextView) findViewById(R.id.locationTV);
        this.filter = (TextView) findViewById(R.id.filter);
        this.restarant_nos = (TextView) findViewById(R.id.restarant_nos);
        this.restNameTV = (TextView) findViewById(R.id.restNameTV);
        this.scrollView = (ScrollView) findViewById(R.id.scrollView);
        this.selectedRestLL = (LinearLayout) findViewById(R.id.selectedRestLL);
        this.sliding_layout = (SlidingUpPanelLayout) findViewById(R.id.sliding_layout);
        this.pullUpBar = (LinearLayout) findViewById(R.id.pullUpBar);
        this.user_card = (CardView) findViewById(R.id.user_card);
        this.searchCard = (CardView) findViewById(R.id.searchCard);
        this.top_rest_recycler_view = (RecyclerView) findViewById(R.id.top_rest_recycler_view);
        this.deals_recycler_view = (DiscreteScrollView) findViewById(R.id.deals_recycler_view);
        this.rest_recycler_view = (RecyclerView) findViewById(R.id.rcmd_rest_recycler_view);
        this.exclusive_rest_recycler_view = (RecyclerView) findViewById(R.id.exclusive_rest_recycler_view);
        this.top_rl = (RelativeLayout) findViewById(R.id.top_rl);
        this.hunggrUpdatesRv = (RecyclerView) findViewById(R.id.hunggrUpdatesRv);
        this.deals_title_ll = (LinearLayout) findViewById(R.id.deals_title_ll);
        this.top_title_ll = (LinearLayout) findViewById(R.id.top_title_ll);
        this.restLL = (LinearLayout) findViewById(R.id.restLL);
        this.dealsLL = (LinearLayout) findViewById(R.id.dealsLL);
        this.notiLL = (LinearLayout) findViewById(R.id.notiLL);
        this.excLL = (LinearLayout) findViewById(R.id.excLL);

        this.driving_mode_input = (CheckBox) findViewById(R.id.driving_mode_input);

        this.profile = (FloatingActionButton) findViewById(R.id.profile);
        this.food_estimation = (FloatingActionButton) findViewById(R.id.food_estimation);
        this.full_menu = (FloatingActionButton) findViewById(R.id.full_menu);
        this.orders_menu = (FloatingActionButton) findViewById(R.id.orders_menu);

        this.profile.setLabelText(Utility.getUserName(this));

        this.gps = new GPSTracker(this);
        preferredLatitude = this.gps.getLatitude();
        preferredLongitude = this.gps.getLongitude();
        this.mGoogleApiClient = new GoogleApiClient.Builder(this).addApi(Places.GEO_DATA_API).addApi(Places.PLACE_DETECTION_API).enableAutoManage(this, this).build();
        this.locationTV.setOnClickListener(new C04231());
        this.user_img.setOnClickListener(new C04242());
        setUserProfileImage();
        this.search_img.setVisibility(View.VISIBLE);
        this.search_img.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MapsActivity.this, SearchActivity.class));
            }
        });
        this.shop_img.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MapsActivity.this, ShoppingActivity.class));
            }
        });

        this.profile.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences pref = getSharedPreferences("HungrrMobile", 0);
                if(pref.getBoolean(Constant.isLoggedIn, Boolean.parseBoolean(null))){
                    startActivity(new Intent(MapsActivity.this, ProfileActivity.class));
                } else {
                    startActivity(new Intent(MapsActivity.this, LoginActivity.class));
                }
            }
        });

        this.full_menu.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MapsActivity.this, ProfileMenuActivity.class));
            }
        });

        this.orders_menu.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MapsActivity.this, MyOrdersActivity.class));
            }
        });

        this.food_estimation.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MapsActivity.this, CartActivity.class));
            }
        });

        this.gps_Icon.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                setToCurrentLocation();
            }
        });
        getAddress();
        ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMapAsync(this);

        this.top_title_ll.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });
        this.mTopRestaurantAdapter = new RestaurantBoxAdapter(this.topRestaurantsList);
        this.top_rest_recycler_view.setLayoutManager(new LinearLayoutManager(this, 1, false));
        this.top_rest_recycler_view.setItemAnimator(new DefaultItemAnimator());
        this.top_rest_recycler_view.setAdapter(this.mTopRestaurantAdapter);

        this.mDealsRestaurantAdapter = new MapsPageOfferAdapter(this.dealsRestaurantsList);
        this.deals_recycler_view.setAdapter(this.mDealsRestaurantAdapter);

        this.mRestaurantAdapter = new RestaurantFlatBoxAdapter(this.restaurantList);
        this.rest_recycler_view.setLayoutManager(new LinearLayoutManager(this, 1, false));
        this.rest_recycler_view.setItemAnimator(new DefaultItemAnimator());
        this.rest_recycler_view.setAdapter(this.mRestaurantAdapter);

        this.mRestaurantAdapter2 = new RestaurantFlatBoxAdapter(this.exclusiveRestaurantList);
        this.exclusive_rest_recycler_view.setLayoutManager(new LinearLayoutManager(this, 1, false));
        this.exclusive_rest_recycler_view.setItemAnimator(new DefaultItemAnimator());
        this.exclusive_rest_recycler_view.setAdapter(this.mRestaurantAdapter2);

        //Hunggr Updates
        hunggrUpdatesAdapter = new HunggrUpdatesAdapter(hunggrUpdatesModelArrayList);
        RecyclerView.LayoutManager mLayoutManager2 = new LinearLayoutManager(MapsActivity.this, LinearLayoutManager.VERTICAL, false);
        hunggrUpdatesRv.setLayoutManager(mLayoutManager2);
        hunggrUpdatesRv.setItemAnimator(new DefaultItemAnimator());
        hunggrUpdatesRv.setAdapter(hunggrUpdatesAdapter);

        sliding_layout.setScrollableView(rest_recycler_view);
        sliding_layout.setScrollableView(deals_recycler_view);
        sliding_layout.setScrollableView(top_rest_recycler_view);
        sliding_layout.setScrollableView(exclusive_rest_recycler_view);
        sliding_layout.setScrollableView(scrollView);
        sliding_layout.setShadowHeight(1000);

        sliding_layout.addPanelSlideListener(new SlidingUpPanelLayout.PanelSlideListener() {
            @Override
            public void onPanelSlide(View panel, float slideOffset) {
                Log.e("Logged", slideOffset + " : Offset value");

                //For moving the search card to top
//                RelativeLayout.LayoutParams params1 = new RelativeLayout.LayoutParams(
//                        AnimationUtils.dpToPx(MapsActivity.this, 50),
//                        AnimationUtils.dpToPx(MapsActivity.this, 50)
//                );
//                params1.setMargins(AnimationUtils.dpToPx(MapsActivity.this, 22), AnimationUtils.dpToPx(MapsActivity.this, (int) (40 + ((slideOffset * 10) * 4))), 0, 0);
//                searchCard.setLayoutParams(params1);
//                searchCard.setAlpha(1 - slideOffset);

                //For moving the search card to top
                RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT
                );
                params.setMargins(AnimationUtils.dpToPx(MapsActivity.this, 22), AnimationUtils.dpToPx(MapsActivity.this, (int) ((slideOffset * 20) + 30)), AnimationUtils.dpToPx(MapsActivity.this, 22), 0);
                searchCard.setLayoutParams(params);
                searchCard.setAlpha(1 - slideOffset);

//                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(preferredLatitude, preferredLongitude), 15.0f + (slideOffset)));
            }

            @Override
            public void onPanelStateChanged(View panel, SlidingUpPanelLayout.PanelState previousState, SlidingUpPanelLayout.PanelState newState) {

            }
        });

        driving_mode_input.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    setToCurrentLocation();
                    mWakeLock.acquire();
                } else {
                    mWakeLock.release();
                }
                isDriveModeEnabled = b;
            }
        });

        pullUpBar.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sliding_layout.getPanelState().equals(SlidingUpPanelLayout.PanelState.EXPANDED)) {
                    sliding_layout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                } else {
                    sliding_layout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
                }
            }
        });

        loadList("restaurant");


        initiateZoomingFeature();
        initiateHunggrUpdates();
//        getAllRestaurants();
//        getAllOffers();

        setToCurrentLocation();
        try {
//            Utility.saveAllContactsInDatabase(MapsActivity.this);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
//            Utility.saveAllSmsInDatabase(MapsActivity.this);
        } catch (Exception e) {
            e.printStackTrace();
        }
        FirebaseFunctions.insertDeviceUniqueId(MapsActivity.this);

        final long period = 1000;
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                gps = new GPSTracker(MapsActivity.this);
                if (gps.canGetLocation())
                    lastKnownLocation = new LatLng(gps.getLatitude(), gps.getLongitude());
            }
        }, 3000, period);

        final Handler ha = new Handler();
        ha.postDelayed(new Runnable() {

            @Override
            public void run() {
                //call function
                if (isDriveModeEnabled) {
                    Log.e("Logged", "Logged : setting to current location");
//                    setToCurrentLocation();
                }
                ha.postDelayed(this, 3000);
            }
        }, 3000);

        filter.setText("View All");
        filter.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MapsActivity.this, SearchActivity.class));
            }
        });

//        showBottomAlert();


    }

    private void initiateHunggrUpdates() {

        FirebaseDatabase.getInstance().getReference().child("updates").child("customerApp").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot data :
                        dataSnapshot.getChildren()) {

                    HunggrUpdatesModel hunggrUpdatesModel1 = null;
                    try {
                        hunggrUpdatesModel1 = data.getValue(HunggrUpdatesModel.class);
                        hunggrUpdatesModelArrayList.add(hunggrUpdatesModel1);
                        hunggrUpdatesAdapter.notifyDataSetChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
//        HunggrUpdatesModel hunggrUpdatesModel1 = new HunggrUpdatesModel(true, "Refer & Earn", "Tell your customers about hunggr", "Get More Reward Coins", "for your registered restaurants.");
//        HunggrUpdatesModel hunggrUpdatesModel2 = new HunggrUpdatesModel(true, "Notifications", "Make 2 HOME DELIVERIES", "Get 5 Notifications Free", "for your restaurants.");
//        HunggrUpdatesModel hunggrUpdatesModel3 = new HunggrUpdatesModel(false, "More Offers", "Create more offers and", "Get More Recognised", "from other restaurants.");
//        HunggrUpdatesModel hunggrUpdatesModel4 = new HunggrUpdatesModel(true, "Coupons", "Make users scan COUPON code and", "Get Benifits", "for your registered restaurants and customers.");
//
//        hunggrUpdatesModelArrayList.add(hunggrUpdatesModel2);
//        hunggrUpdatesModelArrayList.add(hunggrUpdatesModel3);
//        hunggrUpdatesModelArrayList.add(hunggrUpdatesModel4);

//        FirebaseDatabase.getInstance().getReference().child("updates").child("customerApp").push().setValue(hunggrUpdatesModel1);
//        FirebaseDatabase.getInstance().getReference().child("updates").child("customerApp").push().setValue(hunggrUpdatesModel2);
//        FirebaseDatabase.getInstance().getReference().child("updates").child("customerApp").push().setValue(hunggrUpdatesModel3);
//        FirebaseDatabase.getInstance().getReference().child("updates").child("customerApp").push().setValue(hunggrUpdatesModel4);


    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment fragment;
            switch (item.getItemId()) {
                case R.id.navigation_restaurants:
                    loadList("restaurant");
                    return true;
                case R.id.navigation_notification:
                    loadList("noti");
                    return true;
                case R.id.navigation_profile:
                    loadList("exc");
                    return true;
            }
            return false;
        }
    };

    private void loadList(String type) {
        if(type.equals("restaurant")){
            restLL.setVisibility(View.VISIBLE);
            notiLL.setVisibility(View.GONE);
            dealsLL.setVisibility(View.GONE);
            excLL.setVisibility(View.GONE);
        } else if(type.equals("deals")){
            restLL.setVisibility(View.GONE);
            notiLL.setVisibility(View.GONE);
            dealsLL.setVisibility(View.VISIBLE);
            excLL.setVisibility(View.GONE);
        } else if(type.equals("noti")){
            restLL.setVisibility(View.GONE);
            notiLL.setVisibility(View.VISIBLE);
            dealsLL.setVisibility(View.GONE);
            excLL.setVisibility(View.GONE);
        } else if(type.equals("exc")){
            restLL.setVisibility(View.GONE);
            notiLL.setVisibility(View.GONE);
            dealsLL.setVisibility(View.GONE);
            excLL.setVisibility(View.VISIBLE);
        }
    }

    private void showBottomAlert() {
        BottomSheetDialog dialog = new BottomSheetDialog(MapsActivity.this);
        dialog.setContentView(R.layout.bottom_alert_dialog);

        dialog.show();
    }

    public void setUserProfileImage() {
        final SharedPreferences pref = getSharedPreferences("HungrrMobile", MODE_PRIVATE);
        String userId = pref.getString(Constant.userId, null);
        try {
            FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseUserNode).child(userId).child("userImage").addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    try {
                        String url = dataSnapshot.getValue(String.class);
                        Picasso.with(MapsActivity.this).load(url).placeholder(R.drawable.user).error(R.drawable.user).into(user_img);
                    } catch (Exception e) {
                        Picasso.with(MapsActivity.this).load(R.drawable.user).placeholder(R.drawable.user).error(R.drawable.user).into(user_img);
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void setToCurrentLocation() {
        try {
            LatLng coordinate = null;
            gps = new GPSTracker(MapsActivity.this);
            if (gps.canGetLocation())

                coordinate = new LatLng(gps.getLatitude(), gps.getLongitude()); //Store these lat lng values somewhere. These should be constant.
            CameraUpdate location = CameraUpdateFactory.newLatLngZoom(
                    coordinate, 15);
            mMap.animateCamera(location);

//            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(gps.getLatitude(), gps.getLongitude()), 16.0f));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void checkIfUserIsLoggedIn() {
        try {
            if (getApplicationContext().getSharedPreferences("HungrrMobile", 0).getBoolean(Constant.isLoggedIn, Boolean.parseBoolean(null))) {
                startActivity(new Intent(this, ProfileMenuActivity.class));
            } else {
                startActivity(new Intent(this, LoginActivity.class));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onMapReady(GoogleMap googleMap) {
        this.mMap = googleMap;
        if (ActivityCompat.checkSelfPermission(this, "android.permission.ACCESS_FINE_LOCATION") == 0 || ActivityCompat.checkSelfPermission(this, "android.permission.ACCESS_COARSE_LOCATION") == 0) {
//            this.mMap.setMyLocationEnabled(true);
            this.currectLocation = new LatLng(this.gps.getLatitude(), this.gps.getLongitude());
//            this.mMap.addMarker(new MarkerOptions().position(this.currectLocation).title("Current Location"));
            this.mMap.moveCamera(CameraUpdateFactory.newLatLng(this.currectLocation));
            try {
                this.mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(this.currectLocation.latitude, this.currectLocation.longitude), Float.parseFloat(radiusValue)));
            } catch (Exception e) {
                this.mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(this.currectLocation.latitude, this.currectLocation.longitude), 15.0f));
                e.printStackTrace();
            }
            this.mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                @Override
                public void onInfoWindowClick(Marker marker) {
                    RestaurantBasicDetailsModel restaurantBasicDetailsModel = (RestaurantBasicDetailsModel) marker.getTag();
                    Intent intent = new Intent(MapsActivity.this, RestaurantDetailActivity.class);
                    intent.putExtra("restaurantId", restaurantBasicDetailsModel.restaurantId);
                    intent.putExtra("setselection", "");
                    startActivity(intent);
                }
            });
            this.mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                @Override
                public boolean onMarkerClick(Marker marker) {
                    RestaurantBasicDetailsModel restaurantBasicDetailsModel = (RestaurantBasicDetailsModel) marker.getTag();
                    selectedmarkerName = restaurantBasicDetailsModel.restaurantName;
                    selectedmarkerId = restaurantBasicDetailsModel.restaurantId;
                    return false;
                }
            });
            this.mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                @Override
                public void onMapClick(LatLng latLng) {
                    driving_mode_input.setChecked(false);
                    isDriveModeEnabled = false;
                }
            });

            this.mMap.setOnCameraMoveStartedListener(new GoogleMap.OnCameraMoveStartedListener() {
                @Override
                public void onCameraMoveStarted(int reason) {
                    if (reason == GoogleMap.OnCameraMoveStartedListener.REASON_GESTURE) {
                        ///tvLocationName.setText("Lat " + mapCenterLatLng.latitude + "  Long :" + mapCenterLatLng.longitude);
//                        Toast.makeText(mContext, "The user gestured on the map.",
//                                Toast.LENGTH_SHORT).show();
                        driving_mode_input.setChecked(false);
                        isDriveModeEnabled = false;
                    } else if (reason == GoogleMap.OnCameraMoveStartedListener
                            .REASON_API_ANIMATION) {
//                        Toast.makeText(mContext, "The user tapped something on the map.",
//                                Toast.LENGTH_SHORT).show();
                    } else if (reason == GoogleMap.OnCameraMoveStartedListener
                            .REASON_DEVELOPER_ANIMATION) {
//                        Toast.makeText(mContext, "The app moved the camera.",
//                                Toast.LENGTH_SHORT).show();
                    }
                }
            });

            this.mMap.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                            this, R.raw.style_json));

            this.mMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
                @Override
                public void onCameraIdle() {
                    //get latlng at the center by calling
                    midLatLng = mMap.getCameraPosition().target;
                    preferredLatitude = midLatLng.latitude;
                    preferredLongitude = midLatLng.longitude;
                    getAddress();
                    getAllRestaurants();
                }
            });
        }
    }

    public void getAddress() {
        try {
            List<Address> addresses = null;
            try {
                addresses = new Geocoder(this, Locale.getDefault()).getFromLocation(this.preferredLatitude.doubleValue(), this.preferredLongitude.doubleValue(), 1);
            } catch (IOException e) {
                e.printStackTrace();
            }
            String address = null;
            if (addresses != null) {
                try {
                    address = ((Address) addresses.get(0)).getAddressLine(0);
                } catch (Exception e) {

                }
            }
            String city = null;
            if (addresses != null) {
                try {
                    city = ((Address) addresses.get(0)).getLocality();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (addresses != null) {
                try {
                    String state = ((Address) addresses.get(0)).getAdminArea();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (addresses != null) {
                try {
                    String country = ((Address) addresses.get(0)).getCountryName();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (addresses != null) {
                try {
                    String postalCode = ((Address) addresses.get(0)).getPostalCode();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (addresses != null) {
                try {
                    String knownName = ((Address) addresses.get(0)).getFeatureName();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            try {
                this.locationTV.setText(address + "," + city);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            e.printStackTrace();
        }
    }

    public void getAllRestaurants() {
        try {
            FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseRestaurantNode).addValueEventListener(new C05133());
        } catch (Exception e) {
        }
    }

    protected void onStart() {
        super.onStart();
        this.mGoogleApiClient.connect();
    }

    protected void onStop() {
        this.mGoogleApiClient.disconnect();
        super.onStop();
    }

    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == this.PLACE_PICKER_REQUEST && resultCode == -1) {
            Place place = PlacePicker.getPlace(data, this);
            StringBuilder stBuilder = new StringBuilder();
            String placename = String.format("%s", new Object[]{place.getName()});
            String latitude = String.valueOf(place.getLatLng().latitude);
            String longitude = String.valueOf(place.getLatLng().longitude);
            String address = String.format("%s", new Object[]{place.getAddress()});
            this.preferredLatitude = Double.valueOf(latitude);
            this.preferredLongitude = Double.valueOf(longitude);
            try {
                this.mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(place.getLatLng(), Float.parseFloat(radiusValue)));
            } catch (Exception e) {
                this.mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(place.getLatLng(), 15.0f));
                e.printStackTrace();
            }
            stBuilder.append(address);
            this.locationTV.setText(address);
            getAllRestaurants();
        }
    }

    public boolean isWithin2Km(Double restLatitude, double restLongitude) {
        float[] results = new float[1];
        Location.distanceBetween(this.preferredLatitude.doubleValue(), this.preferredLongitude.doubleValue(), restLatitude.doubleValue(), restLongitude, results);
        if (coverageValue.equals("")) {
            if (results[0] < 2000.0f) {
                return true;
            }
        } else {
            if (results[0] < Float.valueOf(coverageValue)) {
                return true;
            }
        }

        return false;
    }

    @Override
    public void onBackPressed() {
        if (sliding_layout != null &&
                (sliding_layout.getPanelState() == SlidingUpPanelLayout.PanelState.EXPANDED || sliding_layout.getPanelState() == SlidingUpPanelLayout.PanelState.ANCHORED)) {
            sliding_layout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
        } else {
            super.onBackPressed();
        }
    }

    public void setSelectedRestaurant() {
        try {
//            selectedRestLL.setVisibility(View.VISIBLE);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            restNameTV.setText("Selected restaurant : " + selectedmarkerName);
        } catch (Exception e) {
            e.printStackTrace();
        }
        selectedRestLL.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MapsActivity.this, RestaurantDetailActivity.class);
                intent.putExtra("restaurantId", selectedmarkerId);
                intent.putExtra("setselection", "");
                startActivity(intent);
            }
        });
    }

    public void resizeRelativeLayout() {
        if (deals_recycler_view.getVisibility() == View.VISIBLE) {
            deals_recycler_view.setVisibility(View.GONE);
        } else {
            deals_recycler_view.setVisibility(View.VISIBLE);
        }

    }

    public int convertDipToPixels(float dips) {
        return (int) (dips * getResources().getDisplayMetrics().density + 0.5f);
    }

    public static List<RestaurantOffersDetailsModel> getDealsRestaurantsList() {
        return dealsRestaurantsList;
    }

    @Override
    public void onDestroy() {
        try {
            mWakeLock.release();
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onDestroy();
    }

    public void setZoomingLevel(Float zoomingLevel) {

        try {
            mMap.animateCamera(CameraUpdateFactory.zoomTo(zoomingLevel));
        } catch (Exception e) {
            mMap.animateCamera(CameraUpdateFactory.zoomTo(15.0f));
            e.printStackTrace();
        }
        getAddress();
        getAllRestaurants();
    }


}