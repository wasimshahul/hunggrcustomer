package com.hunggrmy.hunggrmy;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hunggrmy.hunggrmy.utils.Constant;
import com.hunggrmy.hunggrmy.utils.Utility;

public class ProfileMenuActivity extends AppCompatActivity {

    TextView couponTV;
    TextView estTV;
    LinearLayout logoutLL;
    TextView offerTV;
    TextView pointsTV;
    TextView bookingsTV;
    TextView ordersTV;
    TextView profileTV;
    TextView tcTV, drTV, ctTv;
    TextView versionTV;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_menu);

        couponTV = (TextView) findViewById(R.id.couponTV);
        estTV = (TextView) findViewById(R.id.estTV);
        logoutLL = (LinearLayout) findViewById(R.id.logoutLL);
        offerTV = (TextView) findViewById(R.id.offerTV);
        profileTV = (TextView) findViewById(R.id.profileTV);
        pointsTV = (TextView) findViewById(R.id.pointsTV);
        ordersTV = (TextView) findViewById(R.id.ordersTV);
        bookingsTV = (TextView) findViewById(R.id.bookingsTV);
        tcTV = (TextView) findViewById(R.id.tcTV);
        drTV = (TextView) findViewById(R.id.drTV);
        ctTv = (TextView) findViewById(R.id.ctTV);
        versionTV = (TextView) findViewById(R.id.versionTV);

        final SharedPreferences pref = getSharedPreferences("HungrrMobile", 0);

        if(!pref.getBoolean(Constant.isLoggedIn, Boolean.parseBoolean(null))) {
            logoutLL.setVisibility(View.GONE);
        }

        estTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ProfileMenuActivity.this, CartActivity.class));
            }
        });

        couponTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ProfileMenuActivity.this, MyCouponsActivity.class));
            }
        });

        ordersTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(pref.getBoolean(Constant.isLoggedIn, Boolean.parseBoolean(null))){
                    startActivity(new Intent(ProfileMenuActivity.this, MyOrdersActivity.class));
                } else {
                    startActivity(new Intent(ProfileMenuActivity.this, LoginActivity.class));
                }
            }
        });

        versionTV.setText("Version : "+Utility.getAppVersionDetails(ProfileMenuActivity.this).versionName);

        logoutLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences.Editor editor = ProfileMenuActivity.this.getApplicationContext().getSharedPreferences("HungrrMobile", 0).edit();
                editor.putString(Constant.userId, "");
                editor.putBoolean(Constant.isLoggedIn, false);
                editor.commit();
                Toast.makeText(ProfileMenuActivity.this, "Logged Out Successfully", Toast.LENGTH_SHORT).show();
                finish();
            }
        });

        offerTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ProfileMenuActivity.this, MyOffersActivity.class));
            }
        });

        profileTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences pref = getSharedPreferences("HungrrMobile", 0);
                if(pref.getBoolean(Constant.isLoggedIn, Boolean.parseBoolean(null))){
                    startActivity(new Intent(ProfileMenuActivity.this, ProfileActivity.class));
                } else {
                    startActivity(new Intent(ProfileMenuActivity.this, LoginActivity.class));
                }
            }
        });

        pointsTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ProfileMenuActivity.this, MyPointsActivity.class));
            }
        });

        bookingsTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences pref = getSharedPreferences("HungrrMobile", 0);
                if (pref.getBoolean(Constant.isLoggedIn, Boolean.parseBoolean(null))) {
                    try {
                        startActivity(new Intent(ProfileMenuActivity.this, MyBookingsActivity.class));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    startActivity(new Intent(ProfileMenuActivity.this, LoginActivity.class));
                }
            }
        });

        tcTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ProfileMenuActivity.this, TermsAndConditionActivity.class));
            }
        });

        drTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent( ProfileMenuActivity.this, WebPageActivity.class);
//                intent.putExtra("url", "https://www.androidhive.info/2016/12/android-working-with-webview-building-a-simple-in-app-browser/");
                intent.putExtra("url", "http://hunggr.my/delnrefundpolicy.html");
                startActivity(intent);
            }
        });

        ctTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ProfileMenuActivity.this, WebPageActivity.class);
                intent.putExtra("url", "http://hunggr.my/contactus.html");
                startActivity(intent);
            }
        });
    }
}
