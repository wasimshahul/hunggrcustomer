package com.hunggrmy.hunggrmy.fragments;

import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.hunggrmy.hunggrmy.MainActivity;
import com.hunggrmy.hunggrmy.R;
import com.hunggrmy.hunggrmy.adapters.HunggrUpdatesAdapter;
import com.hunggrmy.hunggrmy.adapters.MapsPageOfferAdapter;
import com.hunggrmy.hunggrmy.adapters.RestaurantFlatBoxAdapter;
import com.hunggrmy.hunggrmy.models.HunggrUpdatesModel;
import com.hunggrmy.hunggrmy.models.RestaurantBasicDetailsModel;
import com.hunggrmy.hunggrmy.models.RestaurantOffersDetailsModel;
import com.hunggrmy.hunggrmy.utils.Constant;
import com.hunggrmy.hunggrmy.utils.GPSTracker;

import java.util.ArrayList;
import java.util.List;

import static com.hunggrmy.hunggrmy.MapsActivity.coverageValue;
import static com.hunggrmy.hunggrmy.utils.Utility.getDate;
import static com.hunggrmy.hunggrmy.utils.Utility.isDateExpired;

/**
 * Created by Wasim on 5/19/2018.
 */

public class NotificationListFragment extends Fragment {

    private ArrayList<HunggrUpdatesModel> hunggrUpdatesModelArrayList = new ArrayList<>();
    private HunggrUpdatesAdapter hunggrUpdatesAdapter;
    private RecyclerView hunggrUpdatesRv;
    LinearLayout title2;
    private TextView noValueText;
    private ProgressBar loadingProgressBar;

    public NotificationListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_updates_list, container, false);
        hunggrUpdatesRv = (RecyclerView) rootView.findViewById(R.id.hunggrUpdatesRv);
        title2 = (LinearLayout) rootView.findViewById(R.id.title2);
        loadingProgressBar = (ProgressBar) rootView.findViewById(R.id.loadingProgressBar);
        noValueText = (TextView) rootView.findViewById(R.id.noValueText);

        hunggrUpdatesAdapter = new HunggrUpdatesAdapter(hunggrUpdatesModelArrayList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        hunggrUpdatesRv.setLayoutManager(mLayoutManager);
        hunggrUpdatesRv.setItemAnimator(new DefaultItemAnimator());
        hunggrUpdatesRv.setAdapter(hunggrUpdatesAdapter);

        showRestaurants();

        return rootView;
    }

    public void showRestaurants() {

        title2.setVisibility(View.GONE);
        loadingProgressBar.setVisibility(View.VISIBLE);
        noValueText.setVisibility(View.GONE);
        hunggrUpdatesRv.setVisibility(View.GONE);

        try {
            FirebaseDatabase.getInstance().getReference().child("updates").child("customerApp").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    for (DataSnapshot data :
                            dataSnapshot.getChildren()) {

                        HunggrUpdatesModel hunggrUpdatesModel1 = null;
                        try {
                            hunggrUpdatesModel1 = data.getValue(HunggrUpdatesModel.class);
                            hunggrUpdatesModelArrayList.add(hunggrUpdatesModel1);
                            hunggrUpdatesAdapter.notifyDataSetChanged();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }

                    if (hunggrUpdatesModelArrayList.isEmpty()) {
                        title2.setVisibility(View.VISIBLE);
                        loadingProgressBar.setVisibility(View.GONE);
                        noValueText.setVisibility(View.VISIBLE);
                        hunggrUpdatesRv.setVisibility(View.GONE);
                    } else {
                        title2.setVisibility(View.VISIBLE);
                        loadingProgressBar.setVisibility(View.GONE);
                        noValueText.setVisibility(View.GONE);
                        hunggrUpdatesRv.setVisibility(View.VISIBLE);
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

//        HunggrUpdatesModel hunggrUpdatesModel1 = new HunggrUpdatesModel(true, "Refer & Earn", "Tell your customers about hunggr", "Get More Reward Coins", "for your registered restaurants.", "www");
//        HunggrUpdatesModel hunggrUpdatesModel2 = new HunggrUpdatesModel(true, "Notifications", "Make 2 HOME DELIVERIES", "Get 5 Notifications Free", "for your restaurants.", "www");
//        HunggrUpdatesModel hunggrUpdatesModel3 = new HunggrUpdatesModel(false, "More Offers", "Create more offers and", "Get More Recognised", "from other restaurants.", "www");
//        HunggrUpdatesModel hunggrUpdatesModel4 = new HunggrUpdatesModel(true, "Coupons", "Make users scan COUPON code and", "Get Benifits", "for your registered restaurants and customers.", "www");
//
////        hunggrUpdatesModelArrayList.add(hunggrUpdatesModel2);
////        hunggrUpdatesModelArrayList.add(hunggrUpdatesModel3);
////        hunggrUpdatesModelArrayList.add(hunggrUpdatesModel4);
//
//        FirebaseDatabase.getInstance().getReference().child("updates").child("customerApp").push().setValue(hunggrUpdatesModel1);
//        FirebaseDatabase.getInstance().getReference().child("updates").child("customerApp").push().setValue(hunggrUpdatesModel2);
//        FirebaseDatabase.getInstance().getReference().child("updates").child("customerApp").push().setValue(hunggrUpdatesModel3);
//        FirebaseDatabase.getInstance().getReference().child("updates").child("customerApp").push().setValue(hunggrUpdatesModel4);


    }

    @Override
    public void onResume() {
        super.onResume();
    }
}