package com.hunggrmy.hunggrmy.fragments;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.google.zxing.WriterException;
import com.hunggrmy.hunggrmy.LoginActivity;
import com.hunggrmy.hunggrmy.R;
import com.hunggrmy.hunggrmy.RestaurantDetailActivity;
import com.hunggrmy.hunggrmy.adapters.CouponAdapter;
import com.hunggrmy.hunggrmy.models.RestaurantOffersDetailsModel;
import com.hunggrmy.hunggrmy.utils.Constant;
import com.yarolegovich.discretescrollview.DiscreteScrollView;
import com.yarolegovich.discretescrollview.transform.Pivot;
import com.yarolegovich.discretescrollview.transform.ScaleTransformer;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

import androidmads.library.qrgenearator.QRGContents;
import androidmads.library.qrgenearator.QRGEncoder;

/**
 * Created by Wasim on 5/19/2018.
 */

public class RestaurantCouponFragment extends Fragment {

    EditText offer_title_input, offer_condition_input, offer_start_date_input, offer_end_date_input, offer_percentage_input, offer_desc_input;
    LinearLayout submit_btn, coupon_LL;
    ImageView coupon_img_input;
    TextView noresultTV;

    String offerImage = "";
    String offerId = "";
    boolean isUploadingImage = false;

    //ForShowing Existing Offers
//    private List<RestaurantCouponDetailsModel> offersList = new ArrayList<>();
    private List<String> offersList = new ArrayList<>();
    private DiscreteScrollView offer_recycler_view;
    private CouponAdapter mAdapter;

    public RestaurantCouponFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_restaurant_coupon, container, false);
        offer_recycler_view = (DiscreteScrollView) rootView.findViewById(R.id.offer_recycler_view);
        offer_title_input = (EditText) rootView.findViewById(R.id.offer_title_input);
        offer_condition_input = (EditText) rootView.findViewById(R.id.offer_condition_input);
        offer_start_date_input = (EditText) rootView.findViewById(R.id.offer_start_date_input);
        offer_end_date_input = (EditText) rootView.findViewById(R.id.offer_end_date_input);
        offer_desc_input = (EditText) rootView.findViewById(R.id.offer_desc_input);
        offer_percentage_input = (EditText) rootView.findViewById(R.id.offer_percentage_input);
        coupon_img_input = (ImageView) rootView.findViewById(R.id.coupon_img_input);
        coupon_LL = (LinearLayout) rootView.findViewById(R.id.coupon_LL);
        noresultTV = (TextView) rootView.findViewById(R.id.noresultTV);
        submit_btn = (LinearLayout) rootView.findViewById(R.id.submit_btn);

        //Initialising menu
        mAdapter = new CouponAdapter(offersList, RestaurantCouponFragment.this);
//        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
//        offer_recycler_view.setLayoutManager(mLayoutManager);
//        offer_recycler_view.setItemAnimator(new DefaultItemAnimator());
        offer_recycler_view.setAdapter(mAdapter);
        showExistingOffers();

        offer_recycler_view.setItemTransformer(new ScaleTransformer.Builder()
                .setMaxScale(1.05f)
                .setMinScale(0.8f)
                .setPivotX(Pivot.X.CENTER) // CENTER is a default one
                .setPivotY(Pivot.Y.BOTTOM) // CENTER is a default one
                .build());

        coupon_img_input.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                Toast.makeText(getContext(), "Downloading QR Code", Toast.LENGTH_SHORT).show();
                return false;
            }
        });

        submit_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addOffers();
            }
        });

        return rootView;
    }

    private void createQrCode(final String restaurantId, final String userId, final String couponId) {
        // Initializing the QR Encoder with your value to be encoded, type you required and Dimension
        final String inputData = "resId:"+restaurantId+",userId:"+userId+",couponId:"+couponId;
        QRGEncoder qrgEncoder = new QRGEncoder(inputData, null, QRGContents.Type.TEXT, 250);
        try {
            // Getting QR-Code as Bitmap
            final Bitmap bitmap = qrgEncoder.encodeAsBitmap();
            // Setting Bitmap to ImageView
            coupon_img_input.setImageBitmap(bitmap);
            coupon_img_input.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(getContext(), "Long press to download the coupon", Toast.LENGTH_SHORT).show();
                }
            });
            coupon_img_input.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                    byte[] data = baos.toByteArray();
                    StorageReference mStorageRef;
                    mStorageRef = FirebaseStorage.getInstance().getReference();
                    StorageReference riversRef = mStorageRef.child(userId+"/coupons/"+userId+restaurantId+".jpg");

                    riversRef.putBytes(data)
                            .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                @Override
                                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                    // Get a URL to the uploaded content
                                    Uri downloadUrl = taskSnapshot.getDownloadUrl();
                                    FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseUserNode).child(userId).child(Constant.FirebaseCouponNode).child(couponId).setValue(downloadUrl+"");
                                    Toast.makeText(getContext(), "Coupon saved on your account", Toast.LENGTH_SHORT).show();
                                }
                            })
                            .addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception exception) {
                                    // Handle unsuccessful uploads
                                    // ...
                                }
                            });
                    return false;
                }
            });
        } catch (WriterException e) {
            Log.v("RestaurantCoupon", e.toString());
        }
    }

    public void setCoupon(final String couponId1) {

        if (getContext().getSharedPreferences("HungrrMobile", 0).getBoolean(Constant.isLoggedIn, Boolean.parseBoolean(null))) {
            try {
                FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseRestaurantNode).child(Constant.FirebaseCouponNode).child(couponId1).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        coupon_LL.setVisibility(View.VISIBLE);
                        noresultTV.setVisibility(View.GONE);
                        RestaurantOffersDetailsModel offerModel = dataSnapshot.getValue(RestaurantOffersDetailsModel.class);
                        try {
                            offerImage = offerModel.offerImage;
                            offerId = offerModel.offerId;
                            isUploadingImage = false;
                            offer_title_input.setText(offerModel.offerImageTitle);
                            offer_condition_input.setText(offerModel.offerCondt);
                            offer_start_date_input.setText(offerModel.offerStartDate);
                            offer_end_date_input.setText(offerModel.offerEndDate);
                            offer_percentage_input.setText(offerModel.offerPercentage);
                            offer_desc_input.setText(offerModel.offerDesc);
    //                        Glide.with(getContext()).load(offerImage).into(coupon_img_input);
                            createQrCode(((RestaurantDetailActivity) getActivity()).getRestauranntId(), getContext().getSharedPreferences("HungrrMobile", 0).getString(Constant.userId, null), couponId1);

                        } catch (Exception e) {

                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Toast.makeText(getContext(), "Please login to see QR code", Toast.LENGTH_SHORT).show();
            startActivity(new Intent(getContext(), LoginActivity.class));
        }
    }

    public void showExistingOffers() {
        try {
            FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseRestaurantNode).child(((RestaurantDetailActivity) getActivity()).getRestauranntId()).child(Constant.FirebaseCouponNode).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    offersList.clear();
                    for (DataSnapshot datasnapshot1 :
                            dataSnapshot.getChildren()) {
//                        RestaurantCouponDetailsModel offerModel = datasnapshot1.getValue(RestaurantCouponDetailsModel.class);
                        String offerModel = datasnapshot1.getValue(String.class);
                        offersList.add(offerModel);
                        mAdapter.notifyDataSetChanged();
                    }
                    if(offersList.isEmpty()){
                        noresultTV.setVisibility(View.VISIBLE);
                    } else {
                        noresultTV.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void addOffers() {
        if (offerId.equals("")) {
            try {
                offerId = FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseRestaurantNode).child(((RestaurantDetailActivity) getActivity()).getRestauranntId()).child(Constant.FirebaseCouponNode).push().getKey();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        try {
            uploadOfferInFirebase();
        } catch (Exception e) {
            uploadOfferInFirebase();
        }
    }

    public void uploadOfferInFirebase() {
        if (isUploadingImage) {
            Toast.makeText(getContext(), "Please wait, Image is uploading.", Toast.LENGTH_SHORT).show();
        } else {
            RestaurantOffersDetailsModel restaurantOffersDetailsModel = new RestaurantOffersDetailsModel(((RestaurantDetailActivity) getActivity()).getRestauranntId(), offerId, offerImage, offer_title_input.getText().toString(), offer_condition_input.getText().toString(), offer_start_date_input.getText().toString(), offer_end_date_input.getText().toString(), offer_percentage_input.getText().toString(), offer_desc_input.getText().toString());
            try {
                FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseRestaurantNode).child(((RestaurantDetailActivity) getActivity()).getRestauranntId()).child(Constant.FirebaseCouponNode).child(offerId).setValue(restaurantOffersDetailsModel).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Toast.makeText(getContext(), "Offer Added successfully", Toast.LENGTH_SHORT).show();
                        resetPage();
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void resetPage() {
        try {
            offerImage = "";
            offerId = "";
            isUploadingImage = false;
            offer_title_input.setText("");
            offer_condition_input.setText("");
            offer_start_date_input.setText("");
            offer_end_date_input.setText("");
            offer_percentage_input.setText("");
            offer_desc_input.setText("");
            Glide.with(getContext()).load("").into(coupon_img_input);
        } catch (Exception e) {

        }
    }

}