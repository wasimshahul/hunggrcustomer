package com.hunggrmy.hunggrmy.fragments;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.hunggrmy.hunggrmy.MapsActivity;
import com.hunggrmy.hunggrmy.R;
import com.hunggrmy.hunggrmy.RestaurantDetailActivity;
import com.hunggrmy.hunggrmy.SplashScreenActivity;
import com.hunggrmy.hunggrmy.adapters.ReviewAdapter;
import com.hunggrmy.hunggrmy.models.RestaurantReviewModel;
import com.hunggrmy.hunggrmy.utils.Constant;
import com.willy.ratingbar.BaseRatingBar;
import com.willy.ratingbar.ScaleRatingBar;

import java.util.ArrayList;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;
import static com.google.android.gms.internal.zzahn.runOnUiThread;

/**
 * Created by Wasim on 5/19/2018.
 */

public class RestaurantReviewFragment extends Fragment {

    private List<String> reviewList = new ArrayList<>();
    private RecyclerView review_recycler_view;
    private ReviewAdapter mAdapter;

    ImageView submitReview;
    EditText rest_review_edt;
    LinearLayout review_LL;
    TextView coupon_not_used;

//    ScaleRatingBar simpleRatingBar;
    RatingBar ratingBar;

    DatabaseReference reviewReference = FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseRestaurantNode).child(Constant.reviews);
    DatabaseReference ratingReference = FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseRestaurantNode).child(Constant.rating);

    public RestaurantReviewFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View rootView = inflater.inflate(R.layout.fragment_restaurant_review, container, false);

        review_recycler_view = (RecyclerView) rootView.findViewById(R.id.reviews_recycler_view);
        rest_review_edt = (EditText) rootView.findViewById(R.id.rest_review_edt);
        submitReview = (ImageView) rootView.findViewById(R.id.submitReview);
        review_LL = (LinearLayout) rootView.findViewById(R.id.review_LL);
        coupon_not_used = (TextView) rootView.findViewById(R.id.coupon_not_used);
//        simpleRatingBar = (ScaleRatingBar) rootView.findViewById(R.id.simpleRatingBar);
        ratingBar = (RatingBar) rootView.findViewById(R.id.ratingBar);

        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                insertRating(rating);
            }
        });

//        simpleRatingBar.setOnRatingChangeListener(new BaseRatingBar.OnRatingChangeListener() {
//            @Override
//            public void onRatingChange(BaseRatingBar baseRatingBar, final float v) {
//
//                insertRating(v);
////                final Handler ha = new Handler();
////                ha.postDelayed(new Runnable() {
////
////                    @Override
////                    public void run() {
////
////                    }
////                }, 1000);
//            }
//        });

        submitReview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (rest_review_edt.getText().toString().isEmpty()) {
                    rest_review_edt.setError("Please enter your comment");
                } else {
                    insertReview(rest_review_edt.getText().toString());
                }
            }
        });

        //Initialising menu
        mAdapter = new ReviewAdapter(reviewList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        review_recycler_view.setLayoutManager(mLayoutManager);
        review_recycler_view.setItemAnimator(new DefaultItemAnimator());
        review_recycler_view.setAdapter(mAdapter);

        isAnyCouponUsedInThisRestaurant();
        showExistingReviews();
        showCurrentRestaurantRating();

        return rootView;
    }

    void insertRating(final float v) {
        // isRecursionEnable = false; when u want to stop
        // on exception on thread make it true again
        new Thread(new Runnable() {
            @Override
            public void run() {
                // DO your work here
                // get the data
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            //uddate UI
                            final SharedPreferences pref = getContext().getSharedPreferences("HungrrMobile", MODE_PRIVATE);

                            try {
                                ratingReference.child(pref.getString(Constant.userId, null)).child(((RestaurantDetailActivity) getActivity()).restaurantId).setValue(v);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            try {
                                FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseRestaurantNode).child(((RestaurantDetailActivity) getActivity()).restaurantId).child(Constant.rating).child(pref.getString(Constant.userId, null)).setValue(v);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
            }
        }).start();
    }

    public void showCurrentRestaurantRating() {
        try {
            final SharedPreferences pref = getContext().getSharedPreferences("HungrrMobile", MODE_PRIVATE);
            ratingReference.child(pref.getString(Constant.userId, null)).child(((RestaurantDetailActivity) getActivity()).restaurantId).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    float rate = 0;
                    try {
                        rate = dataSnapshot.getValue(float.class);
//                        simpleRatingBar.setRating(rate);
                        ratingBar.setRating(rate);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        } catch (Exception e) {

        }
    }

    public void insertReview(String review) {
        final SharedPreferences pref = getContext().getSharedPreferences("HungrrMobile", MODE_PRIVATE);
        String reviewId = reviewReference.push().getKey();
        RestaurantReviewModel restaurantReviewModel = new RestaurantReviewModel(reviewId, ((RestaurantDetailActivity) getActivity()).getRestauranntId(), pref.getString(Constant.userId, null), review, System.currentTimeMillis() + "");
        try {
            reviewReference.child(reviewId).setValue(restaurantReviewModel);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseRestaurantNode).child(((RestaurantDetailActivity) getActivity()).getRestauranntId()).child(Constant.reviews).push().setValue(reviewId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Toast.makeText(getContext(), "Review submitted successfully...", Toast.LENGTH_SHORT).show();
        rest_review_edt.setText("");
    }

    public void showExistingReviews() {
        try {
            FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseRestaurantNode).child(((RestaurantDetailActivity) getActivity()).getRestauranntId()).child(Constant.reviews).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    reviewList.clear();
                    for (DataSnapshot datasnapshot1 :
                            dataSnapshot.getChildren()) {
                        String reviewId = datasnapshot1.getValue(String.class);
                        reviewList.add(reviewId);
                        mAdapter.notifyDataSetChanged();
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void isAnyCouponUsedInThisRestaurant() {
        final Boolean[] bool = {false};
        if (getContext().getSharedPreferences("HungrrMobile", 0).getBoolean(Constant.isLoggedIn, Boolean.parseBoolean(null))) {
            final String userId = getContext().getSharedPreferences("HungrrMobile", 0).getString(Constant.userId, null);
            try {
                FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseRestaurantNode).child(((RestaurantDetailActivity) getActivity()).getRestauranntId()).child(Constant.FirebaseCouponNode).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        for (DataSnapshot data :
                                dataSnapshot.getChildren()) {
                            Log.e("Coupon", data.getKey());
                            FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseRestaurantNode).child(Constant.FirebaseCouponNode).child(data.getKey()).child(Constant.couponUsedBy).child(userId).addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    bool[0] = dataSnapshot.hasChildren();
                                    if (bool[0]) {
                                        enableReview(true);
                                    } else {
                                        coupon_not_used.setText(R.string.please_use_atleast_one_coupon_in_this_restaurant_to_give_your_reviews);
                                        enableReview(false);
                                    }
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });
                        }

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
//            return bool[0];
        } else {
            coupon_not_used.setText("Please Login to review");
            enableReview(false);
//            return bool[0];
        }
    }

    public void enableReview(boolean canAccess) {
        if (canAccess) {
            review_LL.setVisibility(View.VISIBLE);
            coupon_not_used.setVisibility(View.GONE);
        } else {
            review_LL.setVisibility(View.GONE);
            coupon_not_used.setVisibility(View.VISIBLE);
        }
    }

}