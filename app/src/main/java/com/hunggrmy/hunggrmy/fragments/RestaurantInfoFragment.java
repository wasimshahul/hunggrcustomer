package com.hunggrmy.hunggrmy.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.hunggrmy.hunggrmy.R;
import com.hunggrmy.hunggrmy.RestaurantDetailActivity;
import com.hunggrmy.hunggrmy.adapters.MenuImageAdapter;
import com.hunggrmy.hunggrmy.adapters.RestaurantImageAdapter;
import com.hunggrmy.hunggrmy.models.ImageModel;
import com.hunggrmy.hunggrmy.models.RestaurantBasicDetailsModel;
import com.hunggrmy.hunggrmy.models.RestaurantTimeModel;
import com.hunggrmy.hunggrmy.utils.Constant;
import com.hunggrmy.hunggrmy.utils.Utility;
import com.yarolegovich.discretescrollview.DiscreteScrollView;
import com.yarolegovich.discretescrollview.transform.Pivot;
import com.yarolegovich.discretescrollview.transform.ScaleTransformer;

import java.util.ArrayList;

import static com.hunggrmy.hunggrmy.utils.Utility.convert24HrTimeTo12HrFormat;
import static com.hunggrmy.hunggrmy.utils.Utility.getDay;

/**
 * Created by Wasim on 5/19/2018.
 */

public class RestaurantInfoFragment extends Fragment implements View.OnClickListener {

    String restaurantId;
    String contactPhone, restLatLng, latitude, longitude;
    DatabaseReference databaseReference;
    TextView rest_addr_input, noMenuImgTv;
    RelativeLayout mapView;
    LinearLayout isMonHolidayLL, isTueHolidayLL, isWedHolidayLL, isThuHolidayLL, isFriHolidayLL, isSatHolidayLL, isSunHolidayLL;
    TextView mon_from1, mon_from2, mon_to1, mon_to2;
    TextView tue_from1, tue_from2, tue_to1, tue_to2;
    TextView wed_from1, wed_from2, wed_to1, wed_to2;
    TextView thu_from1, thu_from2, thu_to1, thu_to2;
    TextView fri_from1, fri_from2, fri_to1, fri_to2;
    TextView sat_from1, sat_from2, sat_to1, sat_to2;
    TextView sun_from1, sun_from2, sun_to1, sun_to2;
    TextView rest_mon_isClosed, rest_tue_isClosed, rest_wed_isClosed, rest_thu_isClosed, rest_fri_isClosed, rest_sat_isClosed, rest_sun_isClosed;
    ImageView call, map, share;
    TextView starttime1, endtime1, starttime2, endtime2;
    TextView isOpenTv;

    public static int averageCookingTime = 0;

    private ArrayList<ImageModel> menuImageModelList = new ArrayList<>();
    private DiscreteScrollView menuImagesRecyclerView;
    private RestaurantImageAdapter menuImagesAdapter;

    public RestaurantInfoFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_restaurant_info, container, false);
        restaurantId = ((RestaurantDetailActivity) getActivity()).getRestauranntId();
        databaseReference = FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseRestaurantNode).child(restaurantId);

        initialisedata();
        rest_addr_input = (TextView) rootView.findViewById(R.id.rest_addr_input);
        call = (ImageView) rootView.findViewById(R.id.call);
        map = (ImageView) rootView.findViewById(R.id.map);
        share = (ImageView) rootView.findViewById(R.id.share);
        noMenuImgTv = (TextView) rootView.findViewById(R.id.noMenuImgTv);
        mapView = (RelativeLayout) rootView.findViewById(R.id.mapView);
        menuImagesRecyclerView = (DiscreteScrollView) rootView.findViewById(R.id.menu_image_recycler_view);

        menuImagesRecyclerView.setItemTransformer(new ScaleTransformer.Builder()
                .setMaxScale(1.05f)
                .setMinScale(0.8f)
                .setPivotX(Pivot.X.CENTER) // CENTER is a default one
                .setPivotY(Pivot.Y.BOTTOM) // CENTER is a default one
                .build());

        isMonHolidayLL = (LinearLayout) rootView.findViewById(R.id.isMonHolidayLL);
        isTueHolidayLL = (LinearLayout) rootView.findViewById(R.id.isTueHolidayLL);
        isWedHolidayLL = (LinearLayout) rootView.findViewById(R.id.isWedHolidayLL);
        isThuHolidayLL = (LinearLayout) rootView.findViewById(R.id.isThuHolidayLL);
        isFriHolidayLL = (LinearLayout) rootView.findViewById(R.id.isFriHolidayLL);
        isSatHolidayLL = (LinearLayout) rootView.findViewById(R.id.isSatHolidayLL);
        isSunHolidayLL = (LinearLayout) rootView.findViewById(R.id.isSunHolidayLL);

        rest_mon_isClosed = (TextView) rootView.findViewById(R.id.rest_mon_isClosed);
        rest_tue_isClosed = (TextView) rootView.findViewById(R.id.rest_tue_isClosed);
        rest_wed_isClosed = (TextView) rootView.findViewById(R.id.rest_wed_isClosed);
        rest_thu_isClosed = (TextView) rootView.findViewById(R.id.rest_thu_isClosed);
        rest_fri_isClosed = (TextView) rootView.findViewById(R.id.rest_fri_isClosed);
        rest_sat_isClosed = (TextView) rootView.findViewById(R.id.rest_sat_isClosed);
        rest_sun_isClosed = (TextView) rootView.findViewById(R.id.rest_sun_isClosed);

        starttime1 = (TextView) rootView.findViewById(R.id.starttime1);
        endtime1 = (TextView) rootView.findViewById(R.id.endtime1);
        starttime2 = (TextView) rootView.findViewById(R.id.starttime2);
        endtime2 = (TextView) rootView.findViewById(R.id.endtime2);
        isOpenTv = (TextView) rootView.findViewById(R.id.isOpenTv);

        isMonHolidayLL.setVisibility(View.GONE);
        isTueHolidayLL.setVisibility(View.GONE);
        isWedHolidayLL.setVisibility(View.GONE);
        isThuHolidayLL.setVisibility(View.GONE);
        isFriHolidayLL.setVisibility(View.GONE);
        isSatHolidayLL.setVisibility(View.GONE);
        isSunHolidayLL.setVisibility(View.GONE);

        //Initialising all time textViews
        mon_from1 = (TextView) rootView.findViewById(R.id.mon_from1);
        mon_from2 = (TextView) rootView.findViewById(R.id.mon_from2);
        mon_to1 = (TextView) rootView.findViewById(R.id.mon_to1);
        mon_to2 = (TextView) rootView.findViewById(R.id.mon_to2);

        tue_from1 = (TextView) rootView.findViewById(R.id.tue_from1);
        tue_from2 = (TextView) rootView.findViewById(R.id.tue_from2);
        tue_to1 = (TextView) rootView.findViewById(R.id.tue_to1);
        tue_to2 = (TextView) rootView.findViewById(R.id.tue_to2);

        wed_from1 = (TextView) rootView.findViewById(R.id.wed_from1);
        wed_from2 = (TextView) rootView.findViewById(R.id.wed_from2);
        wed_to1 = (TextView) rootView.findViewById(R.id.wed_to1);
        wed_to2 = (TextView) rootView.findViewById(R.id.wed_to2);

        thu_from1 = (TextView) rootView.findViewById(R.id.thu_from1);
        thu_from2 = (TextView) rootView.findViewById(R.id.thu_from2);
        thu_to1 = (TextView) rootView.findViewById(R.id.thu_to1);
        thu_to2 = (TextView) rootView.findViewById(R.id.thu_to2);

        fri_from1 = (TextView) rootView.findViewById(R.id.fri_from1);
        fri_from2 = (TextView) rootView.findViewById(R.id.fri_from2);
        fri_to1 = (TextView) rootView.findViewById(R.id.fri_to1);
        fri_to2 = (TextView) rootView.findViewById(R.id.fri_to2);

        sat_from1 = (TextView) rootView.findViewById(R.id.sat_from1);
        sat_from2 = (TextView) rootView.findViewById(R.id.sat_from2);
        sat_to1 = (TextView) rootView.findViewById(R.id.sat_to1);
        sat_to2 = (TextView) rootView.findViewById(R.id.sat_to2);

        sun_from1 = (TextView) rootView.findViewById(R.id.sun_from1);
        sun_from2 = (TextView) rootView.findViewById(R.id.sun_from2);
        sun_to1 = (TextView) rootView.findViewById(R.id.sun_to1);
        sun_to2 = (TextView) rootView.findViewById(R.id.sun_to2);

        setRestaurantTimings();
        showExistingMenuImages();

        //Initialising menu
        menuImagesAdapter = new RestaurantImageAdapter(menuImageModelList);
//        RecyclerView.LayoutManager mLayoutManagerq = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
//        menuImagesRecyclerView.setLayoutManager(mLayoutManagerq);
//        menuImagesRecyclerView.setItemAnimator(new DefaultItemAnimator());
        menuImagesRecyclerView.setAdapter(menuImagesAdapter);


        return rootView;
    }

    public void showExistingMenuImages() {
        try {
            FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseRestaurantNode).child(((RestaurantDetailActivity) getActivity()).getRestauranntId()).child(Constant.FirebaseRestaurantImagesNode).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    menuImageModelList.clear();
                    for (DataSnapshot datasnapshot1 :
                            dataSnapshot.getChildren()) {
                        ImageModel imagemodel = datasnapshot1.getValue(ImageModel.class);
                        //                    String imageUrl = datasnapshot1.getValue(String.class);
                        //                    ImageModel imagemodel = new ImageModel(imageUrl);
                        menuImageModelList.add(imagemodel);
                        menuImagesAdapter.notifyDataSetChanged();
                    }
                    if(menuImageModelList.isEmpty()){
                        noMenuImgTv.setVisibility(View.VISIBLE);
                    } else {
                        noMenuImgTv.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void initialisedata() {
        try {
            FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseRestaurantNode).child(restaurantId).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    final RestaurantBasicDetailsModel restaurantBasicDetailsModel = dataSnapshot.getValue(RestaurantBasicDetailsModel.class);
                    rest_addr_input.setText(restaurantBasicDetailsModel.restaurantAddress);
                    contactPhone = restaurantBasicDetailsModel.restautantContactNumber;
                    restLatLng = restaurantBasicDetailsModel.restaurantLatLng;
                    String[] separated = restLatLng.split(",");
                    latitude = separated[0].trim();
                    longitude = separated[1].trim();

                    call.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(Intent.ACTION_DIAL);
                            intent.setData(Uri.parse("tel:" + contactPhone));
                            startActivity(intent);
                        }
                    });

                    mapView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            String strUri = "http://maps.google.com/maps?q=loc:" + latitude + "," + longitude + " (" + restaurantBasicDetailsModel.restaurantName + ")";
                            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(strUri));
                            intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
                            startActivity(intent);
                        }
                    });

                    map.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            String strUri = "http://maps.google.com/maps?q=loc:" + latitude + "," + longitude + " (" + restaurantBasicDetailsModel.restaurantName + ")";
                            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(strUri));
                            intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
                            startActivity(intent);
                        }
                    });

                    share.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent share = new Intent(android.content.Intent.ACTION_SEND);
                            share.setType("text/plain");
                            share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);

                            // Add data to the intent, the receiving app will decide
                            // what to do with it.
                            String strUri = " http://maps.google.com/maps?q=loc:" + latitude + "," + longitude + "(" + restaurantBasicDetailsModel.restaurantName + ")";
//                            share.putExtra(Intent.EXTRA_SUBJECT, restaurantBasicDetailsModel.restaurantName);
                            share.putExtra(Intent.EXTRA_TEXT, "Click on the link below to find way to this awesome restaurant \n" + restaurantBasicDetailsModel.restaurantName + "\n" + strUri);

                            startActivity(Intent.createChooser(share, "Share restaurant! " + restaurantBasicDetailsModel.restaurantName));
                        }
                    });

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setRestaurantTimings() {
        try {

            try {
                databaseReference.child(Constant.FirebaseTimeNode).child("starttime1").addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        try {
                            starttime1.setText(convert24HrTimeTo12HrFormat(dataSnapshot.getValue(String.class)));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        if (starttime1.getText().toString().isEmpty()) {
                            starttime1.setText("10:00AM");
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
                starttime1.setText("10:00AM");
            }

            try {
                databaseReference.child(Constant.FirebaseTimeNode).child("endtime1").addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        try {
                            endtime1.setText(convert24HrTimeTo12HrFormat(dataSnapshot.getValue(String.class)));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        if (endtime1.getText().toString().isEmpty()) {
                            endtime1.setText("03:00PM");
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
                endtime1.setText("03:00PM");
            }

            try {
                databaseReference.child(Constant.FirebaseTimeNode).child("starttime2").addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        try {
                            starttime2.setText(convert24HrTimeTo12HrFormat(dataSnapshot.getValue(String.class)));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        if (starttime2.getText().toString().isEmpty()) {
                            starttime2.setText("06:00PM");
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            } catch (Exception e) {
                starttime2.setText("06:00PM");
            }

            try {
                databaseReference.child(Constant.FirebaseTimeNode).child("endtime2").addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        try {
                            endtime2.setText(convert24HrTimeTo12HrFormat(dataSnapshot.getValue(String.class)));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        if (endtime2.getText().toString().isEmpty()) {
                            endtime2.setText("12:00AM");
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            } catch (Exception e) {
                endtime2.setText("12:00AM");
            }

            try {
                databaseReference.child(Constant.FirebaseTimeNode).child("holidays").addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        String holidays = dataSnapshot.getValue(String.class);
                        try {
                            if (holidays != null) {
                                if(holidays.contains(getDay())){
                                    isOpenTv.setText("CLOSED");
                                } else {
                                    isOpenTv.setText("OPEN");
                                }
                            }
                        } catch (Exception e) {
                            isOpenTv.setText("OPEN");
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            } catch (Exception e) {
                isOpenTv.setText("OPEN");
                e.printStackTrace();
            }

//            databaseReference.child(Constant.FirebaseTimeNode).addListenerForSingleValueEvent(new ValueEventListener() {
//                @Override
//                public void onDataChange(DataSnapshot dataSnapshot) {
//                    RestaurantTimeModel restaurantTimeModel = dataSnapshot.getValue(RestaurantTimeModel.class);
//
//                    if (restaurantTimeModel != null) {
//                        mon_from1.setText(restaurantTimeModel.mon_from1);
//                        mon_from2.setText(restaurantTimeModel.mon_from2);
//                        mon_to1.setText(restaurantTimeModel.mon_to1);
//                        mon_to2.setText(restaurantTimeModel.mon_to2);
//
//                        tue_from1.setText(restaurantTimeModel.tue_from1);
//                        tue_from2.setText(restaurantTimeModel.tue_from2);
//                        tue_to1.setText(restaurantTimeModel.tue_to1);
//                        tue_to2.setText(restaurantTimeModel.tue_to2);
//
//                        wed_from1.setText(restaurantTimeModel.wed_from1);
//                        wed_from2.setText(restaurantTimeModel.wed_from2);
//                        wed_to1.setText(restaurantTimeModel.wed_to1);
//                        wed_to2.setText(restaurantTimeModel.wed_to2);
//
//                        thu_from1.setText(restaurantTimeModel.thu_from1);
//                        thu_from2.setText(restaurantTimeModel.thu_from2);
//                        thu_to1.setText(restaurantTimeModel.thu_to1);
//                        thu_to2.setText(restaurantTimeModel.thu_to2);
//
//                        fri_from1.setText(restaurantTimeModel.fri_from1);
//                        fri_from2.setText(restaurantTimeModel.fri_from2);
//                        fri_to1.setText(restaurantTimeModel.fri_to1);
//                        fri_to2.setText(restaurantTimeModel.fri_to2);
//
//                        sat_from1.setText(restaurantTimeModel.sat_from1);
//                        sat_from2.setText(restaurantTimeModel.sat_from2);
//                        sat_to1.setText(restaurantTimeModel.sat_to1);
//                        sat_to2.setText(restaurantTimeModel.sat_to2);
//
//                        sun_from1.setText(restaurantTimeModel.sun_from1);
//                        sun_from2.setText(restaurantTimeModel.sun_from2);
//                        sun_to1.setText(restaurantTimeModel.sun_to1);
//                        sun_to2.setText(restaurantTimeModel.sun_to2);
//
//                        if (restaurantTimeModel.isMonHoliday) {
//                            rest_mon_isClosed.setVisibility(View.VISIBLE);
//                        }
//                        if (restaurantTimeModel.isTueHoliday) {
//                            rest_tue_isClosed.setVisibility(View.VISIBLE);
//                        }
//                        if (restaurantTimeModel.isWedHoliday) {
//                            rest_wed_isClosed.setVisibility(View.VISIBLE);
//                        }
//                        if (restaurantTimeModel.isThuHoliday) {
//                            rest_thu_isClosed.setVisibility(View.VISIBLE);
//                        }
//                        if (restaurantTimeModel.isFriHoliday) {
//                            rest_fri_isClosed.setVisibility(View.VISIBLE);
//                        }
//                        if (restaurantTimeModel.isSatHoliday) {
//                            rest_sat_isClosed.setVisibility(View.VISIBLE);
//                        }
//                        if (restaurantTimeModel.isSunHoliday) {
//                            rest_sun_isClosed.setVisibility(View.VISIBLE);
//                        }
//
//
//    //                    rest_mon_holiday_input.setChecked(restaurantTimeModel.isMonHoliday);
//    //                    rest_tue_holiday_input.setChecked(restaurantTimeModel.isTueHoliday);
//    //                    rest_wed_holiday_input.setChecked(restaurantTimeModel.isWedHoliday);
//    //                    rest_thu_holiday_input.setChecked(restaurantTimeModel.isThuHoliday);
//    //                    rest_fri_holiday_input.setChecked(restaurantTimeModel.isFriHoliday);
//    //                    rest_sat_holiday_input.setChecked(restaurantTimeModel.isSatHoliday);
//    //                    rest_sun_holiday_input.setChecked(restaurantTimeModel.isSunHoliday);
//                    }
//
//                }
//
//                @Override
//                public void onCancelled(DatabaseError databaseError) {
//
//                }
//            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {

    }

}