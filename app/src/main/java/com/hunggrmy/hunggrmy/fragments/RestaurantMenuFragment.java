package com.hunggrmy.hunggrmy.fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.hunggrmy.hunggrmy.CartActivity;
import com.hunggrmy.hunggrmy.MapsActivity;
import com.hunggrmy.hunggrmy.R;
import com.hunggrmy.hunggrmy.RestaurantDetailActivity;
import com.hunggrmy.hunggrmy.SplashScreenActivity;
import com.hunggrmy.hunggrmy.adapters.MenuFragmentAdapter;
import com.hunggrmy.hunggrmy.adapters.MenuImageAdapter;
import com.hunggrmy.hunggrmy.models.ImageModel;
import com.hunggrmy.hunggrmy.models.RestaurantMenuModel;
import com.hunggrmy.hunggrmy.utils.Constant;
import com.yarolegovich.discretescrollview.DiscreteScrollView;
import com.yarolegovich.discretescrollview.transform.Pivot;
import com.yarolegovich.discretescrollview.transform.ScaleTransformer;

import java.util.ArrayList;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Wasim on 5/19/2018.
 */

public class RestaurantMenuFragment extends Fragment {

    EditText item_name_input, item_price_input;
    CheckBox isRcmd_input, isOffer_input, isCoupon_input, isVeg_input;
    LinearLayout submit_btn;
    Spinner item_cat_input;
    ImageView item_img_input;
    TextView totItems, tot, viewCartBtn, noMenuItemsTv, noMenuImgTv;
    RelativeLayout cartLL;

    public static String menuRestaurantId = "";
    List<String> categories = new ArrayList<String>();
    String item_Category = "", itemImage = "";
    String menuId = "";
    boolean isUploadingImage = false;

    //ForShowing Existing Menu
    private List<String> menuList = new ArrayList<>();
    private RecyclerView menu_recycler_view;
    private MenuFragmentAdapter mAdapter;

    private ArrayList<ImageModel> menuImageModelList = new ArrayList<>();
    private DiscreteScrollView menuImagesRecyclerView;
    private MenuImageAdapter menuImagesAdapter;

    public RestaurantMenuFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_restaurant_menu, container, false);
        menu_recycler_view = (RecyclerView) rootView.findViewById(R.id.menu_recycler_view);
        menuImagesRecyclerView = (DiscreteScrollView) rootView.findViewById(R.id.menu_image_recycler_view);
        item_name_input = (EditText) rootView.findViewById(R.id.item_name_input);
        item_price_input = (EditText) rootView.findViewById(R.id.item_price_input);
        isRcmd_input = (CheckBox) rootView.findViewById(R.id.isRcmd_input);
        isOffer_input = (CheckBox) rootView.findViewById(R.id.isOffer_input);
        isCoupon_input = (CheckBox) rootView.findViewById(R.id.isCoupon_input);
        isVeg_input = (CheckBox) rootView.findViewById(R.id.isVeg_input);
        item_cat_input = (Spinner) rootView.findViewById(R.id.item_cat_input);
        item_img_input = (ImageView) rootView.findViewById(R.id.item_img_input);
        noMenuImgTv = (TextView) rootView.findViewById(R.id.noMenuImgTv);
        noMenuItemsTv = (TextView) rootView.findViewById(R.id.noMenuItemsTv);

        totItems = (TextView) rootView.findViewById(R.id.totItems);
        tot = (TextView) rootView.findViewById(R.id.tot);
        cartLL = (RelativeLayout) rootView.findViewById(R.id.cartLL);

        submit_btn = (LinearLayout) rootView.findViewById(R.id.submit_btn);

        menuImagesRecyclerView.setItemTransformer(new ScaleTransformer.Builder()
                .setMaxScale(1.05f)
                .setMinScale(0.8f)
                .setPivotX(Pivot.X.CENTER) // CENTER is a default one
                .setPivotY(Pivot.Y.BOTTOM) // CENTER is a default one
                .build());

        cartLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final ProgressDialog progressDialog = new ProgressDialog(getContext());
                progressDialog.setMessage("Fetching your cart...");
                progressDialog.setCancelable(false);
                progressDialog.show();

                final Handler ha = new Handler();
                ha.postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        progressDialog.dismiss();
                        startActivity(new Intent(getContext(), CartActivity.class));
                    }
                }, 2000);

            }
        });

        //Initialising menu
        mAdapter = new MenuFragmentAdapter(menuList, RestaurantMenuFragment.this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        menu_recycler_view.setLayoutManager(mLayoutManager);
        menu_recycler_view.setItemAnimator(new DefaultItemAnimator());
        menu_recycler_view.setAdapter(mAdapter);

        //Initialising menu
        menuImagesAdapter = new MenuImageAdapter(menuImageModelList);
//        RecyclerView.LayoutManager mLayoutManagerq = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
//        menuImagesRecyclerView.setLayoutManager(mLayoutManagerq);
//        menuImagesRecyclerView.setItemAnimator(new DefaultItemAnimator());
        menuImagesRecyclerView.setAdapter(menuImagesAdapter);



        showExistingMenuImages();
        showExistingMenu();
        showCartItems();

        return rootView;
    }

    public void showExistingMenuImages() {
        try {
            FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseRestaurantNode).child(((RestaurantDetailActivity) getActivity()).getRestauranntId()).child(Constant.FirebaseMenuImagesNode).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    menuImageModelList.clear();
                    for (DataSnapshot datasnapshot1 :
                            dataSnapshot.getChildren()) {
                        ImageModel imagemodel = datasnapshot1.getValue(ImageModel.class);
    //                    String imageUrl = datasnapshot1.getValue(String.class);
    //                    ImageModel imagemodel = new ImageModel(imageUrl);
                        menuImageModelList.add(imagemodel);
                        menuImagesAdapter.notifyDataSetChanged();
                    }
                    if(menuImageModelList.isEmpty()){
                        noMenuImgTv.setVisibility(View.VISIBLE);
                    } else {
                        noMenuImgTv.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showCartItems(){
        final SharedPreferences pref = getContext().getSharedPreferences("HungrrMobile", MODE_PRIVATE);

        if (pref.getBoolean(Constant.isLoggedIn, Boolean.parseBoolean(null))) {

            int totalNumberOfItemsLocalVar = 0;
            try {
                FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseUserNode).child(pref.getString(Constant.userId, null)).child(Constant.FirebaseCartNode).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        final int[] total = {0};
                        final int[] totalItems = {0};
                        if(!dataSnapshot.hasChildren()){
                            tot.setText("RM 0");
                            totItems.setText("0");
                        } else {
                            try {
                                for (DataSnapshot dataSnapshot1 :
                                        dataSnapshot.getChildren()) {

                                    String menuId = dataSnapshot1.getKey();
                                    final String menuNos = dataSnapshot1.getValue(String.class);

                                    totalItems[0] = totalItems[0]+ Integer.parseInt(menuNos);
                                    totItems.setText(totalItems[0]+"");

                                    try {

                                        FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseRestaurantNode).child(Constant.FirebaseMenuNode).child(menuId).addValueEventListener(new ValueEventListener() {
                                            @Override
                                            public void onDataChange(DataSnapshot dataSnapshot) {
                                                RestaurantMenuModel restaurantMenuModel = null;
                                                try {
                                                    restaurantMenuModel = dataSnapshot.getValue(RestaurantMenuModel.class);
                                                    menuRestaurantId = restaurantMenuModel.restaurantId;

                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }
                                                try {
                                                    total[0] = total[0] + Integer.parseInt(restaurantMenuModel.itemPrice) * Integer.parseInt(menuNos);
                                                } catch (NumberFormatException e) {
                                                    e.printStackTrace();
                                                }
                                                try {
                                                    tot.setText("RM "+total[0]);
                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }
                                            }

                                            @Override
                                            public void onCancelled(DatabaseError databaseError) {
                                                try {
                                                    tot.setText("RM 0");
                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }
                                                try {
                                                    totItems.setText("0");
                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        });
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            } catch (NumberFormatException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        tot.setText("RM 0");
                        totItems.setText("0");
                    }
                });
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }

        } else {

        }

    }

    public void showExistingMenu(){
        try {
            FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseRestaurantNode).child(((RestaurantDetailActivity) getActivity()).getRestauranntId()).child(Constant.FirebaseMenuNode).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    menuList.clear();
                    for (DataSnapshot datasnapshot1 :
                            dataSnapshot.getChildren()) {
                        String menuId = datasnapshot1.getValue(String.class);
                        menuList.add(menuId);
                        mAdapter.notifyDataSetChanged();
                    }
                    if(menuList.isEmpty()){
                        noMenuItemsTv.setVisibility(View.VISIBLE);
                    } else {
                        noMenuItemsTv.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getMenuRestaurantId(){
        return menuRestaurantId;
    }


}