package com.hunggrmy.hunggrmy.fragments;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.location.Location;
import android.location.LocationListener;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import com.billing.mobilebillingsystem.main.Utils.LogUtils;
import com.google.android.gms.cast.TextTrackStyle;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.hunggrmy.hunggrmy.MainActivity;
import com.hunggrmy.hunggrmy.R;
import com.hunggrmy.hunggrmy.RestaurantDetailActivity;
import com.hunggrmy.hunggrmy.interfaces.OnZomatoApiListener;
import com.hunggrmy.hunggrmy.models.FSRestaurant;
import com.hunggrmy.hunggrmy.models.ImageModel;
import com.hunggrmy.hunggrmy.models.RestaurantBasicDetailsModel;
import com.hunggrmy.hunggrmy.models.zomato.ZomatoRestaurantModel;
import com.hunggrmy.hunggrmy.networking.GetRestaurantDetailsFromZomato;
import com.hunggrmy.hunggrmy.utils.Constant;
import com.hunggrmy.hunggrmy.utils.GPSTracker;
import com.hunggrmy.hunggrmy.utils.MyApplication;
import com.hunggrmy.hunggrmy.utils.Utility;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Wasim on 5/19/2018.
 */

public class MapViewFragment extends Fragment implements OnMapReadyCallback, LocationListener, OnZomatoApiListener {

    //ForShowing Existing Offers
    private List<RestaurantBasicDetailsModel> restaurantList = new ArrayList<>();

    static double lastMapLoadedLatitude = 0;
    static double lastMapLoadedLongitude = 0;
    private static GoogleMap mMap;
    LatLng currectLocation;
    private static GPSTracker gps;
    String selectedmarkerName;
    String selectedmarkerId;
    public String coverageValue = "2000";


    public MapViewFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_map_view, container, false);
        gps = new GPSTracker(getContext());
        if (getFragmentManager() != null) {
            ((SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map)).getMapAsync(this);
        } else {
            Toast.makeText(getContext(), "Map is null", Toast.LENGTH_SHORT).show();
        }

        return rootView;
    }

    public void setCoverageValue() {
        FirebaseDatabase.getInstance().getReference().child("AppControls").child("map_area_coverage").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                coverageValue = dataSnapshot.getValue(String.class);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void showRestaurants() {

        lastMapLoadedLatitude = MainActivity.centralLatitude;
        lastMapLoadedLongitude = MainActivity.centralLongitude;

//        SharedPreferences shared = getContext().getSharedPreferences("HungrrMobile", MODE_PRIVATE);
//        String restautantsList = (shared.getString("restautantsList", ""));
//        try {
//            JSONObject jsonObject = new JSONObject(restautantsList);
//            JSONArray jsonArray = jsonObject.getJSONArray("restaurants");
//
//            for (int i=0; i<jsonArray.length(); i++){
//                Gson g = new Gson();
//                RestaurantBasicDetailsModel restaurantBasicDetailsModel = g.fromJson(jsonArray.get(i)+"", RestaurantBasicDetailsModel.class);
//
//                try {
//                    final String[] latlng = restaurantBasicDetailsModel.restaurantLatLng.split(",");
//                    if (isWithin2Km(Double.valueOf(latlng[0]), Double.valueOf(latlng[1]))) {
//                        restaurantList.add(restaurantBasicDetailsModel);
//
//                        int height = 80;
//                        int width = 80;
//
//                        int topHeight = 80;
//                        int topWidth = 80;
//
//                        int nvHeight = 40;
//                        int nvWidth = 40;
//
//                        BitmapDrawable bitmapdrawTop = (BitmapDrawable) getResources().getDrawable(R.drawable.top);
//                        BitmapDrawable bitmapdrawRcmd = (BitmapDrawable) getResources().getDrawable(R.drawable.rcmd);
//                        BitmapDrawable bitmapdrawNotVerified = (BitmapDrawable) getResources().getDrawable(R.drawable.notverifiedgps);
//
//                        Bitmap bT = bitmapdrawTop.getBitmap();
//                        Bitmap bR = bitmapdrawRcmd.getBitmap();
//                        Bitmap bNV = bitmapdrawNotVerified.getBitmap();
//
//                        Bitmap topMarker = Bitmap.createScaledBitmap(bT, topWidth, topHeight, false);
//                        Bitmap rcmdMarker = Bitmap.createScaledBitmap(bR, width, height, false);
//                        Bitmap nvMarker = Bitmap.createScaledBitmap(bNV, nvWidth, nvHeight, false);
//
//                        BitmapDescriptor icon = BitmapDescriptorFactory.fromBitmap(rcmdMarker);
//                        BitmapDescriptor iconTop = BitmapDescriptorFactory.fromBitmap(topMarker);
//                        BitmapDescriptor iconNV = BitmapDescriptorFactory.fromBitmap(nvMarker);
//                        Marker marker;
//
//                        try {
//                            if (restaurantBasicDetailsModel.isSubscribed) {
//
//                                if (restaurantBasicDetailsModel.isTopRestaurant) {
//                                    marker = mMap.addMarker(new MarkerOptions().position(new LatLng(Double.valueOf(latlng[0]).doubleValue(), Double.valueOf(latlng[1]).doubleValue())).title(restaurantBasicDetailsModel.restaurantName).snippet(restaurantBasicDetailsModel.cuisines).icon(iconTop).anchor(0.5f, TextTrackStyle.DEFAULT_FONT_SCALE));
//                                    marker.setTag(restaurantBasicDetailsModel);
//                                } else {
//                                    marker = mMap.addMarker(new MarkerOptions().position(new LatLng(Double.valueOf(latlng[0]).doubleValue(), Double.valueOf(latlng[1]).doubleValue())).title(restaurantBasicDetailsModel.restaurantName).snippet(restaurantBasicDetailsModel.cuisines).icon(icon).anchor(0.5f, TextTrackStyle.DEFAULT_FONT_SCALE));
//                                    marker.setTag(restaurantBasicDetailsModel);
//                                }
//                                try {
//                                    if (selectedmarkerName.equals(restaurantBasicDetailsModel.restaurantName)) {
//                                        marker.showInfoWindow();
//                                        //                                        setSelectedRestaurant();
//                                    }
//                                } catch (Exception e) {
//
//                                }
//                            } else {
//                                marker = mMap.addMarker(new MarkerOptions().position(new LatLng(Double.valueOf(latlng[0]).doubleValue(), Double.valueOf(latlng[1]).doubleValue())).title(restaurantBasicDetailsModel.restaurantName).snippet("Not Verified").icon(iconNV).anchor(0.5f, TextTrackStyle.DEFAULT_FONT_SCALE));
////                                        marker = mMap.addMarker(new MarkerOptions().position(new LatLng(Double.valueOf(latlng[0]).doubleValue(), Double.valueOf(latlng[1]).doubleValue())).title(restaurantBasicDetailsModel.restaurantName).snippet("Not Verified").anchor(0.5f, TextTrackStyle.DEFAULT_FONT_SCALE));
//                                marker.setTag(restaurantBasicDetailsModel);
//                                try {
//                                    if (selectedmarkerName.equals(restaurantBasicDetailsModel.restaurantName)) {
//                                        marker.showInfoWindow();
//                                    }
//                                } catch (Exception e) {
//
//                                }
//                            }
//                        } catch (NumberFormatException e) {
//                            e.printStackTrace();
//                        }
//
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }


        try {
            FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseRestaurantNode).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    restaurantList.clear();
                    mMap.clear();
                    for (DataSnapshot datasnapshot1 :
                            dataSnapshot.getChildren()) {
                        RestaurantBasicDetailsModel restaurantBasicDetailsModel = datasnapshot1.getValue(RestaurantBasicDetailsModel.class);
                        try {
                            final String[] latlng = restaurantBasicDetailsModel.restaurantLatLng.split(",");
                            if (isWithin2Km(Double.valueOf(latlng[0]), Double.valueOf(latlng[1]))) {

                                if (MainActivity.selectedFilters.isEmpty()) {
                                    LogUtils.Log("filter : " + "selected filters is empty");
                                    loadRestaurantsInMap(restaurantBasicDetailsModel, latlng);
                                } else {
                                    LogUtils.Log("filter : " + "selected filters is not empty");
                                    for (String filter :
                                            MainActivity.selectedFilters) {
                                        if (restaurantBasicDetailsModel.restaurantName.toLowerCase().contains(filter.toLowerCase())) {
                                            LogUtils.Log("Loading in map : " + restaurantBasicDetailsModel.restaurantName);
                                            loadRestaurantsInMap(restaurantBasicDetailsModel, latlng);
                                        } else {
                                            LogUtils.Log(restaurantBasicDetailsModel.restaurantName + " DOES NOT CONTAIN " + filter);
                                        }

                                    }
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }

                    if (restaurantList.isEmpty()) {

                    } else {

                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void loadRestaurantsInMap(RestaurantBasicDetailsModel restaurantBasicDetailsModel, String[] latlng) {
        restaurantList.add(restaurantBasicDetailsModel);

        int height = 80;
        int width = 80;

        int topHeight = 80;
        int topWidth = 80;

        int nvHeight = 40;
        int nvWidth = 40;

        BitmapDrawable bitmapdrawTop = (BitmapDrawable) getResources().getDrawable(R.drawable.top);
        BitmapDrawable bitmapdrawRcmd = (BitmapDrawable) getResources().getDrawable(R.drawable.rcmd);
        BitmapDrawable bitmapdrawNotVerified = (BitmapDrawable) getResources().getDrawable(R.drawable.notverifiedgps);
        BitmapDrawable bitmapdrawVerified = (BitmapDrawable) getResources().getDrawable(R.drawable.gpsviolet);

        Bitmap bT = bitmapdrawTop.getBitmap();
        Bitmap bR = bitmapdrawRcmd.getBitmap();
        Bitmap bNV = bitmapdrawNotVerified.getBitmap();
        Bitmap bV = bitmapdrawVerified.getBitmap();

        Bitmap topMarker = Bitmap.createScaledBitmap(bT, topWidth, topHeight, false);
        Bitmap rcmdMarker = Bitmap.createScaledBitmap(bR, width, height, false);
        Bitmap nvMarker = Bitmap.createScaledBitmap(bNV, nvWidth, nvHeight, false);
        Bitmap vMarker = Bitmap.createScaledBitmap(bV, nvWidth, nvHeight, false);

        BitmapDescriptor icon = BitmapDescriptorFactory.fromBitmap(rcmdMarker);
        BitmapDescriptor iconTop = BitmapDescriptorFactory.fromBitmap(topMarker);
        BitmapDescriptor iconNV = BitmapDescriptorFactory.fromBitmap(nvMarker);
        BitmapDescriptor iconV = BitmapDescriptorFactory.fromBitmap(vMarker);
        Marker marker;

        try {


            if (restaurantBasicDetailsModel.isSubscribed) {

                if (restaurantBasicDetailsModel.isTopRestaurant) {
                    marker = mMap.addMarker(new MarkerOptions().position(new LatLng(Double.valueOf(latlng[0]).doubleValue(), Double.valueOf(latlng[1]).doubleValue())).title(restaurantBasicDetailsModel.restaurantName).snippet(restaurantBasicDetailsModel.cuisines).icon(iconTop).anchor(0.5f, TextTrackStyle.DEFAULT_FONT_SCALE));
                    marker.setTag(restaurantBasicDetailsModel);
                } else {
                    marker = mMap.addMarker(new MarkerOptions().position(new LatLng(Double.valueOf(latlng[0]).doubleValue(), Double.valueOf(latlng[1]).doubleValue())).title(restaurantBasicDetailsModel.restaurantName).snippet(restaurantBasicDetailsModel.cuisines).icon(icon).anchor(0.5f, TextTrackStyle.DEFAULT_FONT_SCALE));
                    marker.setTag(restaurantBasicDetailsModel);
                }
                try {
                    if (selectedmarkerName.equals(restaurantBasicDetailsModel.restaurantName)) {
                        marker.showInfoWindow();
                        //                                        setSelectedRestaurant();
                    }
                } catch (Exception e) {

                }
            } else {
                if (restaurantBasicDetailsModel.isVerifiedRestaurant != null && restaurantBasicDetailsModel.isVerifiedRestaurant) {
                    marker = mMap.addMarker(new MarkerOptions().position(new LatLng(Double.valueOf(latlng[0]).doubleValue(), Double.valueOf(latlng[1]).doubleValue())).title(restaurantBasicDetailsModel.restaurantName).snippet("Verified").icon(iconNV).anchor(0.5f, TextTrackStyle.DEFAULT_FONT_SCALE));

                } else {
                    marker = mMap.addMarker(new MarkerOptions().position(new LatLng(Double.valueOf(latlng[0]).doubleValue(), Double.valueOf(latlng[1]).doubleValue())).title(restaurantBasicDetailsModel.restaurantName).snippet("Not Verified").icon(iconV).anchor(0.5f, TextTrackStyle.DEFAULT_FONT_SCALE));

                }
//                                        marker = mMap.addMarker(new MarkerOptions().position(new LatLng(Double.valueOf(latlng[0]).doubleValue(), Double.valueOf(latlng[1]).doubleValue())).title(restaurantBasicDetailsModel.restaurantName).snippet("Not Verified").anchor(0.5f, TextTrackStyle.DEFAULT_FONT_SCALE));
                marker.setTag(restaurantBasicDetailsModel);
                try {
                    if (selectedmarkerName.equals(restaurantBasicDetailsModel.restaurantName)) {
                        marker.showInfoWindow();
                    }
                } catch (Exception e) {

                }
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
    }

    public boolean isWithin2Km(Double restLatitude, double restLongitude) {
        float[] results = new float[1];
        Location.distanceBetween(MainActivity.centralLatitude, MainActivity.centralLongitude, restLatitude, restLongitude, results);
        if (coverageValue.equals("")) {
            if (results[0] < 100.0f) {
                return true;
            }
        } else {
            if (results[0] < Float.valueOf(coverageValue)) {
                return true;
            }
        }

        return false;
    }

    public void onMapReady(GoogleMap googleMap) {
        this.mMap = googleMap;

        if (ActivityCompat.checkSelfPermission(getContext(), "android.permission.ACCESS_FINE_LOCATION") == 0 || ActivityCompat.checkSelfPermission(getContext(), "android.permission.ACCESS_COARSE_LOCATION") == 0) {
            currectLocation = new LatLng(MainActivity.centralLatitude, MainActivity.centralLongitude);
            this.mMap.moveCamera(CameraUpdateFactory.newLatLng(this.currectLocation));
            try {
                this.mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(this.currectLocation.latitude, this.currectLocation.longitude), 15.0f));
            } catch (Exception e) {
                this.mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(this.currectLocation.latitude, this.currectLocation.longitude), 15.0f));
                e.printStackTrace();
            }
            this.mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                @Override
                public void onInfoWindowClick(Marker marker) {
                    RestaurantBasicDetailsModel restaurantBasicDetailsModel = (RestaurantBasicDetailsModel) marker.getTag();
                    Intent intent = new Intent(getContext(), RestaurantDetailActivity.class);
                    intent.putExtra("restaurantId", restaurantBasicDetailsModel.restaurantId);
                    intent.putExtra("setselection", "");
                    startActivity(intent);
                }
            });
            this.mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                @Override
                public boolean onMarkerClick(Marker marker) {
                    RestaurantBasicDetailsModel restaurantBasicDetailsModel = (RestaurantBasicDetailsModel) marker.getTag();
                    selectedmarkerName = restaurantBasicDetailsModel.restaurantName;
                    selectedmarkerId = restaurantBasicDetailsModel.restaurantId;
                    Toast.makeText(getContext(), selectedmarkerName, Toast.LENGTH_SHORT).show();
                    makeZomatoApiCall(restaurantBasicDetailsModel);
                    return false;
                }
            });
            this.mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                @Override
                public void onMapClick(LatLng latLng) {
                    MainActivity.driving_mode_input.setChecked(false);
                    MainActivity.isDriveModeEnabled = false;
                }
            });

            this.mMap.setOnCameraMoveStartedListener(new GoogleMap.OnCameraMoveStartedListener() {
                @Override
                public void onCameraMoveStarted(int reason) {
                    if (reason == GoogleMap.OnCameraMoveStartedListener.REASON_GESTURE) {
                        try {
                            MainActivity.stopDrivingMode();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else if (reason == GoogleMap.OnCameraMoveStartedListener
                            .REASON_API_ANIMATION) {
                    } else if (reason == GoogleMap.OnCameraMoveStartedListener
                            .REASON_DEVELOPER_ANIMATION) {
                    }
                }
            });

            this.mMap.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                            getContext(), R.raw.style_json));

            this.mMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
                @Override
                public void onCameraIdle() {
                    //get latlng at the center by calling
                    LatLng midLatLng = mMap.getCameraPosition().target;

                    MainActivity.centralLatitude = midLatLng.latitude;
                    MainActivity.centralLongitude = midLatLng.longitude;
                    MainActivity.changeAddress(midLatLng.latitude, midLatLng.longitude);

                    if (lastMapLoadedLatitude == 0 || lastMapLoadedLongitude == 0) {
                        Toast.makeText(getContext(), "Loading restaurants", Toast.LENGTH_SHORT).show();
                        showRestaurants();
                    } else if (!Utility.isWithinDesiredMeters(Float.parseFloat(coverageValue), lastMapLoadedLatitude, lastMapLoadedLongitude, midLatLng.latitude, midLatLng.longitude)) {
                        Toast.makeText(getContext(), "Updating restaurant list", Toast.LENGTH_SHORT).show();
                        showRestaurants();
                    } else {
//                        Toast.makeText(getContext(), "Not loading restaurants", Toast.LENGTH_SHORT).show();
                    }
//                    makeFSRestaurantsCall();
                }
            });
        }
        setCoverageValue();
        showRestaurants();

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onLocationChanged(Location location) {
//        if (isDriveModeEnabled) {
//            Log.e("Logged", "Logged : location changed " + location.getLatitude() + ":" + location.getLatitude());
//            preferredLatitude = location.getLatitude();
//            preferredLongitude = location.getLongitude();
//            getAddress();
//            getAllRestaurants();
//        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    public static void reloadMap() {
        try {

            mMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(0, 0)));
            mMap.clear();

//            lastMapLoadedLatitude = 0;
//            lastMapLoadedLongitude = 0;

            LatLng coordinate = null;
//            gps = new GPSTracker(MyApplication.getAppContext());
//            if (gps.canGetLocation())
            coordinate = new LatLng(lastMapLoadedLatitude, lastMapLoadedLongitude); //Store these lat lng values somewhere. These should be constant.
            CameraUpdate location = CameraUpdateFactory.newLatLngZoom(
                    coordinate, 15);
            mMap.animateCamera(location);

//            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(gps.getLatitude(), gps.getLongitude()), 16.0f));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void setToCurrentLocation() {
        try {
            LatLng coordinate = null;
            gps = new GPSTracker(MyApplication.getAppContext());
            if (gps.canGetLocation())
                coordinate = new LatLng(gps.getLatitude(), gps.getLongitude()); //Store these lat lng values somewhere. These should be constant.
            CameraUpdate location = CameraUpdateFactory.newLatLngZoom(
                    coordinate, 15);
            mMap.animateCamera(location);

//            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(gps.getLatitude(), gps.getLongitude()), 16.0f));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void switchToLocation(double latitude, double longitude) {
        try {
            LatLng coordinate = null;
            coordinate = new LatLng(latitude, longitude); //Store these lat lng values somewhere. These should be constant.
            CameraUpdate location = CameraUpdateFactory.newLatLngZoom(
                    coordinate, 15);
            mMap.animateCamera(location);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void enableDrivingMode() {
        final Handler ha = new Handler();
        ha.postDelayed(new Runnable() {

            @Override
            public void run() {
                //call function
                if (MainActivity.isDriveModeEnabled) {
                    Log.e("Logged", "Logged : setting to current location");
                    setToCurrentLocation();
                }
                ha.postDelayed(this, 3000);
            }
        }, 3000);
    }

    private void makeFSRestaurantsCall() {

        String latlng = MainActivity.centralLatitude + "," + MainActivity.centralLongitude;

        new JsonTask().execute("https://api.foursquare.com/v2/venues/search?ll=" + latlng + "&client_id=NLWLNGTKVAVNGZEVSRCI1J2HDL3JVQ3AKPISW3YT4JX414GN&client_secret=2PMGK55S5ZGD1JOPVO2VJQCKEZMS2BXS5QX1LQPUDGXLFAEG&v=20190429&categoryId=4d4b7105d754a06374d81259&radius=250");

    }

    private class JsonTask extends AsyncTask<String, String, String> {

        ArrayList<FSRestaurant> fsRestaurants = new ArrayList<>();

        protected void onPreExecute() {
            super.onPreExecute();
        }

        protected String doInBackground(String... params) {


            HttpURLConnection connection = null;
            BufferedReader reader = null;

            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));
                StringBuffer buffer = new StringBuffer();
                String line = "";
                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");
                    Log.d("Response: ", "> " + line);   //here u ll get whole response...... :-)
                }
                return buffer.toString();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ArrayList<ImageModel> restaurantImages = new ArrayList<>();
            try {
                JSONObject json = new JSONObject(result);
                JSONObject response = json.getJSONObject("response");
                JSONArray jsonArray = response.getJSONArray("venues");

                for (int i = 0; i < jsonArray.length(); i++) {
                    Gson g = new Gson();
//                    final FSRestaurant fsRestaurant = g.fromJson(jsonArray.get(i) + "", FSRestaurant.class);
//                    RestaurantBasicDetailsModel restaurantBasicDetailsModel = new RestaurantBasicDetailsModel(fsRestaurant.getId(), fsRestaurant.getName(), fsRestaurant.getLocation().getFormattedAddress().get(0), fsRestaurant.getLocation().getLat() + "," + fsRestaurant.getLocation().getLng(), "", "", "", "", "", restaurantImages, "", "", false, false, false, "", false, "");
//                    FirebaseFunctions.restaurant.child(fsRestaurant.getId()).setValue(restaurantBasicDetailsModel).addOnSuccessListener(new OnSuccessListener<Void>() {
//                        @Override
//                        public void onSuccess(Void aVoid) {
//                            showRestaurants();
//                        }
//                    });
                }

            } catch (Exception e) {
                e.printStackTrace();
                Log.e("FSRestaurant", e.getLocalizedMessage());
            }
        }
    }

    private void makeZomatoApiCall(RestaurantBasicDetailsModel restaurantBasicDetailsModel) {
        String[] latlng = restaurantBasicDetailsModel.restaurantLatLng.split(",");
        new GetRestaurantDetailsFromZomato(this, "https://developers.zomato.com/api/v2.1/search?q=" + restaurantBasicDetailsModel.restaurantName + "&count=1&lat=" + latlng[0] + "&lon=" + latlng[1]).execute();
    }

    @Override
    public void onSuccess(String response) {
        Log.e("Response from zomato", "Response : " + response);

        ZomatoRestaurantModel zomatoRestaurantModel = null;
        try {
            zomatoRestaurantModel = new Gson().fromJson(response, ZomatoRestaurantModel.class);
            showZomatoApiDialog(zomatoRestaurantModel);

        } catch (JsonSyntaxException e) {
            e.printStackTrace();
        }

//        JsonParser parser = new JsonParser();
//
//        JsonObject rootObject = parser.parse(response).getAsJsonObject();
//
//        try {
//            String results_found = rootObject.get("results_found").getAsString();
//            Log.e("Zomatoapi resfound",results_found);
//        } catch (Exception e) {
//            e.printStackTrace();
//            Log.e("Zomatoapi resfound","it is null");
//        }
//
//        JsonArray restautantsArrayObject = null;
//        try {
//            restautantsArrayObject = rootObject.getAsJsonArray("restaurants");
//        } catch (Exception e) {
//            e.printStackTrace();
//            Log.e("Zomatoapi arrobj","it is null");
//        }
//        JsonObject restaurantObject = null;
//        try {
//            restaurantObject = restautantsArrayObject.get(0).getAsJsonObject();
//        } catch (Exception e) {
//            e.printStackTrace();
//            Log.e("Zomatoapi resobj","it is null");
//
//        }
//
//        if(restaurantObject != null){
//            showZomatoApiDialog(restaurantObject);
//        } else {
//            Log.e("Zomatoapi","it is null");
//        }
    }

    public void showZomatoApiDialog(ZomatoRestaurantModel zomatoRestaurantModel) {

        Log.e("Zomatoapi", "inside alert ");


        final BottomSheetDialog dialog = new BottomSheetDialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.zomato_restaurant_dialog);

        TextView res_cuisines = (TextView) dialog.findViewById(R.id.res_cuisines);
        TextView res_timings = (TextView) dialog.findViewById(R.id.res_timings);
        TextView res_offers = (TextView) dialog.findViewById(R.id.res_offers);
        TextView res_voters = (TextView) dialog.findViewById(R.id.res_voters);
        TextView res_rating = (TextView) dialog.findViewById(R.id.res_rating);

        res_cuisines.setText(zomatoRestaurantModel.getRestaurants().get(0).getRestaurant().getCuisines());
        res_timings.setText(zomatoRestaurantModel.getRestaurants().get(0).getRestaurant().getTimings());
        if (zomatoRestaurantModel.getRestaurants().get(0).getRestaurant().getOffers().size() == 0) {
            res_offers.setText("No offers available from zomato for this restaurant");
        } else {
            res_offers.setText("Offers available from zomato for this restaurant");
        }
        res_rating.setText(zomatoRestaurantModel.getRestaurants().get(0).getRestaurant().getUserRating().getAggregateRating() + " / " +zomatoRestaurantModel.getRestaurants().get(0).getRestaurant().getUserRating().getRatingText());
        res_voters.setText(zomatoRestaurantModel.getRestaurants().get(0).getRestaurant().getUserRating().getVotes());

//        JsonParser parser = new JsonParser();
//
//        if(restaurantObject != null){
//            //Check for offers
//            JsonArray offersArray = null;
//            try {
//                offersArray = parser.parse(restaurantObject+"").getAsJsonObject().get("offers").getAsJsonArray();
//
//                if(offersArray.size() == 0){
//                    res_offers.setText("No offers available from zomato for this restaurant");
//                } else {
//                    res_offers.setText("Offers available from zomato for this restaurant");
//                }
//
//            } catch (Exception e) {
//                e.printStackTrace();
//                Log.e("zomatoapi offers", e.getLocalizedMessage());
//            }
//            JsonObject userRating = null;
//            try {
//                userRating = restaurantObject.get("user_rating").getAsJsonObject();
//            } catch (Exception e) {
//                e.printStackTrace();
//                Log.e("zomatoapi offers", e.getLocalizedMessage());
//
//            }
//            String aggregate_rating = "NA";
//            try {
//                aggregate_rating = userRating.get("aggregate_rating").getAsString();
//                res_rating.setText(aggregate_rating);
//
//            } catch (Exception e) {
//                e.printStackTrace();
//                Log.e("zomatoapi rate", e.getLocalizedMessage());
//
//            }
//            String votes = "NA";
//            try {
//                votes = userRating.get("votes").getAsString();
//                res_voters.setText(votes);
//
//            } catch (Exception e) {
//                e.printStackTrace();
//                Log.e("zomatoapi votes", e.getLocalizedMessage());
//
//            }
//            String timings = "Not Available";
//            try {
//                timings = restaurantObject.get("timings").getAsString();
//                res_timings.setText(timings);
//
//            } catch (Exception e) {
//                e.printStackTrace();
//                Log.e("zomatoapi timings", e.getLocalizedMessage());
//
//            }
//            String cuisines = "Not Found";
//            try {
//                cuisines = restaurantObject.get("cuisines").getAsString();
//                res_cuisines.setText(cuisines);
//
//            } catch (Exception e) {
//                e.printStackTrace();
//                Log.e("zomatoapi cuisines", e.getLocalizedMessage());
//
//            }
//        } else {
//            Log.e("Zomato", "restaurant object is null");
//        }

        dialog.show();
    }

}