package com.hunggrmy.hunggrmy.fragments;

import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.hunggrmy.hunggrmy.MainActivity;
import com.hunggrmy.hunggrmy.MapsActivity;
import com.hunggrmy.hunggrmy.R;
import com.hunggrmy.hunggrmy.RestaurantDetailActivity;
import com.hunggrmy.hunggrmy.adapters.MapsPageOfferAdapter;
import com.hunggrmy.hunggrmy.adapters.OfferAdapter;
import com.hunggrmy.hunggrmy.adapters.RestaurantFlatBoxAdapter;
import com.hunggrmy.hunggrmy.adapters.RestaurantListAdapter;
import com.hunggrmy.hunggrmy.models.RestaurantBasicDetailsModel;
import com.hunggrmy.hunggrmy.models.RestaurantOffersDetailsModel;
import com.hunggrmy.hunggrmy.utils.Constant;
import com.hunggrmy.hunggrmy.utils.GPSTracker;
import com.yarolegovich.discretescrollview.DiscreteScrollView;
import com.yarolegovich.discretescrollview.transform.Pivot;
import com.yarolegovich.discretescrollview.transform.ScaleTransformer;

import java.util.ArrayList;
import java.util.List;

import static com.hunggrmy.hunggrmy.utils.Utility.getDate;
import static com.hunggrmy.hunggrmy.utils.Utility.isDateExpired;

/**
 * Created by Wasim on 5/19/2018.
 */

public class CentralListFragment extends Fragment {

    //ForShowing Existing Offers
    private List<RestaurantBasicDetailsModel> restaurantList = new ArrayList<>();
    private List<RestaurantBasicDetailsModel> recommendedRestaurantList = new ArrayList<>();
    private List<RestaurantBasicDetailsModel> notRecommendedRestaurantList = new ArrayList<>();
    private List<RestaurantOffersDetailsModel> offersList = new ArrayList<>();
    private RecyclerView offer_recycler_view;
    private MapsPageOfferAdapter offerAdapter;
    private RecyclerView recycler_view;
    private RestaurantFlatBoxAdapter mAdapter;
    private TextView noValueText;
    private ProgressBar loadingProgressBar;
    private GPSTracker gpsTracker;
    LinearLayout title1, title2;
    public String coverageValue = "2000";

    public CentralListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_central_list, container, false);
        recycler_view = (RecyclerView) rootView.findViewById(R.id.recycler_view);
        offer_recycler_view = (RecyclerView) rootView.findViewById(R.id.offer_recycler_view);
        loadingProgressBar = (ProgressBar) rootView.findViewById(R.id.loadingProgressBar);
        noValueText = (TextView) rootView.findViewById(R.id.noValueText);
        title1 = (LinearLayout) rootView.findViewById(R.id.title1);
        title2 = (LinearLayout) rootView.findViewById(R.id.title2);

        mAdapter = new RestaurantFlatBoxAdapter(restaurantList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recycler_view.setLayoutManager(mLayoutManager);
        recycler_view.setItemAnimator(new DefaultItemAnimator());
        recycler_view.setAdapter(mAdapter);

        recycler_view.setNestedScrollingEnabled(false);

        //Initialising menu
        offerAdapter = new MapsPageOfferAdapter(offersList);
//        RecyclerView.LayoutManager mLayoutManager2 = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
//        offer_recycler_view.setLayoutManager(mLayoutManager2);
//        offer_recycler_view.setItemAnimator(new DefaultItemAnimator());
        offer_recycler_view.setAdapter(offerAdapter);

        gpsTracker = new GPSTracker(getContext());

        showRestaurants();
        setCoverageValue();

        return rootView;
    }

    public void setCoverageValue(){
        FirebaseDatabase.getInstance().getReference().child("AppControls").child("map_area_coverage").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                coverageValue = dataSnapshot.getValue(String.class);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void showRestaurants() {

        title1.setVisibility(View.GONE);
        title2.setVisibility(View.GONE);

        loadingProgressBar.setVisibility(View.VISIBLE);
        noValueText.setVisibility(View.GONE);
        recycler_view.setVisibility(View.GONE);
        try {
            FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseRestaurantNode).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    restaurantList.clear();
                    for (DataSnapshot datasnapshot1 :
                            dataSnapshot.getChildren()) {
                        RestaurantBasicDetailsModel restaurantBasicDetailsModel = datasnapshot1.getValue(RestaurantBasicDetailsModel.class);
                        try {
                            final String[] latlng = restaurantBasicDetailsModel.restaurantLatLng.split(",");
                            if (isWithin2Km(Double.valueOf(latlng[0]), Double.valueOf(latlng[1]))) {

                                try {
                                    if (restaurantBasicDetailsModel.isSubscribed) {
                                        if (restaurantBasicDetailsModel.isRecommended) {
                                            recommendedRestaurantList.add(restaurantBasicDetailsModel);
                                        } else {
                                            notRecommendedRestaurantList.add(restaurantBasicDetailsModel);
                                        }
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }


                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }

                    restaurantList.addAll(recommendedRestaurantList);
                    restaurantList.addAll(notRecommendedRestaurantList);

                    //Deals part
                    offersList.clear();
                    int count = 0;
                    for (RestaurantBasicDetailsModel restaurantBasicDetailsModel :
                            restaurantList) {
                        try {
                            offersList.clear();
                            FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseRestaurantNode).child(restaurantBasicDetailsModel.restaurantId).child(Constant.FirebaseOfferNode).addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {

                                    for (DataSnapshot datasnapshot1 :
                                            dataSnapshot.getChildren()) {
                                        RestaurantOffersDetailsModel offerModel = datasnapshot1.getValue(RestaurantOffersDetailsModel.class);

                                        if (!isDateExpired(offerModel.offerEndDate, getDate())) {
                                            offersList.add(offerModel);
                                            offerAdapter.notifyDataSetChanged();
                                        }
                                    }

                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        count = count + 1;
                    }

                    mAdapter.notifyDataSetChanged();
                    offerAdapter.notifyDataSetChanged();

                    if (offersList.isEmpty()) {
                        title1.setVisibility(View.GONE);
                    } else {
                        title1.setVisibility(View.GONE);
                    }

                    if (restaurantList.isEmpty()) {
                        title2.setVisibility(View.GONE);
                        loadingProgressBar.setVisibility(View.GONE);
                        recycler_view.setVisibility(View.GONE);
                        noValueText.setVisibility(View.VISIBLE);
                    } else {
                        title2.setVisibility(View.VISIBLE);
                        loadingProgressBar.setVisibility(View.GONE);
                        recycler_view.setVisibility(View.VISIBLE);
                        noValueText.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public boolean isWithin2Km(Double restLatitude, double restLongitude) {
        float[] results = new float[1];
        Location.distanceBetween(MainActivity.centralLatitude, MainActivity.centralLongitude, restLatitude, restLongitude, results);
        if (coverageValue.equals("")) {
            if (results[0] < 2000.0f) {
                return true;
            }
        } else {
            if (results[0] < Float.valueOf(coverageValue)) {
                return true;
            }
        }

        return false;
    }

    @Override
    public void onResume() {
        super.onResume();
    }
}