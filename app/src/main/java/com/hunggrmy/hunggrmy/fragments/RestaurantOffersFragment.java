package com.hunggrmy.hunggrmy.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.hunggrmy.hunggrmy.R;
import com.hunggrmy.hunggrmy.RestaurantDetailActivity;
import com.hunggrmy.hunggrmy.adapters.OfferAdapter;
import com.hunggrmy.hunggrmy.models.RestaurantOffersDetailsModel;
import com.hunggrmy.hunggrmy.utils.Constant;
import com.yarolegovich.discretescrollview.DiscreteScrollView;
import com.yarolegovich.discretescrollview.transform.Pivot;
import com.yarolegovich.discretescrollview.transform.ScaleTransformer;

import java.util.ArrayList;
import java.util.List;

import static com.hunggrmy.hunggrmy.utils.Utility.getDate;
import static com.hunggrmy.hunggrmy.utils.Utility.isDateExpired;

/**
 * Created by Wasim on 5/19/2018.
 */

public class RestaurantOffersFragment extends Fragment {

    EditText offer_title_input, offer_condition_input, offer_start_date_input, offer_end_date_input, offer_percentage_input, offer_desc_input;
    LinearLayout submit_btn, offer_LL;
    ImageView offer_img_input;
    TextView noresultTV;

    String offerImage = "";
    String offerId = "";
    boolean isUploadingImage = false;

    //ForShowing Existing Offers
    private List<RestaurantOffersDetailsModel> offersList = new ArrayList<>();
    private DiscreteScrollView offer_recycler_view;
    private OfferAdapter mAdapter;

    public RestaurantOffersFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_restaurant_offers, container, false);
        offer_recycler_view = (DiscreteScrollView) rootView.findViewById(R.id.offer_recycler_view);
        offer_title_input = (EditText) rootView.findViewById(R.id.offer_title_input);
        offer_condition_input = (EditText) rootView.findViewById(R.id.offer_condition_input);
        offer_start_date_input = (EditText) rootView.findViewById(R.id.offer_start_date_input);
        offer_end_date_input = (EditText) rootView.findViewById(R.id.offer_end_date_input);
        offer_desc_input = (EditText) rootView.findViewById(R.id.offer_desc_input);
        offer_percentage_input = (EditText) rootView.findViewById(R.id.offer_percentage_input);
        offer_img_input = (ImageView) rootView.findViewById(R.id.offer_img_input);
        offer_LL = (LinearLayout) rootView.findViewById(R.id.offer_LL);
        noresultTV = (TextView) rootView.findViewById(R.id.noresultTV);
        submit_btn = (LinearLayout) rootView.findViewById(R.id.submit_btn);

        offer_recycler_view.setItemTransformer(new ScaleTransformer.Builder()
                .setMaxScale(1.05f)
                .setMinScale(0.8f)
                .setPivotX(Pivot.X.CENTER) // CENTER is a default one
                .setPivotY(Pivot.Y.BOTTOM) // CENTER is a default one
                .build());

        offer_recycler_view.addOnItemChangedListener(new DiscreteScrollView.OnItemChangedListener<RecyclerView.ViewHolder>() {
            @Override
            public void onCurrentItemChanged(@Nullable RecyclerView.ViewHolder viewHolder, int adapterPosition) {
                try {
                    setOffer(offersList.get(adapterPosition).offerId);
                    offer_recycler_view.scrollToPosition(adapterPosition);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        //Initialising menu
        mAdapter = new OfferAdapter(offersList, RestaurantOffersFragment.this);
//        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
//        offer_recycler_view.setLayoutManager(mLayoutManager);
//        offer_recycler_view.setItemAnimator(new DefaultItemAnimator());
        offer_recycler_view.setAdapter(mAdapter);
        showExistingOffers();

//        offer_img_input.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(getContext(), GalleryActivity.class);
//                Params params = new Params();
//                params.setCaptureLimit(1);
//                params.setPickerLimit(1);
//                params.setToolbarColor(0x000000);
//                params.setActionButtonColor(0x000000);
//                params.setButtonTextColor(0xffffff);
//                intent.putExtra(Constants.KEY_PARAMS, params);
//                startActivityForResult(intent, Constants.TYPE_MULTI_PICKER);
//            }
//        });

        submit_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addOffers();
            }
        });

        return rootView;
    }

    public void setOffer(final String offerId1) {
        offer_LL.setVisibility(View.VISIBLE);
        noresultTV.setVisibility(View.GONE);
        FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseRestaurantNode).child(((RestaurantDetailActivity) getActivity()).getRestauranntId()).child(Constant.FirebaseOfferNode).child(offerId1).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                RestaurantOffersDetailsModel offerModel = dataSnapshot.getValue(RestaurantOffersDetailsModel.class);
                try {
                    offerImage = offerModel.offerImage;
                    offerId = offerModel.offerId;
                    isUploadingImage = false;
                    offer_title_input.setText(offerModel.offerImageTitle);
                    offer_condition_input.setText(offerModel.offerCondt);
                    offer_start_date_input.setText(offerModel.offerStartDate);
                    offer_end_date_input.setText(offerModel.offerEndDate);
                    offer_percentage_input.setText(offerModel.offerPercentage);
                    offer_desc_input.setText(offerModel.offerDesc);
                    Glide.with(getContext()).load(offerImage).into(offer_img_input);
                } catch (Exception e) {

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void showExistingOffers() {
        try {
            FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseRestaurantNode).child(((RestaurantDetailActivity) getActivity()).getRestauranntId()).child(Constant.FirebaseOfferNode).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    offersList.clear();
                    for (DataSnapshot datasnapshot1 :
                            dataSnapshot.getChildren()) {
                        RestaurantOffersDetailsModel offerModel = datasnapshot1.getValue(RestaurantOffersDetailsModel.class);
    //                    Toast.makeText(getContext(), offerModel.offerImage, Toast.LENGTH_SHORT).show();
    //                    String imageUrl = datasnapshot1.getValue(String.class);
    //                    ImageModel imagemodel = new ImageModel(imageUrl);
                        if(!isDateExpired(offerModel.offerEndDate, getDate())){
                            offersList.add(offerModel);
                        }
                        mAdapter.notifyDataSetChanged();
                    }
                    if(offersList.isEmpty()){
                        noresultTV.setVisibility(View.VISIBLE);
                    } else {
                        noresultTV.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void addOffers() {
        if (offerId.equals("")) {
            offerId = FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseRestaurantNode).child(((RestaurantDetailActivity) getActivity()).getRestauranntId()).child(Constant.FirebaseOfferNode).push().getKey();
        }
        try {
            uploadOfferInFirebase();
        } catch (Exception e) {
            uploadOfferInFirebase();
        }
    }

    public void uploadOfferInFirebase() {
        if (isUploadingImage) {
            Toast.makeText(getContext(), "Please wait, Image is uploading.", Toast.LENGTH_SHORT).show();
        } else {
            RestaurantOffersDetailsModel restaurantOffersDetailsModel = new RestaurantOffersDetailsModel(((RestaurantDetailActivity) getActivity()).getRestauranntId(), offerId, offerImage, offer_title_input.getText().toString(), offer_condition_input.getText().toString(), offer_start_date_input.getText().toString(), offer_end_date_input.getText().toString(), offer_percentage_input.getText().toString(), offer_desc_input.getText().toString());
            FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseRestaurantNode).child(((RestaurantDetailActivity) getActivity()).getRestauranntId()).child(Constant.FirebaseOfferNode).child(offerId).setValue(restaurantOffersDetailsModel).addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    Toast.makeText(getContext(), "Offer Added successfully", Toast.LENGTH_SHORT).show();
                    resetPage();
                }
            });
        }
    }

    public void resetPage() {
        try {
            offerImage = "";
            offerId = "";
            isUploadingImage = false;
            offer_title_input.setText("");
            offer_condition_input.setText("");
            offer_start_date_input.setText("");
            offer_end_date_input.setText("");
            offer_percentage_input.setText("");
            offer_desc_input.setText("");
            Glide.with(getContext()).load("").into(offer_img_input);
        } catch (Exception e) {

        }
    }

}