package com.hunggrmy.hunggrmy;


import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.hunggrmy.hunggrmy.models.BookTableModel;
import com.hunggrmy.hunggrmy.utils.Constant;
import com.hunggrmy.hunggrmy.utils.NotificationUtil;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class BookTableActivity extends AppCompatActivity {
    EditText ct_input, note_input;
    EditText day_input;
    Calendar myCalendar = Calendar.getInstance();
    EditText nop_input;
    String restId, bookingId = "";
    OnDateSetListener startDate = new C04184();
    LinearLayout submit_btn;
    EditText time_input;

    class C04141 implements OnClickListener {
        C04141() {
        }

        public void onClick(View v) {
            new DatePickerDialog(BookTableActivity.this, BookTableActivity.this.startDate, BookTableActivity.this.myCalendar.get(Calendar.YEAR), BookTableActivity.this.myCalendar.get(Calendar.MONTH), BookTableActivity.this.myCalendar.get(Calendar.DATE)).show();
        }
    }

    class C04162 implements OnClickListener {

        class C04151 implements OnTimeSetListener {
            C04151() {
            }

            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                BookTableActivity.this.time_input.setText(selectedHour + ":" + selectedMinute + "hrs");
            }
        }

        C04162() {
        }

        public void onClick(View v) {
            Calendar mcurrentTime = Calendar.getInstance();
            TimePickerDialog mTimePicker = new TimePickerDialog(BookTableActivity.this, new C04151(), mcurrentTime.get(Calendar.SECOND), mcurrentTime.get(Calendar.MINUTE), true);
            mTimePicker.setTitle("Select Time");
            mTimePicker.show();
        }
    }

    class C04173 implements OnClickListener {
        C04173() {
        }

        public void onClick(View v) {
            BookTableActivity.this.createBookingOrder();
        }
    }

    class C04184 implements OnDateSetListener {
        C04184() {
        }

        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            BookTableActivity.this.myCalendar.set(Calendar.YEAR, year);
            BookTableActivity.this.myCalendar.set(Calendar.MONTH, monthOfYear);
            BookTableActivity.this.myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            BookTableActivity.this.day_input.setText(new SimpleDateFormat("dd/MM/yy", Locale.US).format(BookTableActivity.this.myCalendar.getTime()));
        }
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_table);

        final SharedPreferences pref = getApplicationContext().getSharedPreferences("HungrrMobile", MODE_PRIVATE);
        if (!pref.getBoolean(Constant.isLoggedIn, Boolean.parseBoolean(null))) {
            Toast.makeText(getApplicationContext(), "Please login to book restaurant", Toast.LENGTH_SHORT).show();
            startActivity(new Intent(getApplicationContext(), LoginActivity.class));
        }

        this.day_input = (EditText) findViewById(R.id.day_input);
        this.nop_input = (EditText) findViewById(R.id.nop_input);
        this.time_input = (EditText) findViewById(R.id.time_input);
        this.ct_input = (EditText) findViewById(R.id.ct_input);
        this.note_input = (EditText) findViewById(R.id.note_input);
        this.submit_btn = (LinearLayout) findViewById(R.id.submit_btn);
        try {
            this.restId = getIntent().getStringExtra("restaurantId");
            this.bookingId = getIntent().getStringExtra("bookingId");
        } catch (Exception e) {
            this.restId = "unknown";
            this.bookingId = "";
        }
        this.day_input.setFocusable(false);
        this.time_input.setFocusable(false);
        this.day_input.setOnClickListener(new C04141());
        this.time_input.setOnClickListener(new C04162());
        this.submit_btn.setOnClickListener(new C04173());
        try {
            if(!bookingId.equals("")){
                showbookingDetails();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showbookingDetails() {

        day_input.setEnabled(false);
        nop_input.setEnabled(false);
        time_input.setEnabled(false);

        FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseBookingNode).child(bookingId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                BookTableModel bookTableModel = null;
                try {
                    bookTableModel = dataSnapshot.getValue(BookTableModel.class);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    day_input.setText(bookTableModel.bookingDate);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    nop_input.setText(bookTableModel.noOfPeople);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    time_input.setText(bookTableModel.bookingTime);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    ct_input.setText(bookTableModel.contactNumber);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    note_input.setText(bookTableModel.note);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void createBookingOrder() {
        if(isAllFieldsFilled()){
            String userId = null;
            try {
                userId = getApplicationContext().getSharedPreferences("HungrrMobile", 0).getString(Constant.userId, null);

                Log.d("Got user id::",userId);
            } catch (Exception e) {
                Log.d("Error getting user Id", "");
                userId = "";
                e.printStackTrace();
            }
            if (userId == null) {
                Intent intent = new Intent(BookTableActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
            }
            DatabaseReference bookingReference = null;
            try {
                bookingReference = FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseBookingNode);
            } catch (Exception e) {
                e.printStackTrace();
            }

//            bookingId = "";
            try {
                if( bookingId == null || bookingId.equals("")){
                    bookingId = bookingReference.push().getKey();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                bookingReference.child(bookingId).setValue(new BookTableModel(userId, this.restId, this.time_input.getText().toString(), this.day_input.getText().toString(), this.nop_input.getText().toString(), this.ct_input.getText().toString(), this.note_input.getText().toString(), "PENDING", bookingId));
                Log.d("Booking created::",bookingId);
            } catch (Exception e) {
                e.printStackTrace();
            }
            Log.d("Creating user record::",userId);
            try {
                FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseUserNode).child(userId).child(Constant.FirebaseBookingNode).push().setValue(bookingId);
                Log.d("user record created::",userId);
            } catch (Exception e) {
                e.printStackTrace();
            }
            Log.d("Creating restu record::",this.restId);
            try {
                FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseRestaurantNode).child(this.restId).child(Constant.FirebaseBookingNode).push().setValue(bookingId);
                Log.d("Created restu record::",this.restId);

            } catch (Exception e) {
                e.printStackTrace();
            }

            this.sendNotification(bookingId);


            Toast.makeText(this, "Booking request sent successfully", Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    private void sendNotification(final String bookingId) {
        try {
            final String finalOrderId = bookingId;
            final String restId = this.restId;
            FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseRestaurantNode).child(this.restId).child("restaurantOwners").addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    for (DataSnapshot owners :
                            dataSnapshot.getChildren()) {

                        String ownerNumber = owners.getValue(String.class);
                        try {
                            if (!ownerNumber.isEmpty()) {
                                Log.d("Owner Number",ownerNumber);
                                FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseUserNode).child(ownerNumber).child("DeviceUniqueId").addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        String deviceUniqueNumber = dataSnapshot.getValue(String.class);
                                        try {
                                            if (!deviceUniqueNumber.isEmpty()) {
                                                FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseUserNode).child("FCMTokens").child(deviceUniqueNumber).addListenerForSingleValueEvent(new ValueEventListener() {
                                                    @Override
                                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                                        String fcmToken1 = dataSnapshot.getValue(String.class);
//                                                          Toast.makeText(BookTableActivity.this, fcmToken1, Toast.LENGTH_SHORT).show();
                                                                NotificationUtil.SendNotification("Hunggr table booking", "New Table has been booked: rest Id "+restId, fcmToken1, "", "openactivity", "BookingPage", "", "", "", restId);
                                                    }

                                                    @Override
                                                    public void onCancelled(DatabaseError databaseError) {

                                                    }
                                                });
                                            }
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {

                                    }
                                });
                            } else {
                                Toast.makeText(BookTableActivity.this, "Cannot Notify the restaurant. Please call and book the table.", Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public boolean isAllFieldsFilled(){
        Boolean isFilled = true;
        if(day_input.getText().toString().isEmpty()){
            isFilled = false;
            day_input.setError("Field Required");
        }
        if(nop_input.getText().toString().isEmpty()){
            isFilled = false;
            nop_input.setError("Field Required");
        }
        if(time_input.getText().toString().isEmpty()){
            isFilled = false;
            time_input.setError("Field Required");
        }
        if(ct_input.getText().toString().isEmpty()){
            isFilled = false;
            ct_input.setError("Field Required");
        }
        return isFilled;
    }
}
