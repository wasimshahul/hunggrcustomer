package com.hunggrmy.hunggrmy;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;

public class WebPageActivity extends AppCompatActivity {

    String url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_page);

        WebView webView = (WebView) findViewById(R.id.webView);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setHorizontalScrollBarEnabled(false);
        webView.clearCache(true);
        try {
            url = getIntent().getStringExtra("url");
        } catch (Exception e) {
            url = "http://hunggr.com/";
            e.printStackTrace();
        }
        webView.loadUrl(url);
    }
}
