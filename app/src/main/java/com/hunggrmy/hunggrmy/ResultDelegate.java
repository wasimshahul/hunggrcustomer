package com.hunggrmy.hunggrmy;

import android.util.Log;
import android.widget.Toast;

import com.ipay.IpayResultDelegate;

import java.io.Serializable;

 public class ResultDelegate implements IpayResultDelegate, Serializable {

    private static final long serialVersionUID = 5963066398271211659L;

    public final static int STATUS_OK = 1;
    public final static int STATUS_FAILED = 2;
    public final static int STATUS_CANCELED = 0;

    public void onPaymentSucceeded (String transId, String refNo, String amount, String remarks, String auth)
    {
        Log.e("Logged", "payment success");
        CartActivity.r_status = STATUS_OK;
        CartActivity.r_transactionId = transId;
        CartActivity.r_referenceNo = refNo;
        CartActivity.r_amount = amount;
        CartActivity.r_remarks = remarks;
        CartActivity.r_authCode = auth;
        CartActivity.onPaymentSucceeded(transId, refNo, amount, remarks, auth);
    }

    public void onPaymentFailed (String transId, String refNo, String amount, String remarks, String err)
    {
        Log.e("Logged", "payment failed");
        CartActivity.r_status = STATUS_FAILED;
        CartActivity.r_transactionId = transId;
        CartActivity.r_referenceNo = refNo;
        CartActivity.r_amount = amount;
        CartActivity.r_remarks = remarks;
        CartActivity.r_err = err;
        CartActivity.onPaymentFailed(transId, refNo, amount, remarks, err);
    }

    public void onPaymentCanceled (String transId, String refNo, String amount, String remarks, String errDesc)
    {
        Log.e("Logged", "payment cancelled");

        CartActivity.r_status = STATUS_CANCELED;
        CartActivity.r_transactionId = transId;
        CartActivity.r_referenceNo = refNo;
        CartActivity.r_amount = amount;
        CartActivity.r_remarks = remarks;
        CartActivity.r_err = "canceled";
        CartActivity.onPaymentCanceled( transId,  refNo,  amount,  remarks,  errDesc);
    }

    public void onRequeryResult (String merchantCode, String refNo, String amount, String result)
    {
        // TODO warning, this is a stub to satisfy superclass interface
        // requirements. We do not yet have any meaningful support for
        // requery in this Cordova library yet.
        Log.e("Logged", "payment requeryResult");

    }
}