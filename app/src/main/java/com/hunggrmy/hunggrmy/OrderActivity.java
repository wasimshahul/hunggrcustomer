package com.hunggrmy.hunggrmy;

import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.os.Build;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.hunggrmy.hunggrmy.adapters.OrderedMenuActivityAdapter;
import com.hunggrmy.hunggrmy.adapters.PlacedOrderAdapter;
import com.hunggrmy.hunggrmy.models.ImageModel;
import com.hunggrmy.hunggrmy.models.OrdersModel;
import com.hunggrmy.hunggrmy.models.RestaurantBasicDetailsModel;
import com.hunggrmy.hunggrmy.utils.Constant;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import it.sephiroth.android.library.viewrevealanimator.ViewRevealAnimator;

public class OrderActivity extends AppCompatActivity {
    TextView orderIdTV, orderStatusTV;
    ViewRevealAnimator mViewAnimator;
    ImageView backIcon, restIcon;
    TextView ctNo, ctName, ctAddr, notes;
    TextView restName, restLocation, totalPrice, totalValue, offerValue, packageValue, totalPayingValue;
    String orderId;
    RecyclerView menu_recycler_view;
    private PlacedOrderAdapter mAdapter;
    private ArrayList<String> menuIds = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);

        orderIdTV = (TextView) findViewById(R.id.orderIdTV);
        orderStatusTV = (TextView) findViewById(R.id.orderStatusTV);
        totalPrice = (TextView) findViewById(R.id.totalPrice);
        totalValue = (TextView) findViewById(R.id.totalValue);
        offerValue = (TextView) findViewById(R.id.offerValue);
        packageValue = (TextView) findViewById(R.id.packageValue);
        totalPayingValue = (TextView) findViewById(R.id.totalPayingValue);

        backIcon = (ImageView) findViewById(R.id.backIcon);
        mViewAnimator = (ViewRevealAnimator) findViewById(R.id.animator);
        menu_recycler_view = (RecyclerView) findViewById(R.id.menu_recycler_view);

        this.restName = (TextView) findViewById(R.id.restName);
        this.restLocation = (TextView) findViewById(R.id.restLocation);
        this.restIcon = (ImageView) findViewById(R.id.restIcon);

        this.ctNo = (TextView) findViewById(R.id.ctNo);
        this.ctName = (TextView) findViewById(R.id.ctName);
        this.ctAddr = (TextView) findViewById(R.id.ctAddr);
        this.notes = (TextView) findViewById(R.id.notes);

        this.mAdapter = new PlacedOrderAdapter(this.menuIds);
        this.menu_recycler_view.setLayoutManager(new LinearLayoutManager(this));
        this.menu_recycler_view.setItemAnimator(new DefaultItemAnimator());
        this.menu_recycler_view.setAdapter(this.mAdapter);

        backIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        try {
            orderId = getIntent().getStringExtra("orderId");
            orderIdTV.setText("Order ID : " + orderId);
        } catch (Exception e) {
            orderId = "";
            orderIdTV.setText("NA");
            e.printStackTrace();
        }

        showOrderStatus();
        initiateRestaurantDetails();
        showMenuItems();

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //Do something after 3000ms
                try {
                    if (orderStatusTV.getText().toString().equalsIgnoreCase("APPROVED")) {
                        //                    mViewAnimator.showNext();
                        mViewAnimator.setDisplayedChild(2, true, new Point(250, 250));
                    } else if (orderStatusTV.getText().toString().equalsIgnoreCase("rejected")) {
                        mViewAnimator.setDisplayedChild(3, true, new Point(250, 250));
                    } else {
                        if (mViewAnimator.getDisplayedChild() == 0) {
                            mViewAnimator.setDisplayedChild(1, true, new Point(250, 250));
                        } else {
                            try {
                                mViewAnimator.setDisplayedChild(0, true, new Point(250, 250));
                            } catch (Exception e) {
                                finish();
                                e.printStackTrace();
                            }
                        }
                        handler.postDelayed(this, 3000);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    finish();
                }
            }
        }, 3000);


    }

    private void showMenuItems() {

        menuIds.clear();

        FirebaseDatabase.getInstance().getReference().child(Constant.OrderNode).child(orderId).child("items").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot dataSnapshot1 :
                        dataSnapshot.getChildren()) {
                    String itemId = dataSnapshot1.getKey();
                    String itemNos = dataSnapshot1.getValue(String.class);

                    menuIds.add(itemId + ":" + itemNos);
                    mAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void showOrderStatus() {
        final SharedPreferences pref = getSharedPreferences("HungrrMobile", 0);
        FirebaseDatabase.getInstance().getReference().child(Constant.OrderNode).child(orderId).child("status").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                orderStatusTV.setText(dataSnapshot.getValue(String.class));
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        FirebaseDatabase.getInstance().getReference().child(Constant.OrderNode).child(orderId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                OrdersModel ordersModel = dataSnapshot.getValue(OrdersModel.class);
                totalPrice.setText("ORDER VALUE : " + ordersModel.totalPayingValue);
                totalPayingValue.setText("RM " +ordersModel.totalPayingValue);
                totalValue.setText("RM " +ordersModel.total_price);
                offerValue.setText(ordersModel.offerValue);
                packageValue.setText(ordersModel.packageValue);
                ctName.setText(ordersModel.orderContactName);
                ctNo.setText(ordersModel.orderContactNumber);
                ctAddr.setText(ordersModel.orderContactAddress);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        try {
            FirebaseDatabase.getInstance().getReference().child(Constant.OrderNode).child(orderId).child(Constant.orderNotes).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    String notesValue = dataSnapshot.getValue(String.class);
                    notes.setText(notesValue);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

//        FirebaseDatabase.getInstance().getReference().child(Constant.OrderNode).child(orderId).child(Constant.total_paying_price).addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//                totalPrice.setText("ORDER VALUE : " + dataSnapshot.getValue(String.class));
//                totalPayingValue.setText(dataSnapshot.getValue(String.class));
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//
//            }
//        });

//        FirebaseDatabase.getInstance().getReference().child(Constant.OrderNode).child(orderId).child(Constant.total_price).addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//                totalValue.setText(dataSnapshot.getValue(String.class));
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//
//            }
//        });

//        FirebaseDatabase.getInstance().getReference().child(Constant.OrderNode).child(orderId).child(Constant.offerValue).addListenerForSingleValueEvent(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//                offerValue.setText(dataSnapshot.getValue(String.class));
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//
//            }
//        });

//        FirebaseDatabase.getInstance().getReference().child(Constant.OrderNode).child(orderId).child(Constant.packageValue).addListenerForSingleValueEvent(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//                packageValue.setText(dataSnapshot.getValue(String.class));
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//
//            }
//        });

    }

    private void initiateRestaurantDetails() {
        final SharedPreferences pref = getSharedPreferences("HungrrMobile", 0);
        try {
            FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseUserNode).child(pref.getString(Constant.userId, null)).child(Constant.OrderNode).child(Constant.restautanyId).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    String restaurantId = dataSnapshot.getValue(String.class);
                    FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseRestaurantNode).child(restaurantId).addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            RestaurantBasicDetailsModel restaurantBasicDetailsModel = dataSnapshot.getValue(RestaurantBasicDetailsModel.class);
                            try {
                                Picasso.with(OrderActivity.this).load(((ImageModel) restaurantBasicDetailsModel.restaurantImages.get(0)).getImageLink()).error(R.drawable.bghunggr).into(restIcon);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            restName.setText(restaurantBasicDetailsModel.restaurantName);
                            restLocation.setText(restaurantBasicDetailsModel.restaurantAddress);
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
