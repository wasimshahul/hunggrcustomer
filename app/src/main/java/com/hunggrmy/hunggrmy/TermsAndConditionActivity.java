package com.hunggrmy.hunggrmy;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class TermsAndConditionActivity extends AppCompatActivity {

    WebView tcWv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms_and_condition);

        tcWv = (WebView) findViewById(R.id.tcwv);

        FirebaseDatabase.getInstance().getReference().child("AppControls").child("t&c").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String tc = dataSnapshot.getValue(String.class);
                tcWv.loadData(tc, "text/html","UTF-8");
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
