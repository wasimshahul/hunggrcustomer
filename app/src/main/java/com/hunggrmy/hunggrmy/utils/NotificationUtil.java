package com.hunggrmy.hunggrmy.utils;

import android.util.Log;
import android.widget.Toast;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class NotificationUtil {

    public static void SendNotification(String title, String message, String fcmToken, String image_url, String action, String action_destination, String km, String latitude, String longitude, String restaurantId) {
        Log.d("Request Body::::::::", "here" );
        try {
            OkHttpClient client = new OkHttpClient();
            Log.d("Request Body::::::::", fcmToken );

            RequestBody formBody = new FormBody.Builder()
                    .add("title", title)
                    .add("message", message)
                    .add("image_url", image_url)
                    .add("action", action)
                    .add("action_destination", action_destination)
                    .add("firebase_token", fcmToken)
                    .add("coverage", km + "00")
                    .add("latlng", latitude +","+ longitude)
                    .add("restaurantId", restaurantId)
                    .build();
            Log.d("Request Body", formBody.toString());
            Request request = new Request.Builder()
                    .url(URLConstants.NOTIFICATION_URL)
                    .post(formBody)
                    .build();
            Log.d("Notification Util","making request");
            client.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    Log.d("error", e.getMessage());
                    call.cancel();
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    Log.d("Logged", response.body().string());
                }
            });
        } catch (Exception e) {
            Log.d("Exception", e.getMessage());
        }

    }

}
