package com.hunggrmy.hunggrmy.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;

import com.billing.mobilebillingsystem.main.Utils.LogUtils;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.hunggrmy.hunggrmy.CartActivity;
import com.hunggrmy.hunggrmy.models.RestaurantBasicDetailsModel;
import com.hunggrmy.hunggrmy.models.RestaurantTimeModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import static android.content.Context.MODE_PRIVATE;
import static com.hunggrmy.hunggrmy.utils.Utility.getAndroidDeviceUniqueId;

public class FirebaseFunctions {

    public static DatabaseReference baseReference = FirebaseDatabase.getInstance().getReference();
    public static DatabaseReference appControl = baseReference.child("AppControls");
    public static DatabaseReference mapZoomingRadius = appControl.child("map_radius");
    public static DatabaseReference mapAreaCoverageRadius = appControl.child("map_area_coverage");
    public static DatabaseReference restaurant = baseReference.child(Constant.FirebaseRestaurantNode);

    public static void insertFCMToken(Context context, String FCMTaken){
        Log.e("Logged", "FCM Token is : "+FCMTaken);
        baseReference.child(Constant.FirebaseUserNode).child(Constant.USERFCMTokens).child(getAndroidDeviceUniqueId(context)).setValue(FCMTaken);
    }

    public static void insertDeviceUniqueId(Context context){
        Log.e("Logged", "Inside inserUniqueId");
        final SharedPreferences pref = context.getSharedPreferences("HungrrMobile", MODE_PRIVATE);
        if(Utility.checkIfUserIsLoggedIn(context)){
            Log.e("Logged", "Inside user is logged in");
            baseReference.child(Constant.FirebaseUserNode).child(Utility.getUserId(context)).child("DeviceUniqueId").setValue(getAndroidDeviceUniqueId(context));
        }
    }

    public static void getAllRestaurants(final Context context){
        FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseRestaurantNode).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String restaurantsJson = "";
                JSONArray restaurantsArray = new JSONArray();

                Iterable<DataSnapshot> contactChildren = dataSnapshot.getChildren();

                for (DataSnapshot datasnapshot1 : contactChildren) {
                    final RestaurantBasicDetailsModel restModel = datasnapshot1.getValue(RestaurantBasicDetailsModel.class);
                    restaurantsArray.put(restModel);
                    LogUtils.Log("Logged : "+restModel.restaurantName);
                    if(!contactChildren.iterator().hasNext()){
                        Toast.makeText(context, "Reached Last", Toast.LENGTH_SHORT).show();
                        JSONObject restaurants = new JSONObject();
                        try {
                            restaurants.put("restaurants", restaurantsArray);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public static boolean isRestaurantOpen(final String restaurantId, ValueEventListener valueEventListener){
        final boolean[] isRestaurantOpen = {false};

        FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseRestaurantNode).child(restaurantId).child("isOpen").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                boolean isOpen = false;
                try {
                    isOpen = dataSnapshot.getValue(boolean.class);
                } catch (Exception e) {
                    isOpen = true;
                }
                if (isOpen) {
                    try {
                        FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseRestaurantNode).child(restaurantId).child("time").addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                RestaurantTimeModel restaurantTimeModel = dataSnapshot.getValue(RestaurantTimeModel.class);
                                if (Utility.ifCurrentTimeIsBetweenTwoTimes(restaurantTimeModel.starttime1, restaurantTimeModel.endtime1) || Utility.ifCurrentTimeIsBetweenTwoTimes(restaurantTimeModel.starttime2, restaurantTimeModel.endtime2)) {
                                    isRestaurantOpen[0] = true;
                                } else {
                                    isRestaurantOpen[0] = false;
                                }

                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });
                    } catch (Exception e) {
                        isRestaurantOpen[0] = true;
                    }
                } else {
                    isRestaurantOpen[0] = false;
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        return isRestaurantOpen[0];
    }
}
