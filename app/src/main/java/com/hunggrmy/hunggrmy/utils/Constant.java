package com.hunggrmy.hunggrmy.utils;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

/**
 * Created by Wasim on 5/19/2018.
 */

public class Constant {
    public static final String MAP_API_KEY = "AIzaSyBo8Km-OH1kw2-8HO52RIEef7ne-Et4100";
    public static String FirebaseBookingNode = "bookings";
    public static String FirebaseCartNode = "cart";
    public static String FirebaseCouponNode = "coupons";
    public static String couponUsedBy = "couponUsedBy";
    public static String FirebaseMenuImagesNode = "menuImages";
    public static String FirebaseMenuNode = "menu";
    public static String FirebaseOfferNode = "offers";
    public static String FirebaseRestaurantImagesNode = "restaurantImages";
    public static String FirebaseRestaurantNode = "restaurants";
    public static String FirebaseSearchNode = "search";
    public static String FirebaseTimeNode = "time";
    public static String FirebaseUserNode = "users";
    public static String isLoggedIn = "isLoggedIn";
    public static String reviews = "reviews";
    public static String rating = "rating";
    public static String userId = "userId";
    public static String USER_NAME = "userName";
    public static String cuisines = "cuisines";
    public static String bookingApproved = "APPROVED";
    public static String bookingPending = "PENDING";
    public static String bookingDenied = "DENIED";
    public static String restautanyId = "restaurantId";
    public static String OrderNode = "orders";
    public static String FCMTokens = "FCMTokens";
    public static String USERFCMTokens = "USERFCMTokens";
    public static String items = "items";
    public static String status = "status";
    public static Object pending = "pending";
    public static String ordered_time = "ordered_time";
    public static String total_price = "total_price";
    public static String offerValue = "offerValue";
    public static String packageValue = "packageValue";
    public static String deliveryType = "deliveryType";
    public static String total_paying_price = "totalPayingValue";
    public static String orderContactName = "orderContactName";
    public static String orderContactNumber = "orderContactNumber";
    public static String orderContactAddress = "orderContactAddress";
    public static String deliveryTime = "deliveryTime";
    public static String introURL = "introURL";
    public static String orderNotes = "orderNotes";
    public static String paymentMode = "paymentMode";
    public static String paymentReferenceNumber = "referenceNumber";
    public static String USER_EMAIL = "userEmail";
    public static String USER_PHONE = "userPhone";
    public static String restautantsList = "restautantsList";
}
