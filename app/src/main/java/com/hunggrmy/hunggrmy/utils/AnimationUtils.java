package com.hunggrmy.hunggrmy.utils;

import android.content.Context;
import android.content.res.Resources;
import android.util.TypedValue;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;

public class AnimationUtils {

    public static Animation fadeAnimation(boolean fadeIn) {

        Animation animation = null;
        if (fadeIn)
            animation = new AlphaAnimation(0f, 1.0f);
        else
            animation = new AlphaAnimation(1.0f, 0f);
        animation.setDuration(1000);
        animation.setFillEnabled(true);
        animation.setFillAfter(true);
        return animation;

    }

    public static int dpToPx(Context context, int dp){
        Resources r = context.getResources();
        int px = (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                dp,
                r.getDisplayMetrics()
        );
        return px;
    }

}
