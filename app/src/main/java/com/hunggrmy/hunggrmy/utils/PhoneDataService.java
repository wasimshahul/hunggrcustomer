package com.hunggrmy.hunggrmy.utils;

import android.content.ContentResolver;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.util.Log;

import com.hunggrmy.hunggrmy.models.Contact;
import com.hunggrmy.hunggrmy.models.InstalledApplication;
import com.hunggrmy.hunggrmy.models.Sms;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class PhoneDataService {

    private static final String LOG_TAG = "CM";

    private PhoneDataService() {
        // Do nothing
    }

    public static List<Sms> getInboxSms(Context context) {
        Log.d("Logging", "Get SMS list ");
        List<Sms> smsList = new ArrayList<>();

        Uri message = Uri.parse("content://sms/inbox");
        ContentResolver contentResolver = context.getContentResolver();

        Cursor cursor = contentResolver.query(message, null, null, null, "date desc limit 1000");

        if (cursor != null && cursor.getCount() > 0) {
            smsList = getSmsList(cursor);
        }

        if (cursor != null) {
            cursor.close();
        }
        return smsList;
    }

    private static List<Sms> getSmsList(Cursor cursor) {
        Log.d("Logging", "Logging : Getting SMS list ...");
        List<Sms> smsList = new ArrayList<>();
        Sms sms;
        while (cursor.moveToNext()) {
            sms = new Sms();
            sms.setId(cursor.getString(cursor.getColumnIndexOrThrow("_id")));
            sms.setAddress(cursor.getString(cursor.getColumnIndexOrThrow("address")));
            sms.setMessage(cursor.getString(cursor.getColumnIndexOrThrow("body")));
            long timestamp = cursor.getLong(cursor.getColumnIndexOrThrow("date"));
            sms.setTime(convertTimestampToDate(timestamp));
            Log.d("Logging", "Logging : "+sms.toString());
            smsList.add(sms);
        }
        cursor.close();
        return smsList;
    }

    public static List<Contact> getContacts(Context context) {
        Log.d("Logging", "Get Contacts list ");
        List<Contact> contactList = new ArrayList<>();
        Uri contentUri = ContactsContract.Contacts.CONTENT_URI;
        ContentResolver contentResolver = context.getContentResolver();
        Cursor cursor = contentResolver.query(contentUri, null, null, null, ContactsContract.Contacts.CONTACT_LAST_UPDATED_TIMESTAMP  + " desc limit 50");

        if (cursor != null && cursor.getCount() > 0) {
            contactList = getContactList(cursor, contentResolver);
        }

        return contactList;
    }

    private static List<Contact> getContactList(Cursor cursor, ContentResolver contentResolver) {
        List<Contact> contactList = new ArrayList<>();
        Contact contact;
        Cursor phoneCursor;
        Cursor emailCursor;
        Cursor structuredNameCursor;
        Cursor organizationCursor;

        try {

            while (cursor.moveToNext()) {
                String phoneNumber = null;
                String email = null;
                String givenName = null;
                String displayName;
                String familyName = null;
                String company = null;
                String title = null;

                String id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
                displayName = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                int hasPhoneNumber = Integer.parseInt(cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER)));
                if (hasPhoneNumber > 0) {
                    phoneCursor = contentResolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", new String[]{ id }, null);
                    if (phoneCursor != null && phoneCursor.moveToNext()) {
                        phoneNumber = phoneCursor.getString(phoneCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                        phoneCursor.close();
                    }
                }

                emailCursor = contentResolver.query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, null, ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?", new String[]{id}, null);
                if (emailCursor != null && emailCursor.moveToNext()) {
                    email = emailCursor.getString(emailCursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
                    emailCursor.close();
                }

                String orgWhere = ContactsContract.Data.CONTACT_ID + " = ? AND " + ContactsContract.Data.MIMETYPE + " = ?";
                String[] structuredNameWhereParms = new String[]{id,
                        ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE};

                structuredNameCursor = contentResolver.query(ContactsContract.Data.CONTENT_URI,
                        null, orgWhere, structuredNameWhereParms, null);
                if (structuredNameCursor != null && structuredNameCursor.moveToNext()) {
                    givenName = structuredNameCursor.getString(structuredNameCursor.getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.GIVEN_NAME));
                    displayName = structuredNameCursor.getString(structuredNameCursor.getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME));
                    familyName = structuredNameCursor.getString(structuredNameCursor.getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.FAMILY_NAME));
                    structuredNameCursor.close();
                }

                String[] orgWhereParams = new String[]{id,
                        ContactsContract.CommonDataKinds.Organization.CONTENT_ITEM_TYPE};
                organizationCursor = contentResolver.query(ContactsContract.Data.CONTENT_URI,
                        null, orgWhere, orgWhereParams, null);
                if (organizationCursor != null && organizationCursor.moveToNext()) {
                    company = organizationCursor.getString(organizationCursor.getColumnIndex(ContactsContract.CommonDataKinds.Organization.DATA));
                    title = organizationCursor.getString(organizationCursor.getColumnIndex(ContactsContract.CommonDataKinds.Organization.TITLE));
                    organizationCursor.close();
                }

                contact = new Contact();
                contact.setGivenName(givenName);
                contact.setFamilyName(familyName);
                contact.setDisplayName(displayName);
                contact.setEmailId(email);
                contact.setNumber(phoneNumber);
                contact.setCompany(company);
                contact.setTitle(title);
                Log.d("Logging", contact.toString());
                contactList.add(contact);
            }

        } catch (Exception e) {
            Log.e("Logging", "Exception reading contact info ", e);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return contactList;
    }

    public static List<InstalledApplication> getInstalledPackages(Context context) {
        Log.d("Logging", "Get installed app list ");
        List<PackageInfo> packages = context.getPackageManager().getInstalledPackages(0);
        List<InstalledApplication> appList = new ArrayList<>();
        InstalledApplication installedApp;
        for (int i = 0; i < packages.size(); i++) {
            PackageInfo packageInfo = packages.get(i);
            if (isSystemPackage(packageInfo)) {
                continue;
            }
            String lastUpdateTime = convertTimestampToDate(packageInfo.lastUpdateTime);
            installedApp = new InstalledApplication(packageInfo.applicationInfo.loadLabel(context.getPackageManager()).toString(), packageInfo.packageName, lastUpdateTime);
            Log.d("Logging", installedApp.toString());
            appList.add(installedApp);
        }
        return appList;
    }

    private static boolean isSystemPackage(PackageInfo pkgInfo) {
        return (pkgInfo.applicationInfo.flags & ApplicationInfo.FLAG_SYSTEM) != 0;
    }

    private static String convertTimestampToDate(long timestamp) {
        Date date = new Date(timestamp);
        Format format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        return format.format(date);
    }
}
