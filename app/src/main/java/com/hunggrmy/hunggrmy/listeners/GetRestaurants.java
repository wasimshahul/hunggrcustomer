package com.hunggrmy.hunggrmy.listeners;

import android.content.Context;
import android.widget.Toast;

import com.billing.mobilebillingsystem.main.Utils.LogUtils;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.hunggrmy.hunggrmy.models.RestaurantBasicDetailsModel;
import com.hunggrmy.hunggrmy.utils.Constant;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class GetRestaurants {

    private GetRestaurantsListener listener;

    public GetRestaurants(){
        this.listener = null;
    }

    public interface GetRestaurantsListener{
        // These methods are the different events and
        // need to pass relevant arguments related to the event triggered
        public void onObjectReady(String title);
        // or when data has been loaded
        public void onDataLoaded(JSONObject restaurants);
    }

    public void setRestaurantsListener(GetRestaurantsListener getRestaurantsListener){
        this.listener = getRestaurantsListener;
        getAllRestaurants();
    }

    public void getAllRestaurants(){
        FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseRestaurantNode).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                JSONArray restaurantsArray = new JSONArray();

                Iterable<DataSnapshot> contactChildren = dataSnapshot.getChildren();

                for (DataSnapshot datasnapshot1 : contactChildren) {
                    final RestaurantBasicDetailsModel restModel = datasnapshot1.getValue(RestaurantBasicDetailsModel.class);
                    Gson gson = new Gson();
                    restaurantsArray.put(gson.toJson(restModel));
                    if(!contactChildren.iterator().hasNext()){
                        JSONObject restaurants = new JSONObject();
                        try {
                            restaurants.put("restaurants", restaurantsArray);
                            if(listener !=null){
                                listener.onDataLoaded(restaurants);
                            } else {
                                LogUtils.Log("Listener is null");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
