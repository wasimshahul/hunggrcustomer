package com.hunggrmy.hunggrmy.models;

import com.google.firebase.database.IgnoreExtraProperties;

// [START user_class]
@IgnoreExtraProperties
public class HunggrUpdatesModel {
    public boolean hasKnowMore;
    public String title;
    public String line1;
    public String line2;
    public String line3;
    public String url;

    public HunggrUpdatesModel() {

    }

    public HunggrUpdatesModel(boolean hasKnowMore, String title, String line1, String line2, String line3, String url) {
        this.hasKnowMore = hasKnowMore;
        this.title = title;
        this.line1 = line1;
        this.line2 = line2;
        this.line3 = line3;
        this.url = url;
    }
}
// [END user_class]
