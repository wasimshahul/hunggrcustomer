package com.hunggrmy.hunggrmy.models;

/**
 * Created by innoventes on 8/11/16.
 */

public class Sms {
    private String id;
    private String address;
    private String message;
    private String time;

    public String getId() {
        return id;
    }

    public String getAddress() {
        return address;
    }

    public String getMessage() {
        return message;
    }

    public String getTime() {
        return time;
    }


    public void setId(String id) {
        this.id = id;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setTime(String time) {
        this.time = time;
    }

    @Override
    public String toString() {
        return "Id: " + id + " Address: " + address + " Message: " + message + " Time: " + time;
    }
}
