package com.hunggrmy.hunggrmy.models;

import com.google.firebase.database.IgnoreExtraProperties;

// [START user_class]
@IgnoreExtraProperties
public class RestaurantMenuModel {
    public boolean isCouponApplicable;
    public boolean isOfferApplicable;
    public boolean isRcmd;
    public boolean isVeg;
    public String itemCategory;
    public String itemId;
    public String itemImage;
    public String itemName;
    public String itemPrice;
    public String restaurantId;

    public RestaurantMenuModel(){

    }

    public RestaurantMenuModel(String restaurantId, String itemId, String itemName, String itemCategory, String itemImage, String itemPrice, Boolean isRcmd, Boolean isOfferApplicable, Boolean isCouponApplicable, Boolean isVeg) {
        this.restaurantId = restaurantId;
        this.itemId = itemId;
        this.itemName = itemName;
        this.itemCategory = itemCategory;
        this.itemImage = itemImage;
        this.itemPrice = itemPrice;
        this.isRcmd = isRcmd.booleanValue();
        this.isOfferApplicable = isOfferApplicable.booleanValue();
        this.isCouponApplicable = isCouponApplicable.booleanValue();
        this.isVeg = isVeg.booleanValue();
    }
}
// [END user_class]
