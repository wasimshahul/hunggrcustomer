package com.hunggrmy.hunggrmy.models;

import com.google.firebase.database.IgnoreExtraProperties;

import java.util.ArrayList;

// [START user_class]
@IgnoreExtraProperties
public class RestaurantBasicDetailsModel {
    public String avgPersonCost;
    public String cuisines;
    public Boolean isAdvertisement;
    public Boolean isRecommended;
    public Boolean isTopRestaurant;
    public Boolean isSubscribed;
    public String ownerContactMail;
    public String ownerContactNumber;
    public String ownerName;
    public String restaurantAddress;
    public String restaurantId;
    public ArrayList<ImageModel> restaurantImages;
    public String restaurantLatLng;
    public String restaurantNote;
    public String restaurantName;
    public String restautantContactMail;
    public String restautantContactNumber;
    public String restaurantType;
    public Boolean isVerifiedRestaurant;

    public RestaurantBasicDetailsModel(){

    }

    public RestaurantBasicDetailsModel(String restaurantId, String restaurantName, String restaurantAddress, String restaurantLatLng, String ownerName, String ownerContactNumber, String ownerContactMail, String restautantContactNumber, String restautantContactMail, ArrayList<ImageModel> restaurantImages, String cuisines, String avgPersonCost, Boolean isRecommended, Boolean isTopRestaurant, Boolean isAdvertisement, String restaurantType, Boolean isSubscribed, String restaurantNote) {
        this.restaurantId = restaurantId;
        this.restaurantName = restaurantName;
        this.restaurantAddress = restaurantAddress;
        this.restaurantLatLng = restaurantLatLng;
        this.ownerName = ownerName;
        this.ownerContactNumber = ownerContactNumber;
        this.ownerContactMail = ownerContactMail;
        this.restautantContactNumber = restautantContactNumber;
        this.restautantContactMail = restautantContactMail;
        this.restaurantImages = restaurantImages;
        this.cuisines = cuisines;
        this.avgPersonCost = avgPersonCost;
        this.isRecommended = isRecommended;
        this.isTopRestaurant = isTopRestaurant;
        this.isAdvertisement = isAdvertisement;
        this.isSubscribed = isSubscribed;
        this.restaurantType = restaurantType;
        this.restaurantNote = restaurantNote;
    }
}
// [END user_class]
