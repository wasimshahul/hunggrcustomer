package com.hunggrmy.hunggrmy.models;

/**
 * Created by Innoventes on 07-12-2016.
 */

public class InstalledApplication {
    private String appName;
    private String packageName;
    private String lastUpdatedTime;

    public InstalledApplication(String appName, String packageName, String lastUpdatedTime) {
        this.appName = appName;
        this.packageName = packageName;
        this.lastUpdatedTime = lastUpdatedTime;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public void setLastUpdatedTime(String lastUpdatedTime) {
        this.lastUpdatedTime = lastUpdatedTime;
    }

    public String getAppName() {
        return appName;
    }

    public String getPackageName() {
        return packageName;
    }

    public String getLastUpdatedTime() {
        return lastUpdatedTime;
    }

    @Override
    public String toString() {
        return "Name : " + appName + " Package Name : " + packageName + " Last Update " + lastUpdatedTime;
    }
}
