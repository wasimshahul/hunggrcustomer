package com.hunggrmy.hunggrmy.models;

import com.google.firebase.database.IgnoreExtraProperties;

// [START user_class]
@IgnoreExtraProperties
public class RestaurantCouponDetailsModel {
    public String offerCondt;
    public String offerDesc;
    public String offerEndDate;
    public String offerId;
    public String offerImage;
    public String offerImageTitle;
    public String offerPercentage;
    public String offerStartDate;
    public String restaurantId;

    public RestaurantCouponDetailsModel(){

    }

    public RestaurantCouponDetailsModel(String restaurantId, String offerId, String offerImage, String offerImageTitle, String offerCondt, String offerStartDate, String offerEndDate, String offerPercentage, String offerDesc) {
        this.restaurantId = restaurantId;
        this.offerId = offerId;
        this.offerImage = offerImage;
        this.offerImageTitle = offerImageTitle;
        this.offerCondt = offerCondt;
        this.offerStartDate = offerStartDate;
        this.offerEndDate = offerEndDate;
        this.offerPercentage = offerPercentage;
        this.offerDesc = offerDesc;
    }
}
// [END user_class]
