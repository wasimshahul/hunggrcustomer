package com.hunggrmy.hunggrmy.models;

/**
 * Created by Wasim on 5/14/2018.
 */

public class CuisineModel {
    private String cuisineName;

    public CuisineModel(){

    }

    public CuisineModel(String cuisineName) {
        this.cuisineName = cuisineName;
    }

    public String getCuisineName() {
        return this.cuisineName;
    }

    public void setCuisineName(String cuisineName) {
        this.cuisineName = cuisineName;
    }
}
