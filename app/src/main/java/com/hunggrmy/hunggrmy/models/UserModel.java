package com.hunggrmy.hunggrmy.models;

import com.google.firebase.database.IgnoreExtraProperties;

/**
 * Created by Wasim on 5/14/2018.
 */

@IgnoreExtraProperties
public class UserModel {
    public String userEmail;
    public String userId;
    public String userImage;
    public String userName;
    public String userPhone;
    public String userPwd;

    public UserModel(){

    }

    public UserModel(String userId, String userImage, String userName, String userPwd, String userPhone, String userEmail) {
        this.userId = userId;
        this.userImage = userImage;
        this.userName = userName;
        this.userPwd = userPwd;
        this.userPhone = userPhone;
        this.userEmail = userEmail;
    }
}