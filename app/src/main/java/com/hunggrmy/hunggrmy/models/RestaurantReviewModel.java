package com.hunggrmy.hunggrmy.models;

import com.google.firebase.database.IgnoreExtraProperties;

// [START user_class]
@IgnoreExtraProperties
public class RestaurantReviewModel {
    public String reviewId;
    public String userId;
    public String review;
    public String restaurantId;
    public String time;

    public RestaurantReviewModel(){

    }

    public RestaurantReviewModel(String reviewId, String restaurantId, String userId, String review, String time) {
        this.restaurantId = restaurantId;
        this.reviewId = reviewId;
        this.userId = userId;
        this.review = review;
        this.time = time;
    }
}
// [END user_class]
