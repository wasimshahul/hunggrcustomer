package com.hunggrmy.hunggrmy.models;

/**
 * Created by Wasim on 5/14/2018.
 */

public class ImageModel {
    private String imageLink;

    public ImageModel(){

    }

    public ImageModel(String imageLink) {
        this.imageLink = imageLink;
    }

    public String getImageLink() {
        return this.imageLink;
    }

    public void setImageLink(String imageLink) {
        this.imageLink = imageLink;
    }
}
