package com.hunggrmy.hunggrmy.models;

/**
 * Created by innoventes on 8/11/16.
 */
public class Contact {
    private String number;
    private String emailId;
    private String givenName;
    private String familyName;
    private String displayName;
    private String company;
    private String title;

    public void setNumber(String number) {
        this.number = number;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public void setGivenName(String givenName) {
        this.givenName = givenName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getNumber() {
        return number;
    }

    public String getEmailId() {
        return emailId;
    }

    public String getGivenName() {
        return givenName;
    }

    public String getDisplayName() {
        return displayName;
    }

    public String getFamilyName() {
        return familyName;
    }

    public String getCompany() {
        return company;
    }

    public String getTitle() {
        return title;
    }

    @Override
    public String toString() {
        return " Number: " + number + " Email: " + emailId + " Given name " + givenName
                + " Family Name: " + familyName + " Display Name : " + displayName +
                " Company : " + company + " Title: " + title;
    }
}
