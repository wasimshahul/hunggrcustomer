package com.hunggrmy.hunggrmy.models;

/**
 * Created by Wasim on 5/14/2018.
 */

public class CouponUsedModel {
    public String couponId;
    public String userId;
    public String restaurantId;
    public String usedTime;
    public String noOfTimeUsed;

    public CouponUsedModel() {
    }

    public CouponUsedModel(String couponId, String userId, String restaurantId, String usedTime, String noOfTimeUsed) {
        this.couponId = couponId;
        this.userId = userId;
        this.restaurantId = restaurantId;
        this.usedTime = usedTime;
        this.noOfTimeUsed = noOfTimeUsed;
    }

}
