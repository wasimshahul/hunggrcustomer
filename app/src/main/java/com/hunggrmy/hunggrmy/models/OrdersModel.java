package com.hunggrmy.hunggrmy.models;

import com.google.firebase.database.IgnoreExtraProperties;

// [START user_class]
@IgnoreExtraProperties
public class OrdersModel {
    public Long ordered_time;
    public String status;
    public String total_price;
    public String restaurantId;
    public String deliveryType;
    public String offerValue;
    public String orderContactAddress;
    public String orderContactName;
    public String orderContactNumber;
    public String packageValue;
    public String totalPayingValue;

    public OrdersModel(){

    }

    public OrdersModel(Long ordered_time, String status, String total_price, String restaurantId, String deliveryType, String offerValue, String orderContactAddress, String orderContactName, String orderContactNumber, String packageValue, String totalPayingValue) {
        this.ordered_time = ordered_time;
        this.status = status;
        this.total_price = total_price;
        this.restaurantId = restaurantId;
        this.restaurantId = deliveryType;
        this.restaurantId = offerValue;
        this.restaurantId = orderContactAddress;
        this.restaurantId = orderContactName;
        this.restaurantId = orderContactNumber;
        this.restaurantId = packageValue;
        this.restaurantId = totalPayingValue;
    }
}
// [END user_class]
