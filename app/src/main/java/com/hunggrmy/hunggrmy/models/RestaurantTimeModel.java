package com.hunggrmy.hunggrmy.models;

import com.google.firebase.database.IgnoreExtraProperties;

/**
 * Created by Wasim on 5/14/2018.
 */
// [START user_class]
@IgnoreExtraProperties
public class RestaurantTimeModel {

    public String starttime1, starttime2, endtime1, endtime2;

    public RestaurantTimeModel() {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }

    public RestaurantTimeModel(String starttime1, String starttime2, String endtime1, String endtime2) {
        this.starttime1 = starttime1;
        this.starttime2 = starttime2;
        this.endtime1 = endtime1;
        this.endtime2 = endtime2;

    }

}
// [END user_class]
