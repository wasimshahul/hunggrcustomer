package com.hunggrmy.hunggrmy.models;

import com.google.firebase.database.IgnoreExtraProperties;

import java.util.ArrayList;

// [START user_class]
@IgnoreExtraProperties
public class RestaurantFullDetailsModel {
    public RestaurantBasicDetailsModel restaurantBasicDetailsModel;
    public ArrayList<RestaurantMenuModel> restaurantMenuModel;

    public RestaurantFullDetailsModel(){

    }

    public RestaurantFullDetailsModel(RestaurantBasicDetailsModel restaurantBasicDetailsModel, ArrayList<RestaurantMenuModel> restaurantMenuModel) {
        this.restaurantBasicDetailsModel = restaurantBasicDetailsModel;
        this.restaurantMenuModel = restaurantMenuModel;
    }
}
// [END user_class]
