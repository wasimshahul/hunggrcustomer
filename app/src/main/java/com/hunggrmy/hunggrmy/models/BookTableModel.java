package com.hunggrmy.hunggrmy.models;

import com.google.firebase.database.IgnoreExtraProperties;

// [START user_class]
@IgnoreExtraProperties
public class BookTableModel {
    public String bookingDate;
    public String bookingId;
    public String bookingTime;
    public String contactNumber;
    public String note;
    public String noOfPeople;
    public String restId;
    public String status;
    public String userId;

    public BookTableModel(){

    }

    public BookTableModel(String userId, String restId, String bookingTime, String bookingDate, String noOfPeople, String contactNumber, String note, String status, String bookingId) {
        this.userId = userId;
        this.restId = restId;
        this.bookingTime = bookingTime;
        this.bookingDate = bookingDate;
        this.noOfPeople = noOfPeople;
        this.contactNumber = contactNumber;
        this.note = note;
        this.status = status;
        this.bookingId = bookingId;
    }
}
// [END user_class]
