package com.hunggrmy.hunggrmy.models;

/**
 * Created by Wasim on 5/14/2018.
 */

public class CouponModel {
    private String imageLink;
    private String id;

    public CouponModel(){

    }

    public CouponModel(String imageLink, String id) {
        this.imageLink = imageLink;
        this.id = id;
    }

    public String getImageLink() {
        return this.imageLink;
    }

    public void setImageLink(String imageLink) {
        this.imageLink = imageLink;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
