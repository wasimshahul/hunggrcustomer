package com.hunggrmy.hunggrmy;


import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.billing.mobilebillingsystem.main.MobileBillingSystem;
import com.billing.mobilebillingsystem.main.Utils.LogUtils;
import com.billing.mobilebillingsystem.main.models.ProductModel;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.hunggrmy.hunggrmy.adapters.OrderedMenuActivityAdapter;
import com.hunggrmy.hunggrmy.interfaces.isRestaurantOpenInterface;
import com.hunggrmy.hunggrmy.models.ImageModel;
import com.hunggrmy.hunggrmy.models.RestaurantBasicDetailsModel;
import com.hunggrmy.hunggrmy.models.RestaurantMenuModel;
import com.hunggrmy.hunggrmy.models.RestaurantOffersDetailsModel;
import com.hunggrmy.hunggrmy.models.RestaurantTimeModel;
import com.hunggrmy.hunggrmy.models.UserModel;
import com.hunggrmy.hunggrmy.notification.NotificationVO;
import com.hunggrmy.hunggrmy.utils.Constant;
import com.hunggrmy.hunggrmy.utils.MyApplication;
import com.hunggrmy.hunggrmy.utils.NotificationUtil;
import com.hunggrmy.hunggrmy.utils.Utility;
import com.ipay.Ipay;
import com.ipay.IpayPayment;
import com.ipay.IpayResultDelegate;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.concurrent.LinkedBlockingDeque;

import gr.escsoft.michaelprimez.searchablespinner.SearchableSpinner;
import gr.escsoft.michaelprimez.searchablespinner.interfaces.OnItemSelectedListener;

import static com.hunggrmy.hunggrmy.utils.Utility.getDate;
import static com.hunggrmy.hunggrmy.utils.Utility.ifTimeIsGreaterThanCurrentTime;
import static com.hunggrmy.hunggrmy.utils.Utility.isDateExpired;

public class CartActivity extends AppCompatActivity {
    public static int r_status;
    public static String r_transactionId;
    public static String r_referenceNo;
    public static String r_amount;
    public static String r_remarks;
    public static String r_authCode;
    public static String r_err;
    private OrderedMenuActivityAdapter mAdapter;
    private List<String> menuList = new ArrayList();
    private RecyclerView menu_recycler_view;
    TextView tot;
    static TextView totalPayingValue;
    static TextView totalValue;
    static TextView offerValue;
    static TextView packageValue;
    TextView item1Value;
    TextView item2Value;
    TextView item3Value;
    TextView totItems;
    TextView restName;
    TextView restLocation;
    TextView placeOrder;
    static TextView chosenPaymentMode;
    TextView changePayment;

    private static Context mContext;

    //    RelativeLayout checkoutCart;
    ImageView backIcon, restIcon;
    LinearLayout paymentModeLL;
    int[] total;
    ArrayList<String> pickUpTime = new ArrayList<>();
    static HashMap<String, String> menuItems;
    RelativeLayout restDetails;
    ScrollView billdetails;
    LinearLayout item1LL, item2LL, item3LL;
    static String deliveryType = "";
    static String deliveryTime = "";
    private static String restaurantId = "";
    static EditText ctNo;
    static EditText ctName;
    static EditText ctAddr;
    static EditText notes;
    static final String refNo = (new Random().nextInt(100000) + 20) + "";
    public String restaurantIdInCart;
    public static String codString = "Cash On Delivery";
    public String olpString = "Online Payment";

    //Billing System
    private static final int INITCODE = 131;
    private static final int BILLRESPONSECODE = 141;
    private static final int ERRORCODE = 111;
    MobileBillingSystem mobileBillingSystem;

    public static void onPaymentSucceeded(String transId, String refNo, String amount, String remarks, String auth) {
        Toast.makeText(MyApplication.getAppContext(), "Payment Success", Toast.LENGTH_SHORT).show();
        String id = FirebaseDatabase.getInstance().getReference().child("transactions").push().getKey();
        FirebaseDatabase.getInstance().getReference().child("transactions").child(id).child(refNo).child("state").setValue("success");
        FirebaseDatabase.getInstance().getReference().child("transactions").child(id).child(refNo).child("transactionId").setValue(transId);
        FirebaseDatabase.getInstance().getReference().child("transactions").child(id).child(refNo).child("amount").setValue(amount);
        FirebaseDatabase.getInstance().getReference().child("transactions").child(id).child(refNo).child("remarks").setValue(remarks);
        FirebaseDatabase.getInstance().getReference().child("transactions").child(id).child(refNo).child("auth").setValue(auth);
        FirebaseDatabase.getInstance().getReference().child("transactions").child(id).child(refNo).child("refNo").setValue(refNo);
        FirebaseDatabase.getInstance().getReference().child("transactions").child(id).child(refNo).child("dateTime").setValue(Utility.getDate()+" "+Utility.getCurrentTime());
        createOrder(restaurantId);

    }

    public static void onPaymentFailed(String transId, String refNo, String amount, String remarks, String err) {
        Log.e("Logged", "payment error : " + r_err);
        Toast.makeText(MyApplication.getAppContext(), "Payment failed. Please try again." + r_err, Toast.LENGTH_SHORT).show();
        String id = FirebaseDatabase.getInstance().getReference().child("transactions").push().getKey();
        FirebaseDatabase.getInstance().getReference().child("transactions").child(id).child(refNo).child("state").setValue("failed");
        FirebaseDatabase.getInstance().getReference().child("transactions").child(id).child(refNo).child("transactionId").setValue(transId);
        FirebaseDatabase.getInstance().getReference().child("transactions").child(id).child(refNo).child("amount").setValue(amount);
        FirebaseDatabase.getInstance().getReference().child("transactions").child(id).child(refNo).child("remarks").setValue(remarks);
        FirebaseDatabase.getInstance().getReference().child("transactions").child(id).child(refNo).child("error").setValue(err);
        FirebaseDatabase.getInstance().getReference().child("transactions").child(id).child(refNo).child("refNo").setValue(refNo);
        FirebaseDatabase.getInstance().getReference().child("transactions").child(id).child(refNo).child("dateTime").setValue(Utility.getDate()+" "+Utility.getCurrentTime());
    }

    public static void onPaymentCanceled(String transId, String refNo, String amount, String remarks, String errDesc) {
        Toast.makeText(MyApplication.getAppContext(), "Payment Cancelled", Toast.LENGTH_SHORT).show();
        String id = FirebaseDatabase.getInstance().getReference().child("transactions").push().getKey();
        FirebaseDatabase.getInstance().getReference().child("transactions").child(id).child(refNo).child("state").setValue("canceled");
        FirebaseDatabase.getInstance().getReference().child("transactions").child(id).child(refNo).child("transactionId").setValue(transId);
        FirebaseDatabase.getInstance().getReference().child("transactions").child(id).child(refNo).child("amount").setValue(amount);
        FirebaseDatabase.getInstance().getReference().child("transactions").child(id).child(refNo).child("remarks").setValue(remarks);
        FirebaseDatabase.getInstance().getReference().child("transactions").child(id).child(refNo).child("errorDescription").setValue(errDesc);
        FirebaseDatabase.getInstance().getReference().child("transactions").child(id).child(refNo).child("refNo").setValue(refNo);
        FirebaseDatabase.getInstance().getReference().child("transactions").child(id).child(refNo).child("dateTime").setValue(Utility.getDate()+" "+Utility.getCurrentTime());
    }

    public void onRequeryResult(String s, String s1, String s2, String s3) {
//        Toast.makeText(MyApplication.getAppContext(), "On Requery Result", Toast.LENGTH_SHORT).show();

    }

    class C05091 implements ValueEventListener {
        C05091() {
        }

        public void onDataChange(DataSnapshot dataSnapshot) {
            CartActivity.this.menuList.clear();
            if (dataSnapshot.hasChildren()) {
                initiateRestaurantDetails();
                for (DataSnapshot datasnapshot1 : dataSnapshot.getChildren()) {
                    CartActivity.this.menuList.add(datasnapshot1.getKey());
                    CartActivity.this.mAdapter.notifyDataSetChanged();
                }
            }

        }

        public void onCancelled(DatabaseError databaseError) {
        }
    }

    class C05112 implements ValueEventListener {
        C05112() {
        }

        public void onDataChange(DataSnapshot dataSnapshot) {
            total = new int[]{0};
            int[] totalItems = new int[]{0};
            if (dataSnapshot.hasChildren()) {
                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {

                    String menuId = dataSnapshot1.getKey();
                    final String menuNos = (String) dataSnapshot1.getValue(String.class);
                    menuItems.put(menuId, menuNos);
                    totalItems[0] = totalItems[0] + Integer.parseInt(menuNos);
                    CartActivity.this.totItems.setText(totalItems[0] + "");
                    FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseRestaurantNode).child(Constant.FirebaseMenuNode).child(menuId).addValueEventListener(new ValueEventListener() {
                        public void onDataChange(DataSnapshot dataSnapshot) {

                            RestaurantMenuModel restaurantMenuModel = dataSnapshot.getValue(RestaurantMenuModel.class);

                            ProductModel productModel = new ProductModel(restaurantMenuModel.itemId, Double.parseDouble(restaurantMenuModel.itemPrice), restaurantMenuModel.itemName, 1.0, 1.0, 1.0);
                            mobileBillingSystem.addItem(productModel, Integer.parseInt(menuNos));
                            LogUtils.Log(restaurantMenuModel.itemId+" added successfully");

                            total[0] = total[0] + (Integer.parseInt(((RestaurantMenuModel) dataSnapshot.getValue(RestaurantMenuModel.class)).itemPrice) * Integer.parseInt(menuNos));
                            CartActivity.this.tot.setText("RM " + total[0]);
                            CartActivity.this.totalPayingValue.setText("RM " + totalPayingValue());
                            CartActivity.this.totalValue.setText("RM " + total[0]);
                        }

                        public void onCancelled(DatabaseError databaseError) {
                            CartActivity.this.tot.setText("RM 0");
                            CartActivity.this.totItems.setText("0");
                            CartActivity.this.totalPayingValue.setText("RM 0");
                            CartActivity.this.totalValue.setText("RM 0");
                        }
                    });

                }
                return;
            }
            CartActivity.this.tot.setText("RM 0");
            CartActivity.this.totItems.setText("0");
            CartActivity.this.totalPayingValue.setText("RM 0");
            CartActivity.this.totalValue.setText("RM 0");
        }

        public void onCancelled(DatabaseError databaseError) {
            CartActivity.this.tot.setText("RM 0");
            CartActivity.this.totItems.setText("0");
            CartActivity.this.totalPayingValue.setText("RM 0");
            CartActivity.this.totalValue.setText("RM 0");
        }
    }

    private float totalPayingValue() {
        float billTotalValue = Float.parseFloat(tot.getText().toString().replace("RM", "").replace("+", "").replace("-", "").trim());
        float billTotalValueOriginal = billTotalValue;

        if (offerValue.getText().toString().contains("-")) {
            float billOfferValue = 0;
            try {
                billOfferValue = Float.parseFloat(offerValue.getText().toString().replace("RM", "").replace("+", "").replace("-", "").replace("%", "").trim());
            } catch (NumberFormatException e) {
                billOfferValue = 0;
            }
            try {
                billTotalValue = billTotalValue - ((billTotalValueOriginal * billOfferValue) / 100);
            } catch (Exception e) {
                billTotalValue = 0;
                e.printStackTrace();
            }
            Log.e("Logged", "Logged : " + billTotalValue);
        } else {
            float billOfferValue = 0;
            try {
                billOfferValue = Float.parseFloat(offerValue.getText().toString().replace("RM", "").replace("+", "").replace("-", "").replace("%", "").trim());
            } catch (NumberFormatException e) {
                billOfferValue = 0;
            }
            try {
                billTotalValue = billTotalValue + ((billTotalValueOriginal * billOfferValue) / 100);
            } catch (Exception e) {
                billTotalValue = 0;
            }
            Log.e("Logged", "Logged : " + billTotalValue);
        }

        if (packageValue.getText().toString().contains("-")) {
            float billPackageValue = 0;
            try {
                billPackageValue = Float.parseFloat(packageValue.getText().toString().replace("RM", "").replace("+", "").replace("-", "").replace("%", "").trim());
            } catch (NumberFormatException e) {
                billPackageValue = 0;
            }
            try {
                billTotalValue = billTotalValue - ((billTotalValueOriginal * billPackageValue) / 100);
            } catch (Exception e) {
                billTotalValue = 0;
            }
        } else {
            float billPackageValue = 0;
            try {
                billPackageValue = Float.parseFloat(packageValue.getText().toString().replace("RM", "").replace("+", "").replace("-", "").replace("%", "").trim());
            } catch (NumberFormatException e) {
                billPackageValue = 0;
            }
            try {
                billTotalValue = billTotalValue + ((billTotalValueOriginal * billPackageValue) / 100);
            } catch (Exception e) {
                billTotalValue = 0;
            }
        }

        if (item1Value.getText().toString().contains("-")) {
            float billItem1Value = 0;
            try {
                billItem1Value = Float.parseFloat(item1Value.getText().toString().replace("RM", "").replace("+", "").replace("-", "").trim());
            } catch (NumberFormatException e) {
                billItem1Value = 0;
            }
            try {
                billTotalValue = billTotalValue - billItem1Value;
            } catch (Exception e) {
                billTotalValue = 0;
            }
        } else {
            float billItem1Value = 0;
            try {
                billItem1Value = Float.parseFloat(item1Value.getText().toString().replace("RM", "").replace("+", "").replace("-", "").trim());
            } catch (NumberFormatException e) {
                billItem1Value = 0;
            }
            try {
                billTotalValue = billTotalValue + billItem1Value;
            } catch (Exception e) {
                billTotalValue = 0;
            }
        }

        if (item2Value.getText().toString().contains("-")) {
            float billItem2Value = 0;
            try {
                billItem2Value = Float.parseFloat(item2Value.getText().toString().replace("RM", "").replace("+", "").replace("-", "").trim());
            } catch (NumberFormatException e) {
                billItem2Value = 0;
            }
            try {
                billTotalValue = billTotalValue - billItem2Value;
            } catch (Exception e) {
                billTotalValue = 0;
            }
        } else {
            float billItem2Value = 0;
            try {
                billItem2Value = Float.parseFloat(item2Value.getText().toString().replace("RM", "").replace("+", "").replace("-", "").trim());
            } catch (NumberFormatException e) {
                billItem2Value = 0;
            }
            try {
                billTotalValue = billTotalValue + billItem2Value;
            } catch (Exception e) {
                billTotalValue = 0;
            }
        }

        if (item3Value.getText().toString().contains("-")) {
            float billItem3Value = 0;
            try {
                billItem3Value = Float.parseFloat(item3Value.getText().toString().replace("RM", "").replace("+", "").replace("-", "").trim());
            } catch (NumberFormatException e) {
                billItem3Value = 0;
            }
            try {
                billTotalValue = billTotalValue - billItem3Value;
            } catch (Exception e) {
                billTotalValue = 0;
            }
        } else {
            float billItem3Value = 0;
            try {
                billItem3Value = Float.parseFloat(item3Value.getText().toString().replace("RM", "").replace("+", "").replace("-", "").trim());
            } catch (NumberFormatException e) {
                billItem3Value = 0;
            }
            try {
                billTotalValue = billTotalValue + billItem3Value;
            } catch (Exception e) {
                billTotalValue = 0;
            }
        }


        return billTotalValue;
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);

        mContext = this;

        this.menu_recycler_view = (RecyclerView) findViewById(R.id.menu_recycler_view);
        this.totItems = (TextView) findViewById(R.id.totItems);
        this.tot = (TextView) findViewById(R.id.tot);
        this.chosenPaymentMode = (TextView) findViewById(R.id.chosenPaymentMode);
        this.changePayment = (TextView) findViewById(R.id.changePayment);
        this.placeOrder = (TextView) findViewById(R.id.placeOrder);
        this.paymentModeLL = (LinearLayout) findViewById(R.id.paymentModeLL);
//        this.checkoutCart = (RelativeLayout) findViewById(R.id.checkoutCart);
        this.backIcon = (ImageView) findViewById(R.id.backIcon);

        this.restName = (TextView) findViewById(R.id.restName);
        this.restLocation = (TextView) findViewById(R.id.restLocation);
        this.restIcon = (ImageView) findViewById(R.id.restIcon);

        this.restDetails = (RelativeLayout) findViewById(R.id.restDetails);
        this.billdetails = (ScrollView) findViewById(R.id.billdetails);

        this.totalPayingValue = (TextView) findViewById(R.id.totalPayingValue);
        this.totalValue = (TextView) findViewById(R.id.totalValue);
        this.offerValue = (TextView) findViewById(R.id.offerValue);
        this.packageValue = (TextView) findViewById(R.id.packageValue);
        this.item1Value = (TextView) findViewById(R.id.item1Value);
        this.item2Value = (TextView) findViewById(R.id.item2Value);
        this.item3Value = (TextView) findViewById(R.id.item3Value);

        this.ctNo = (EditText) findViewById(R.id.ctNo);
        this.ctName = (EditText) findViewById(R.id.ctName);
        this.ctAddr = (EditText) findViewById(R.id.ctAddr);
        this.notes = (EditText) findViewById(R.id.notes);

        this.item1LL = (LinearLayout) findViewById(R.id.item1LL);
        this.item2LL = (LinearLayout) findViewById(R.id.item2LL);
        this.item3LL = (LinearLayout) findViewById(R.id.item3LL);

        this.item1LL.setVisibility(View.GONE);
        this.item2LL.setVisibility(View.GONE);
        this.item3LL.setVisibility(View.GONE);

        this.mAdapter = new OrderedMenuActivityAdapter(this.menuList);
        this.menu_recycler_view.setLayoutManager(new LinearLayoutManager(this));
        this.menu_recycler_view.setItemAnimator(new DefaultItemAnimator());
        this.menu_recycler_view.setAdapter(this.mAdapter);
        menuItems = new HashMap<String, String>();

        this.paymentModeLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pickPaymentType();
            }
        });


        this.placeOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (menuList.size() > 0) {
                    try {
                        FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseRestaurantNode).child(restaurantId).child("isOpen").addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                boolean isOpen = false;
                                try {
                                    isOpen = dataSnapshot.getValue(boolean.class);
                                } catch (Exception e) {
                                    isOpen = true;
                                }
                                if (isOpen) {
                                    try {
                                        Log.e("Logged", "Time: " + "isopen");
                                        FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseRestaurantNode).child(restaurantId).child("time").addListenerForSingleValueEvent(new ValueEventListener() {
                                            @Override
                                            public void onDataChange(DataSnapshot dataSnapshot) {
                                                RestaurantTimeModel restaurantTimeModel = dataSnapshot.getValue(RestaurantTimeModel.class);
                                                try {
                                                    if (Utility.ifCurrentTimeIsBetweenTwoTimes(restaurantTimeModel.starttime1, restaurantTimeModel.endtime1) || Utility.ifCurrentTimeIsBetweenTwoTimes(restaurantTimeModel.starttime2, restaurantTimeModel.endtime2)) {
                                                        Log.e("Logged", "Time: " + "intimatedRestaurant1");
                                                        intimateRestaurant();
                                                    } else {
                                                        Toast.makeText(CartActivity.this, "Sorry. The Restaurant is closed now.", Toast.LENGTH_SHORT).show();
                                                    }
                                                } catch (Exception e) {
                                                    Log.e("Logged", "Time: " + "intimatedRestaurant2");
                                                    intimateRestaurant();
                                                }

                                            }

                                            @Override
                                            public void onCancelled(DatabaseError databaseError) {

                                            }
                                        });
                                    } catch (Exception e) {
                                        Log.e("Logged", "Time: " + "intimatedRestaurant3");
                                        intimateRestaurant();
                                    }
                                } else {
                                    Toast.makeText(CartActivity.this, "Sorry. Restaurant is not accepting any orders currently.", Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });
                    } catch (Exception e) {
                        Log.e("Logged", "Time: " + "intimatedRestaurant4");
                        intimateRestaurant();
                    }


                } else {
                    Toast.makeText(CartActivity.this, "Please Add some items in your cart", Toast.LENGTH_SHORT).show();
                }
            }
        });
        this.backIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        //Billing system Initialising
        mobileBillingSystem = new MobileBillingSystem(CartActivity.this, "com.hunggrmy.hunggrmy.CartActivity");
        mobileBillingSystem.initialize("-LbcGPDRt-uUAVCeBSsu");


    }

    public void setPaymentMethod(String paymentMethod) {

        chosenPaymentMode.setText(paymentMethod);

    }

    public static String getPaymentMethod() {

        try {
            if (chosenPaymentMode.getText().toString().isEmpty()) {
                return codString;
            } else {
                return chosenPaymentMode.getText().toString();
            }
        } catch (Resources.NotFoundException e) {
            return codString;
        }
    }

    private void initiatePickUpTimes() {
        pickUpTime.add("Immediate Available");
        pickUpTime.add("30min after cooking time");
        pickUpTime.add("1hr after cooking time");
        pickUpTime.add("1hr 30min after cooking time");
        pickUpTime.add("2hr after cooking time");
    }

    private void setContactDetails() {
        final SharedPreferences pref = getSharedPreferences("HungrrMobile", 0);

        try {
            FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseUserNode).child(pref.getString(Constant.userId, null)).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    UserModel userModel = dataSnapshot.getValue(UserModel.class);
                    try {
                        ctNo.setText(userModel.userPhone);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try {
                        ctName.setText(userModel.userName);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void initiateRestaurantDetails() {
        final SharedPreferences pref = getSharedPreferences("HungrrMobile", 0);
        final ProgressDialog progressDialog = new ProgressDialog(CartActivity.this);
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        try {
            progressDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseUserNode).child(pref.getString(Constant.userId, null)).child(Constant.OrderNode).child(Constant.restautanyId).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    restaurantId = dataSnapshot.getValue(String.class);
                    try {
                        FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseRestaurantNode).child(restaurantId).addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                RestaurantBasicDetailsModel restaurantBasicDetailsModel = dataSnapshot.getValue(RestaurantBasicDetailsModel.class);
                                try {
                                    Picasso.with(CartActivity.this).load(((ImageModel) restaurantBasicDetailsModel.restaurantImages.get(0)).getImageLink()).error(R.drawable.bghunggr).into(restIcon);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                restName.setText(restaurantBasicDetailsModel.restaurantName);
                                restLocation.setText(restaurantBasicDetailsModel.restaurantAddress);
                                showBillDetails(restaurantId);
                                progressDialog.dismiss();
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                                try {
                                    progressDialog.dismiss();
                                    Toast.makeText(CartActivity.this, "Cant fetch restaurant details. Please contact admin.", Toast.LENGTH_SHORT).show();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                    billdetails.setVisibility(View.VISIBLE);
                    restDetails.setVisibility(View.VISIBLE);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    try {
                        progressDialog.dismiss();
                        Toast.makeText(CartActivity.this, "Cant fetch restaurant details. Please contact admin.", Toast.LENGTH_SHORT).show();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void intimateRestaurant() {

        final SharedPreferences pref = getSharedPreferences("HungrrMobile", 0);
        final ProgressDialog progressDialog = new ProgressDialog(CartActivity.this);
        progressDialog.setMessage("Please hold on... While we intimate the restaurant...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        try {
            FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseUserNode).child(pref.getString(Constant.userId, null)).child(Constant.OrderNode).child(Constant.restautanyId).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    String res = dataSnapshot.getValue(String.class);
                    if (!res.isEmpty()) {
                        if (menuItems.size() > 0) {
                            showDeliveryType(res);
                            restaurantIdInCart = res;
                            progressDialog.dismiss();
                        } else {
                            progressDialog.dismiss();
                            Toast.makeText(CartActivity.this, "There are no items in your cart", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        progressDialog.dismiss();
                        //Cant get restaurant ID
                        Toast.makeText(CartActivity.this, "Cant get restaurant Id", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    progressDialog.dismiss();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            progressDialog.dismiss();
        }

    }

    public void showBillDetails(String restaurantId) {
//        Toast.makeText(this, restaurantId, Toast.LENGTH_SHORT).show();
//        try {
//            FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseRestaurantNode).child(restaurantId).child("BILL").child(Constant.offerValue).addListenerForSingleValueEvent(new ValueEventListener() {
//                @Override
//                public void onDataChange(DataSnapshot dataSnapshot) {
//                    try {
////                        Toast.makeText(CartActivity.this, dataSnapshot.getValue(String.class), Toast.LENGTH_SHORT).show();
//                        offerValue.setText(dataSnapshot.getValue(String.class));
//                        if (offerValue.getText().toString().isEmpty()) {
//                            offerValue.setText("RM 0.00");
//                        }
//                    } catch (Exception e) {
//                        offerValue.setText("RM 0.00");
//                        e.printStackTrace();
//                    }
//                    CartActivity.this.totalPayingValue.setText("RM " + totalPayingValue());
//
//                }
//
//                @Override
//                public void onCancelled(DatabaseError databaseError) {
//
//                }
//
//            });
//        } catch (Exception e) {
//            offerValue.setText("RM 0.00");
//        }

        try {
            getExistingOffers(restaurantId);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseRestaurantNode).child(restaurantId).child("BILL").child(Constant.packageValue).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    try {
//                        Toast.makeText(CartActivity.this, dataSnapshot.getValue(String.class), Toast.LENGTH_SHORT).show();
                        packageValue.setText(dataSnapshot.getValue(String.class));
                        if (packageValue.getText().toString().isEmpty()) {
                            packageValue.setText("RM 0.00");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        packageValue.setText("RM 0.00");
                    }
                    CartActivity.this.totalPayingValue.setText("RM " + totalPayingValue());

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            packageValue.setText("RM 0.00");
        }

    }

    public void showExistingMenu() {
        SharedPreferences pref = getSharedPreferences("HungrrMobile", 0);
        if (pref.getBoolean(Constant.isLoggedIn, Boolean.parseBoolean(null))) {
            try {
                FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseUserNode).child(pref.getString(Constant.userId, null)).child(Constant.FirebaseCartNode).addValueEventListener(new C05091());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void showCartItems() {
        SharedPreferences pref = getSharedPreferences("HungrrMobile", 0);
        if (pref.getBoolean(Constant.isLoggedIn, Boolean.parseBoolean(null))) {
            try {
                FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseUserNode).child(pref.getString(Constant.userId, null)).child(Constant.FirebaseCartNode).addValueEventListener(new C05112());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void moveToPaymentType() {
        if (getPaymentMethod().equals(codString)) {
            createOrder(restaurantId);
        } else {
            moveToPaymentSDK();
        }
    }

    private void showDeliveryType(final String restaurantId) {
        final BottomSheetDialog dialog2 = new BottomSheetDialog(CartActivity.this);
        dialog2.setContentView(R.layout.delivery_type);

        // set the custom dialog components - text, image and button
        final Spinner spinner = dialog2.findViewById(R.id.spinner);
        ArrayAdapter mSimpleArrayListAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, pickUpTime);
        final TextView no = (TextView) dialog2.findViewById(R.id.noTv);
        TextView yes = (TextView) dialog2.findViewById(R.id.yesTv);
        final TextView deliveryTimeTV = (TextView) dialog2.findViewById(R.id.deliveryTime);
        final TextView cookingTime = (TextView) dialog2.findViewById(R.id.cookingTime);
        final boolean[] isDeliveryAvailable = {false};
        spinner.setAdapter(mSimpleArrayListAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        deliveryTimeTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(CartActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        if (ifTimeIsGreaterThanCurrentTime(selectedHour + ":" + selectedMinute)) {
                            deliveryTimeTV.setText(Utility.convert24HrTimeTo12HrFormat(selectedHour + ":" + selectedMinute));
                            deliveryTime = deliveryTimeTV.getText().toString();
                        } else {
                            Toast.makeText(CartActivity.this, "Please select later time of today.", Toast.LENGTH_SHORT).show();
                        }

                    }
                }, hour, minute, false);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();
            }
        });

        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deliveryType = "PICKUP";
                restaurantIdInCart = restaurantId;
                moveToPaymentType();
            }
        });


        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deliveryType = "DELIVERY";
                moveToPaymentSDK();
                restaurantIdInCart = restaurantId;
                moveToPaymentType();
            }
        });


        FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseRestaurantNode).child(restaurantId).child("avg_cooking_time").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String ct;
                try {
                    ct = dataSnapshot.getValue(String.class);
                } catch (Exception e) {
                    ct = "23";
                    e.printStackTrace();
                }
                if (ct.equals("")) {
                    ct = "0";
                }
                deliveryTimeTV.setText(Utility.getCookingTime(ct));
                cookingTime.setText("Usually takes " + ct + " min to prepare food");
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        try {
            FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseRestaurantNode).child(restaurantId).child("isDeliveryAvailable").addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    try {
                        isDeliveryAvailable[0] = dataSnapshot.getValue(boolean.class);
                    } catch (Exception e) {
                        isDeliveryAvailable[0] = false;
                    }
                    if (isDeliveryAvailable[0]) {
                        no.setVisibility(View.VISIBLE);
                    } else {
                        no.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        } catch (Exception e) {
            no.setVisibility(View.GONE);
            e.printStackTrace();
        }

        try {
            dialog2.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            dialog2.show();
        } catch (Exception e) {
//            finish();
            showDeliveryType(restaurantId);
            e.printStackTrace();
        }
    }

    private void pickPaymentType() {
        final BottomSheetDialog dialog2 = new BottomSheetDialog(CartActivity.this);
        dialog2.setContentView(R.layout.pick_payment);

        // set the custom dialog components - text, image and button
        final TextView oLTV = (TextView) dialog2.findViewById(R.id.oLTv);
        final TextView codTV = (TextView) dialog2.findViewById(R.id.codTv);
        final boolean[] isCODAvailable = {false};

        oLTV.setText(olpString);
        codTV.setText(codString);

        oLTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                setPaymentMethod(olpString);
                dialog2.dismiss();
            }
        });


        codTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setPaymentMethod(codString);
                dialog2.dismiss();
            }
        });


        try {
            FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseRestaurantNode).child(restaurantId).child("isCashOnDeliveryAvailable").addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    try {
                        isCODAvailable[0] = dataSnapshot.getValue(boolean.class);
                    } catch (Exception e) {
                        isCODAvailable[0] = false;
                    }
                    if (isCODAvailable[0]) {
                        codTV.setVisibility(View.VISIBLE);
                    } else {
                        codTV.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        } catch (Exception e) {
            codTV.setVisibility(View.VISIBLE);
            e.printStackTrace();
        }

        try {
            dialog2.show();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void moveToPaymentSDK() {
        final SharedPreferences pref = getSharedPreferences("HungrrMobile", MODE_PRIVATE);
        IpayPayment payment = new IpayPayment();
        payment.setMerchantKey("SNNOxX3p9m");
        payment.setMerchantCode("M19478");
        payment.setPaymentId("2");
        payment.setCurrency("MYR");
        payment.setRefNo(refNo);
        payment.setAmount("1.00");
        payment.setProdDesc("Food purchase");
        payment.setUserName(Utility.getUserName(CartActivity.this));
        payment.setUserEmail(pref.getString(Constant.USER_EMAIL, "hunggr.my@gmail.com"));
        payment.setUserContact(pref.getString(Constant.USER_PHONE, "9486749788"));
        payment.setCountry("MY");
        payment.setBackendPostURL("http://www.hunggr.com/paymentHandler.php");

        Intent checkoutIntent = Ipay.getInstance().checkout(payment, this, new ResultDelegate());
        startActivityForResult(checkoutIntent, 1);
    }

    public static void createOrder(final String restaurantId) {

        Toast.makeText(MyApplication.getAppContext(), "Creating order", Toast.LENGTH_SHORT).show();
        final SharedPreferences pref = MyApplication.getAppContext().getSharedPreferences("HungrrMobile", MODE_PRIVATE);
        //Create order
        String orderId = null;
        try {

            if (validateMobileNumber(ctNo) && validateName(ctName)) {
                orderId = FirebaseDatabase.getInstance().getReference().child(Constant.OrderNode).child(pref.getString(Constant.userId, null)).push().getKey();
                FirebaseDatabase.getInstance().getReference().child(Constant.OrderNode).child(orderId).child(Constant.items).setValue(menuItems);
                FirebaseDatabase.getInstance().getReference().child(Constant.OrderNode).child(orderId).child(Constant.status).setValue(Constant.pending);
                FirebaseDatabase.getInstance().getReference().child(Constant.OrderNode).child(orderId).child(Constant.ordered_time).setValue(System.currentTimeMillis());
                FirebaseDatabase.getInstance().getReference().child(Constant.OrderNode).child(orderId).child(Constant.total_price).setValue(totalValue.getText().toString().replace("RM", ""));
                FirebaseDatabase.getInstance().getReference().child(Constant.OrderNode).child(orderId).child(Constant.total_paying_price).setValue(totalPayingValue.getText().toString().replace("RM", ""));
                FirebaseDatabase.getInstance().getReference().child(Constant.OrderNode).child(orderId).child(Constant.restautanyId).setValue(restaurantId);
                FirebaseDatabase.getInstance().getReference().child(Constant.OrderNode).child(orderId).child(Constant.offerValue).setValue(offerValue.getText().toString());
                FirebaseDatabase.getInstance().getReference().child(Constant.OrderNode).child(orderId).child(Constant.packageValue).setValue(packageValue.getText().toString());
                FirebaseDatabase.getInstance().getReference().child(Constant.OrderNode).child(orderId).child(Constant.deliveryType).setValue(deliveryType);
                FirebaseDatabase.getInstance().getReference().child(Constant.OrderNode).child(orderId).child(Constant.paymentMode).setValue(getPaymentMethod());
                FirebaseDatabase.getInstance().getReference().child(Constant.OrderNode).child(orderId).child(Constant.paymentReferenceNumber).setValue(refNo);
                FirebaseDatabase.getInstance().getReference().child(Constant.OrderNode).child(orderId).child(Constant.deliveryTime).setValue(deliveryTime);
                FirebaseDatabase.getInstance().getReference().child(Constant.OrderNode).child(orderId).child(Constant.orderContactName).setValue(ctName.getText().toString());
                FirebaseDatabase.getInstance().getReference().child(Constant.OrderNode).child(orderId).child(Constant.orderContactNumber).setValue(ctNo.getText().toString());
                FirebaseDatabase.getInstance().getReference().child(Constant.OrderNode).child(orderId).child(Constant.orderContactAddress).setValue(ctAddr.getText().toString());
                FirebaseDatabase.getInstance().getReference().child(Constant.OrderNode).child(orderId).child(Constant.orderNotes).setValue(notes.getText().toString());


                //Remove Cart items of user
                try {
                    FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseUserNode).child(pref.getString(Constant.userId, null)).child(Constant.FirebaseCartNode).removeValue();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                //Set order record for user
                try {
                    FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseUserNode).child(pref.getString(Constant.userId, null)).child(Constant.OrderNode).child(orderId).setValue(orderId);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                //Set order record for restaurant
                try {
                    FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseRestaurantNode).child(restaurantId).child(Constant.OrderNode).child(orderId).setValue(orderId);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                //Send Notification for restaurant
                try {
                    final String finalOrderId = orderId;
                    FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseRestaurantNode).child(restaurantId).child("restaurantOwners").addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {

                            for (DataSnapshot owners :
                                    dataSnapshot.getChildren()) {

                                String ownerNumber = owners.getValue(String.class);
                                Log.e("Logged", "noti : ownerNumber : " + ownerNumber);
                                try {
                                    if (!ownerNumber.isEmpty()) {
                                        FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseUserNode).child(ownerNumber).child("DeviceUniqueId").addListenerForSingleValueEvent(new ValueEventListener() {
                                            @Override
                                            public void onDataChange(DataSnapshot dataSnapshot) {
                                                String deviceUniqueNumber = dataSnapshot.getValue(String.class);
                                                Log.e("Logged", "noti : deviceUniqueNumber : " + deviceUniqueNumber);
                                                try {
                                                    if (!deviceUniqueNumber.isEmpty()) {
                                                        FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseUserNode).child("FCMTokens").child(deviceUniqueNumber).addListenerForSingleValueEvent(new ValueEventListener() {
                                                            @Override
                                                            public void onDataChange(DataSnapshot dataSnapshot) {
                                                                String fcmToken1 = dataSnapshot.getValue(String.class);
                                                                Log.e("Logged", "noti : fcmToken1 : " + fcmToken1);
                                                                NotificationUtil.SendNotification("Hunggr Order", "New Order has arrived \n" + "Order id : " + finalOrderId, fcmToken1, "", "openactivity", "OrdersPage", "", "", "", restaurantId);

                                                            }

                                                            @Override
                                                            public void onCancelled(DatabaseError databaseError) {

                                                            }
                                                        });
                                                    }
                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }
                                            }

                                            @Override
                                            public void onCancelled(DatabaseError databaseError) {

                                            }
                                        });
                                    } else {
                                        Toast.makeText(MyApplication.getAppContext(), "Cannot Notify the restaurant. Please call and order.", Toast.LENGTH_SHORT).show();
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                            }

                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Toast.makeText(MyApplication.getAppContext(), "Ordered successfully", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(mContext, OrderActivity.class);
                intent.putExtra("orderId", orderId);
                mContext.startActivity(intent);

            } else {
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private boolean validateAddress(EditText ctAddr) {
        if (ctAddr.getText().toString().isEmpty()) {
            ctAddr.setError("This field is required");
            return false;
        } else {
            return true;
        }
    }

    private static boolean validateName(EditText ctName) {
        if (ctName.getText().toString().isEmpty()) {
            ctName.setError("This field is required");
            return false;
        } else {
            return true;
        }

    }

    private static boolean validateMobileNumber(EditText mobileNo) {
        if (mobileNo.getText().toString().isEmpty()) {
            mobileNo.setError("Contact number is required");
            return false;
        } else if (mobileNo.getText().toString().length() < 10) {
            mobileNo.setError("Please enter a valid mobile number");
            return false;
        } else {
            return true;
        }
    }

    public void getExistingOffers(String restaurantId) {
        try {
            FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseRestaurantNode).child(restaurantId).child(Constant.FirebaseOfferNode).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    ArrayList<RestaurantOffersDetailsModel> offersList = new ArrayList();
                    offersList.clear();
                    for (DataSnapshot datasnapshot1 :
                            dataSnapshot.getChildren()) {
                        RestaurantOffersDetailsModel offerModel = datasnapshot1.getValue(RestaurantOffersDetailsModel.class);
                        //                    Toast.makeText(getContext(), offerModel.offerImage, Toast.LENGTH_SHORT).show();
                        //                    String imageUrl = datasnapshot1.getValue(String.class);
                        //                    ImageModel imagemodel = new ImageModel(imageUrl);
                        if (!isDateExpired(offerModel.offerEndDate, getDate())) {
                            offersList.add(offerModel);
                        }
                        mAdapter.notifyDataSetChanged();
                    }
                    if (offersList.isEmpty()) {
                        offerValue.setText("- RM 0%");
                    } else {
                        offerValue.setText("- RM " + offersList.get(0).offerPercentage + "%");
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onLibraryResult(int code, String result){
        switch (code){
            case INITCODE:
                if(result.equals("success")){
                    LogUtils.Log("Initiasation successful : "+result);
                    if (Utility.checkIfUserIsLoggedIn(MyApplication.getAppContext())) {
                        initiatePickUpTimes();
                        setContactDetails();
                        showExistingMenu();
                        showCartItems();
                    } else {
                        Toast.makeText(this, "Please Log in to continue", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(CartActivity.this, ProfileActivity.class));
                    }
                }
                break;
            case BILLRESPONSECODE:
                LogUtils.Log("Bill response : "+result);
                break;
            case ERRORCODE:
                LogUtils.Log(result);
        }
    }
}