package com.hunggrmy.hunggrmy.networking;

import android.os.AsyncTask;
import android.util.Log;

import com.hunggrmy.hunggrmy.interfaces.OnZomatoApiListener;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class GetRestaurantDetailsFromZomato extends AsyncTask<String, String, String> {

    String url;
    OnZomatoApiListener onZomatoApiListener;

    public GetRestaurantDetailsFromZomato(OnZomatoApiListener onZomatoApiListener, String url){
        this.url = url;
        this.onZomatoApiListener = onZomatoApiListener;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(String... strings) {
        String result = "";
        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
//                .url("https://developers.zomato.com/api/v2.1/search?q=Hot%20Spice%20Kitchen&count=1&lat=13.0412454&lon=80.1744267")
                .url(url)
                .get()
                .addHeader("user-key", "d6b48c05f56e6860c1d4fb066689920c")
                .addHeader("cache-control", "no-cache")
                .addHeader("postman-token", "ae3cd45f-d7ff-4da7-ef60-fce9df79c319")
                .build();

        try {
            Response response = client.newCall(request).execute();
            result = response.body().string();
//            Log.e("Response", response.body().string());
        } catch (IOException e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        Log.e("Response", s);
        onZomatoApiListener.onSuccess(s);
    }
}
