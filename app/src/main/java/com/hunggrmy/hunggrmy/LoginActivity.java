package com.hunggrmy.hunggrmy;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.hunggrmy.hunggrmy.models.UserModel;
import com.hunggrmy.hunggrmy.service.RegistrationIntentService;
import com.hunggrmy.hunggrmy.utils.Constant;

public class LoginActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {
    TextView newuserTV;
    EditText password_input;
    LinearLayout submit_btn;
    EditText userId_title_input;
    private GoogleApiClient mGoogleApiClient;
    private ProgressDialog mProgressDialog;
    private static final int RC_SIGN_IN = 007;
    private SignInButton btnSignIn;
    ImageView gpIcon, fbIcon;

    class C04211 implements OnClickListener {
        C04211() {
        }

        public void onClick(View v) {
            LoginActivity.this.startActivity(new Intent(LoginActivity.this, CreateUserActivity.class));
            finish();
        }
    }

    class C04222 implements OnClickListener {
        C04222() {
        }

        public void onClick(View v) {
            if (userId_title_input.getText().toString().isEmpty()) {
                userId_title_input.setError("Field can't be empty");
            } else if (userId_title_input.getText().toString().isEmpty()) {
                userId_title_input.setError("Field can't be empty");
            } else {
                checkIfUserIsPresent(userId_title_input.getText().toString(), password_input.getText().toString());
                LoginActivity.this.checkIfUserIsPresent(LoginActivity.this.userId_title_input.getText().toString(), LoginActivity.this.password_input.getText().toString());
            }
        }
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        this.btnSignIn = (SignInButton) findViewById(R.id.btn_sign_in);
        this.userId_title_input = (EditText) findViewById(R.id.userId_title_input);
        this.password_input = (EditText) findViewById(R.id.password_input);
        this.newuserTV = (TextView) findViewById(R.id.newuserTV);
        this.submit_btn = (LinearLayout) findViewById(R.id.submit_btn);
        this.gpIcon = (ImageView) findViewById(R.id.gpIcon);
        this.fbIcon = (ImageView) findViewById(R.id.fbIcon);
        this.newuserTV.setOnClickListener(new C04211());
        this.submit_btn.setOnClickListener(new C04222());

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        // Customizing G+ button
        btnSignIn.setSize(SignInButton.SIZE_STANDARD);
        btnSignIn.setScopes(gso.getScopeArray());

        btnSignIn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                signIn();
            }
        });

        checkIfUserIsLoggedIn();
    }

    public void checkIfUserIsLoggedIn() {
        try {
            if (getApplicationContext().getSharedPreferences("HungrrMobile", 0).getBoolean(Constant.isLoggedIn, Boolean.parseBoolean(null))) {
                startActivity(new Intent(this, ProfileMenuActivity.class));
                finish();
                return;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void checkIfUserIsPresent(String userId, final String userPwd) {
        try {
            FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseUserNode).child(userId).addListenerForSingleValueEvent(new ValueEventListener() {
                public void onDataChange(DataSnapshot dataSnapshot) {
                    UserModel userModel = (UserModel) dataSnapshot.getValue(UserModel.class);
                    if (userModel == null) {
                        LoginActivity.this.userId_title_input.setError("User Not Found");
                    } else if (userPwd.equals(userModel.userPwd)) {
                        Editor editor = LoginActivity.this.getApplicationContext().getSharedPreferences("HungrrMobile", 0).edit();
                        editor.putString(Constant.userId, userModel.userId);
                        editor.putBoolean(Constant.isLoggedIn, true);
                        editor.putString(Constant.USER_NAME, userModel.userName);
                        editor.putString(Constant.USER_EMAIL, userModel.userEmail);
                        editor.putString(Constant.USER_PHONE, userModel.userPhone);
                        Intent intent = new Intent(getApplicationContext(), RegistrationIntentService.class);
                        startService(intent);
                        editor.commit();
                        LoginActivity.this.finish();
                        Toast.makeText(LoginActivity.this, "Logged In successfully", Toast.LENGTH_SHORT).show();
                    } else {
                        LoginActivity.this.password_input.setError("Incorrect Password");
                    }
                }

                public void onCancelled(DatabaseError databaseError) {
                    LoginActivity.this.userId_title_input.setError("User Not found");
                }
            });
        } catch (Exception e) {
            this.userId_title_input.setError("User Not found");
        }
    }

    public void checkIfUserIsPresentGPVersion(final String userId1, final GoogleSignInAccount acct) {
        Log.e("Logged", "Checking if userispresent");
        FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseUserNode).child(userId1.replace(".", ":")).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
//                UserModel userModel = (UserModel) dataSnapshot.getValue(UserModel.class);
//                if (userModel == null) {
//                    LoginActivity.this.userId_title_input.setError("User Not Found");
//                } else {
                final UserModel newUserModel = new UserModel(userId1.replace(".", ":"), acct.getPhotoUrl() + "", acct.getDisplayName(), "", "", acct.getEmail());
                try {
                    FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseUserNode).child(userId1.replace(".", ":")).setValue(newUserModel).addOnSuccessListener(new OnSuccessListener<Void>() {
                        public void onSuccess(Void aVoid) {
                            Log.e("Logged", " LogSuccess");
                            Editor editor = LoginActivity.this.getApplicationContext().getSharedPreferences("HungrrMobile", 0).edit();
                            editor.putString(Constant.userId, newUserModel.userId);
                            editor.putBoolean(Constant.isLoggedIn, true);
                            editor.commit();
                            Toast.makeText(LoginActivity.this, "User Logged In", Toast.LENGTH_SHORT).show();
//                            startActivity(new Intent(LoginActivity.this, MapsActivity.class));
                            LoginActivity.this.finish();
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e("Logged", e.getLocalizedMessage() + " LogError");
                }
//                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    private void signOut() {
        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
//                        updateUI(false);
                    }
                });
    }

    private void revokeAccess() {
        Auth.GoogleSignInApi.revokeAccess(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
//                        updateUI(false);
                    }
                });
    }

    private void handleSignInResult(GoogleSignInResult result) {
        Log.d("Logged", "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();

            Log.e("Logged", "display name: " + acct.getDisplayName());

            String personName = acct.getDisplayName();
            String personPhotoUrl = acct.getPhotoUrl().toString();
            String email = acct.getEmail();

            Log.e("Logged", "Name: " + personName + ", email: " + email
                    + ", Image: " + personPhotoUrl);

            checkIfUserIsPresentGPVersion(acct.getEmail(), acct);
//            updateUI(true);
        } else {
            // Signed out, show unauthenticated UI.
//            updateUI(false);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            int statusCode = result.getStatus().getStatusCode();
            Log.d("Logged", "STATUS CODE : " + statusCode);
            handleSignInResult(result);

//            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
//            handleSignInResult(result);
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        OptionalPendingResult<GoogleSignInResult> opr = Auth.GoogleSignInApi.silentSignIn(mGoogleApiClient);
        if (opr.isDone()) {
            // If the user's cached credentials are valid, the OptionalPendingResult will be "done"
            // and the GoogleSignInResult will be available instantly.
            Log.d("Logged", "Got cached sign-in");
//            GoogleSignInResult result = opr.get();
//            handleSignInResult(result);
        } else {
            // If the user has not previously signed in on this device or the sign-in has expired,
            // this asynchronous branch will attempt to sign in the user silently.  Cross-device
            // single sign-on will occur in this branch.
//            showProgressDialog();
//            opr.setResultCallback(new ResultCallback<GoogleSignInResult>() {
//                @Override
//                public void onResult(GoogleSignInResult googleSignInResult) {
//                    hideProgressDialog();
////                    handleSignInResult(googleSignInResult);
//                }
//            });
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        // An unresolvable error has occurred and Google APIs (including Sign-In) will not
        // be available.
        Log.d("Logged", "onConnectionFailed:" + connectionResult);
//        hideProgressDialog();
    }

    private void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(true);
        }

        mProgressDialog.show();
    }

    private void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.hide();
        }
    }

    private void updateUI(boolean isSignedIn) {
        if (isSignedIn) {
            btnSignIn.setVisibility(View.GONE);
//            btnSignOut.setVisibility(View.VISIBLE);
//            btnRevokeAccess.setVisibility(View.VISIBLE);
//            llProfileLayout.setVisibility(View.VISIBLE);
        } else {
            btnSignIn.setVisibility(View.VISIBLE);
//            btnSignOut.setVisibility(View.GONE);
//            btnRevokeAccess.setVisibility(View.GONE);
//            llProfileLayout.setVisibility(View.GONE);
        }
    }

}
