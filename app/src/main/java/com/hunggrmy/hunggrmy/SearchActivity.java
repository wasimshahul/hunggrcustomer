package com.hunggrmy.hunggrmy;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.hunggrmy.hunggrmy.adapters.CuisineAdapter;
import com.hunggrmy.hunggrmy.adapters.SearchRestaurantListAdapter;
import com.hunggrmy.hunggrmy.models.RestaurantBasicDetailsModel;
import com.hunggrmy.hunggrmy.utils.Constant;

import java.util.ArrayList;
import java.util.List;

public class SearchActivity extends AppCompatActivity {

    RecyclerView rest_recycler_view;
    private List<RestaurantBasicDetailsModel> restaurantList = new ArrayList<>();
    private List<RestaurantBasicDetailsModel> filteredRestaurantList = new ArrayList<>();
    private SearchRestaurantListAdapter mRestaurantAdapter;

    RecyclerView cuisine_recycler_view;
    List<String> cuisineList = new ArrayList<>();
    private CuisineAdapter cuisineAdapter;

    public List<String> filterCuisineList = new ArrayList<>();

    EditText rest_name_edt;
    String userId;
    TextView restarant_nos;
    ImageView bookingimg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        rest_recycler_view = (RecyclerView) findViewById(R.id.rcmd_rest_recycler_view);
        cuisine_recycler_view = (RecyclerView) findViewById(R.id.cuisine_recycler_view);
        rest_name_edt = (EditText) findViewById(R.id.rest_name_edt);
        restarant_nos = (TextView) findViewById(R.id.restarant_nos);
        bookingimg = (ImageView) findViewById(R.id.bookingimg);

        //All Restaurants
        mRestaurantAdapter = new SearchRestaurantListAdapter(filteredRestaurantList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(SearchActivity.this);
        rest_recycler_view.setLayoutManager(mLayoutManager);
        rest_recycler_view.setItemAnimator(new DefaultItemAnimator());
        rest_recycler_view.setAdapter(mRestaurantAdapter);

        //All cuisines
        cuisineAdapter = new CuisineAdapter(cuisineList);
        RecyclerView.LayoutManager cuisineLayoutManager = new LinearLayoutManager(SearchActivity.this);
        cuisine_recycler_view.setLayoutManager(cuisineLayoutManager);
        cuisine_recycler_view.setItemAnimator(new DefaultItemAnimator());
        cuisine_recycler_view.setAdapter(cuisineAdapter);

        getAllRestaurants();
        getAllCuisines();

        bookingimg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        try {
            rest_name_edt.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    if (rest_name_edt.getText().toString().isEmpty()) {
                        filteredRestaurantList.clear();
                        mRestaurantAdapter.notifyDataSetChanged();
                    }
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    filteredRestaurantList.clear();
                    for (RestaurantBasicDetailsModel restBasicDetailsModel :
                            restaurantList) {
                        try {
                            if (restBasicDetailsModel.restaurantName.contains(rest_name_edt.getText().toString())) {
                                filteredRestaurantList.add(restBasicDetailsModel);
                            }
                        } catch (Exception e) {

                        }
                        mRestaurantAdapter.notifyDataSetChanged();
                    }
                    if (rest_name_edt.getText().toString().isEmpty()) {
                        filteredRestaurantList.clear();
                        mRestaurantAdapter.notifyDataSetChanged();
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {
                    filteredRestaurantList.clear();
                    for (RestaurantBasicDetailsModel restBasicDetailsModel :
                            restaurantList) {
                        try {
                            if (restBasicDetailsModel.restaurantName.toLowerCase().contains(rest_name_edt.getText().toString().toLowerCase())) {
                                filteredRestaurantList.add(restBasicDetailsModel);
                            }
                        } catch (Exception e) {

                        }
                        mRestaurantAdapter.notifyDataSetChanged();
                    }
                    if (rest_name_edt.getText().toString().isEmpty()) {
                        filteredRestaurantList.clear();
                        mRestaurantAdapter.notifyDataSetChanged();
                    }
                }
            });
        } catch (Exception e) {

        }

    }

    private void getAllCuisines() {
        try {
            FirebaseDatabase.getInstance().getReference().child(Constant.cuisines).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    for (DataSnapshot data :
                            dataSnapshot.getChildren()) {
                        cuisineList.add(data.getValue(String.class).toLowerCase());
                    }
                    cuisineAdapter.notifyDataSetChanged();
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getAllRestaurants() {
        try {
            FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseRestaurantNode).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    restaurantList.clear();
                    for (DataSnapshot datasnapshot1 :
                            dataSnapshot.getChildren()) {
                        final RestaurantBasicDetailsModel restModel = datasnapshot1.getValue(RestaurantBasicDetailsModel.class);
                        restaurantList.add(restModel);
                        filteredRestaurantList.add(restModel);
                        mRestaurantAdapter.notifyDataSetChanged();
                    }
                    restarant_nos.setText(filteredRestaurantList.size() + " Restaurants");
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        } catch (Exception e) {

        }
    }

    public void refrestFilteredList() {
        if (filterCuisineList.isEmpty() || filteredRestaurantList.isEmpty()) {
            restaurantList.clear();
            filteredRestaurantList.clear();
            try {
                getAllRestaurants();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            filteredRestaurantList.clear();
            try {
                for (RestaurantBasicDetailsModel rest :
                        restaurantList) {
                    try {
                        if (filterCuisineList.contains(rest.cuisines.toLowerCase())) {
                            filteredRestaurantList.add(rest);
                        }
                    } catch (ArrayIndexOutOfBoundsException e) {
                        e.printStackTrace();
                    }
                    if (!filteredRestaurantList.isEmpty()) {
                        restarant_nos.setText(filteredRestaurantList.size() + " Restaurants");
                        mRestaurantAdapter.notifyDataSetChanged();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void addFilterCuisine(String cuisineName) {
        filterCuisineList.add(cuisineName);
        refrestFilteredList();
    }

    public void removeFilterCuisine(String cuisineName) {
        filterCuisineList.remove(cuisineName);
        refrestFilteredList();
    }

}
