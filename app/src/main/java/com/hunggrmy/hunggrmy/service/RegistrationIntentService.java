package com.hunggrmy.hunggrmy.service;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.hunggrmy.hunggrmy.utils.FirebaseFunctions;

public class RegistrationIntentService extends IntentService {

    private static final String TAG = "RegIntentService";


    public RegistrationIntentService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        String token = FirebaseInstanceId.getInstance().getToken();
        Log.e(TAG, "FCM Registration Token: " + token);
        FirebaseFunctions.insertFCMToken(getApplicationContext(),token);
        FirebaseFunctions.insertDeviceUniqueId(getApplicationContext());

    }
}