package com.hunggrmy.hunggrmy.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.hunggrmy.hunggrmy.MapsActivity;
import com.hunggrmy.hunggrmy.MyBookingsActivity;
import com.hunggrmy.hunggrmy.MyOrdersActivity;
import com.hunggrmy.hunggrmy.R;
import com.hunggrmy.hunggrmy.RestaurantDetailActivity;
import com.hunggrmy.hunggrmy.notification.NotificationUtils;
import com.hunggrmy.hunggrmy.notification.NotificationVO;
import com.hunggrmy.hunggrmy.utils.Utility;

import java.util.Map;

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "MyFirebaseMsgingService";
    private static final String TITLE = "title";
    private static final String EMPTY = "";
    private static final String MESSAGE = "message";
    private static final String IMAGE = "image";
    private static final String ACTION = "action";
    private static final String COVERAGE = "coverage";
    private static final String LATLNG = "latlng";
    private static final String RESTAURANTID = "restaurantId";
    private static final String DATA = "data";
    private static final String ACTION_DESTINATION = "action_destination";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        Log.e(TAG, "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.e(TAG, "Message data payload: " + remoteMessage.getData());
            Map<String, String> data = remoteMessage.getData();
            try {
                handleData(data);
            } catch (Exception e) {
                Log.e("Logged {}", e.getMessage());
                e.printStackTrace();
            }

        } else if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
            handleNotification(remoteMessage.getNotification());
        }// Check if message contains a notification payload.

    }

    private void handleNotification(RemoteMessage.Notification RemoteMsgNotification) {
        String message = RemoteMsgNotification.getBody();
        String title = RemoteMsgNotification.getTitle();
        NotificationVO notificationVO = new NotificationVO();
        notificationVO.setTitle(title);
        notificationVO.setMessage(message);

        Intent resultIntent = new Intent(getApplicationContext(), MapsActivity.class);
        NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
        notificationUtils.displayNotification(notificationVO, resultIntent);
        notificationUtils.playNotificationSound();
    }

    private void handleData(Map<String, String> data) {
        String title = data.get(TITLE);
        String message = data.get(MESSAGE);
        String iconUrl = data.get(IMAGE);
        String action = data.get(ACTION);
        String actionDestination = data.get(ACTION_DESTINATION);
        String coverage = data.get(COVERAGE);
        String latlng = data.get(LATLNG);
        String restaurantId = data.get(RESTAURANTID);

        if (actionDestination.equals("BookingPage") || actionDestination.equals("OrderPage")) {
            Log.e("Logged", "Booking or order msg");

            Uri soundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Intent notificationIntent;
            String restId = "";
            String orderIdReceived = "";

            if (actionDestination.equals("BookingPage")) {
                notificationIntent = new Intent(getApplicationContext(), MyBookingsActivity.class);
            } else {
                notificationIntent = new Intent(getApplicationContext(), MyOrdersActivity.class);
            }

            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                    | Intent.FLAG_ACTIVITY_SINGLE_TOP);

            PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0,
                    notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.drawable.ic_launcher)
                    .setContentTitle(title)
                    .setContentText(message)
                    .setAutoCancel(true)
                    .setSound(soundUri)
                    .setContentIntent(pendingIntent);

            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            Notification notification = notificationBuilder.build();

            notification.flags |= Notification.FLAG_AUTO_CANCEL;
            notificationManager.notify((int) (Math.random()), notification);


        } else {

            NotificationVO notificationVO = new NotificationVO();
            notificationVO.setTitle(title);
            notificationVO.setMessage(message);
            notificationVO.setIconUrl(iconUrl);
            notificationVO.setAction(action);
            notificationVO.setActionDestination(actionDestination);
            try {
                notificationVO.setCoverage(coverage);
            } catch (Exception e) {
                e.printStackTrace();
            }
            notificationVO.setLatlng(latlng);
            String[] location = latlng.split(",");
//        if (Utility.isWithinMeters(Float.parseFloat(coverage), Double.parseDouble(location[0].trim()), Double.parseDouble(location[1].trim()), getApplicationContext())) {
            if (Utility.isWithinMeters(Float.parseFloat(coverage), Double.parseDouble(location[0].trim()), Double.parseDouble(location[1].trim()), getApplicationContext())) {
                Log.e("Logged", "Is inside coverage area");
                Intent resultIntent = null;
                if (action.equals("openactivity")) {
                    if (actionDestination.equals("RestaurantDetailActivity")) {
                        resultIntent = new Intent(getApplicationContext(), RestaurantDetailActivity.class);
                        resultIntent.putExtra("restaurantId", restaurantId);
                    } else if (actionDestination.equals("BookingPage")){
                        resultIntent = new Intent(getApplicationContext(), MyBookingsActivity.class);
                    } else {
                        resultIntent = new Intent(getApplicationContext(), MapsActivity.class);
                    }
                } else {
                    resultIntent = new Intent(getApplicationContext(), MapsActivity.class);
                }
                NotificationUtils notificationUtils = new NotificationUtils(this);
                notificationUtils.displayNotification(notificationVO, resultIntent);
                notificationUtils.playNotificationSound();
                Log.e("Logged", "Notification displayed");
            } else {
                Log.e("Logged", "Is not within coverage area");
            }

        }
    }
}