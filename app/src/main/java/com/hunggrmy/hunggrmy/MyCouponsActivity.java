package com.hunggrmy.hunggrmy;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.hunggrmy.hunggrmy.adapters.MyCouponAdapter;
import com.hunggrmy.hunggrmy.models.CouponModel;
import com.hunggrmy.hunggrmy.utils.Constant;

import java.util.ArrayList;

public class MyCouponsActivity extends AppCompatActivity {

    TextView noresultTV;
    RecyclerView recycler_view;
    private ArrayList<CouponModel> dataList = new ArrayList<>();
    private MyCouponAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_coupons);

        recycler_view = (RecyclerView) findViewById(R.id.recycler_view);
        noresultTV = (TextView) findViewById(R.id.noresultTV);

        //All Data List
        adapter = new MyCouponAdapter(dataList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(MyCouponsActivity.this);
        recycler_view.setLayoutManager(mLayoutManager);
        recycler_view.setItemAnimator(new DefaultItemAnimator());
        recycler_view.setAdapter(adapter);

        getAllIds();

    }

    public void getAllIds() {
        try {
            final SharedPreferences pref = getSharedPreferences("HungrrMobile", MODE_PRIVATE);

            FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseUserNode).child(pref.getString(Constant.userId, null)).child(Constant.FirebaseCouponNode).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    dataList.clear();
                    for (DataSnapshot datasnapshot1 :
                            dataSnapshot.getChildren()) {
                        final String imageLink = datasnapshot1.getValue(String.class);
                        final String id = datasnapshot1.getKey();
                        CouponModel couponModel = new CouponModel(imageLink, id);
                        dataList.add(couponModel);
                        adapter.notifyDataSetChanged();
                    }
                    if(dataList.size()<1){
                        noresultTV.setVisibility(View.VISIBLE);
                        recycler_view.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        } catch (Exception e) {

        }
    }
}
