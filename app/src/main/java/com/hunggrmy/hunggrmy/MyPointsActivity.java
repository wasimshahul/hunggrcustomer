package com.hunggrmy.hunggrmy;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.hunggrmy.hunggrmy.utils.Constant;

public class MyPointsActivity extends AppCompatActivity {

    TextView gainedpoints;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_points);

        gainedpoints = (TextView) findViewById(R.id.gainedpoints);

        SharedPreferences pref = getSharedPreferences("HungrrMobile", 0);
        if (pref.getBoolean(Constant.isLoggedIn, Boolean.parseBoolean(null))) {
            try {
                String userId = null;
                try {
                    userId = pref.getString(Constant.userId, null);
                    FirebaseDatabase.getInstance().getReference().child(Constant.FirebaseUserNode).child(userId).child("userRewardPoints").addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            String points = null;
                            try {
                                points = String.valueOf(dataSnapshot.getValue(Long.class));
                            } catch (Exception e) {
                                points = "";
                                e.printStackTrace();
                            }
//                            if(points.isEmpty()){
//                                gainedpoints.setText("0");
//                            } else {
                                gainedpoints.setText(points);

                            if(gainedpoints.getText().toString().isEmpty() || gainedpoints.getText().toString().equals("null")){
                                gainedpoints.setText("0");
                            }
//                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
