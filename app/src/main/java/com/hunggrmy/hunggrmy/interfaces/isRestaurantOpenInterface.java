package com.hunggrmy.hunggrmy.interfaces;

public interface isRestaurantOpenInterface {

    // you can define any parameter as per your requirement
    public void isRestaurantOpenCallBack(boolean isRestaurantOpen);
}
