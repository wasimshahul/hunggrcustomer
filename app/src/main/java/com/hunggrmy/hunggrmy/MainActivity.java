package com.hunggrmy.hunggrmy;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.abdeveloper.library.MultiSelectDialog;
import com.abdeveloper.library.MultiSelectModel;
import com.billing.mobilebillingsystem.main.Utils.LogUtils;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.vision.text.Line;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.hunggrmy.hunggrmy.fragments.CentralListFragment;
import com.hunggrmy.hunggrmy.fragments.MapViewFragment;
import com.hunggrmy.hunggrmy.fragments.NotificationListFragment;
import com.hunggrmy.hunggrmy.service.RegistrationIntentService;
import com.hunggrmy.hunggrmy.utils.Constant;
import com.hunggrmy.hunggrmy.utils.GPSTracker;
import com.hunggrmy.hunggrmy.utils.MyApplication;
import com.hunggrmy.hunggrmy.utils.Utility;

import java.util.ArrayList;

import eightbitlab.com.blurview.BlurView;
import eightbitlab.com.blurview.RenderScriptBlur;

import static com.hunggrmy.hunggrmy.utils.Utility.getAddress;

public class MainActivity extends AppCompatActivity {

    private static TextView locationTV;
    private int PLACE_PICKER_REQUEST = 1;
    private static GPSTracker gpsTracker;
    public static double centralLatitude;
    public static double centralLongitude;
    ImageView gps_Icon, search_img, shop_img;
    private String currentActiveFragment = "";
    public static CheckBox driving_mode_input;
    public static boolean isDriveModeEnabled = false;
    BlurView blurView;

    ImageView filterIV;
    public static ArrayList<String> selectedFilters = new ArrayList<>();
    public static ArrayList<Integer> alreadySelectedFilters = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.maps_rest_content2);

        locationTV = (TextView) findViewById(R.id.locationTV);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        gps_Icon = (ImageView) findViewById(R.id.gps_Icon);
        search_img = (ImageView) findViewById(R.id.search_img);
        shop_img = (ImageView) findViewById(R.id.shop_img);
        filterIV = (ImageView) findViewById(R.id.filterIV);
        driving_mode_input = (CheckBox) findViewById(R.id.driving_mode_input);

        gpsTracker = new GPSTracker(this);
        centralLatitude = gpsTracker.getLatitude();
        centralLongitude = gpsTracker.getLongitude();

        blurView = (BlurView) findViewById(R.id.blurView);

        float radius = 20f;

        View decorView = getWindow().getDecorView();
        //ViewGroup you want to start blur from. Choose root as close to BlurView in hierarchy as possible.
        ViewGroup rootView = (ViewGroup) decorView.findViewById(android.R.id.content);
        //Set drawable to draw in the beginning of each blurred frame (Optional).
        //Can be used in case your layout has a lot of transparent space and your content
        //gets kinda lost after after blur is applied.
        Drawable windowBackground = decorView.getBackground();

        blurView.setupWith(rootView)
                .setFrameClearDrawable(windowBackground)
                .setBlurAlgorithm(new RenderScriptBlur(this))
                .setBlurRadius(radius)
                .setHasFixedTransformationMatrix(true);

        search_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, SearchActivity.class));
            }
        });
        shop_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, ShoppingActivity.class));
            }
        });

        driving_mode_input.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    isDriveModeEnabled = true;
                    MapViewFragment.enableDrivingMode();
                } else {
                    isDriveModeEnabled = false;
                }
            }
        });

        gps_Icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                centralLatitude = gpsTracker.getLatitude();
                centralLongitude = gpsTracker.getLongitude();
                MapViewFragment.setToCurrentLocation();
            }
        });

        locationTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Exception e;
                try {
                    Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY)
                            .build(MainActivity.this);
                    startActivityForResult(intent, PLACE_PICKER_REQUEST);
//                MapsActivity.this.startActivityForResult(new PlacePicker.IntentBuilder().build(MapsActivity.this), MapsActivity.this.PLACE_PICKER_REQUEST);
                    return;
                } catch (GooglePlayServicesRepairableException e2) {
                    e = e2;
                } catch (GooglePlayServicesNotAvailableException e3) {
                    e = e3;
                }
                e.printStackTrace();
            }
        });
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        gps_Icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MapViewFragment.setToCurrentLocation();
            }
        });

        filterIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filter();
            }
        });
        //Initialise

        try {
            changeAddress(centralLatitude, centralLongitude);
        } catch (Exception e) {
            e.printStackTrace();
        }
        currentActiveFragment = "maps";
        loadFragment(new MapViewFragment());

//        showIntro(MainActivity.this);
    }

    public static void stopDrivingMode(){
        isDriveModeEnabled = false;
        driving_mode_input.setChecked(false);
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment fragment;
            switch (item.getItemId()) {
                case R.id.navigation_maps:
                    currentActiveFragment = "maps";
                    loadFragment(new MapViewFragment());
                    return true;
                case R.id.navigation_restaurants:
                    currentActiveFragment = "restaurants";
                    loadFragment(new CentralListFragment());
                    return true;
                case R.id.navigation_notification:
                    currentActiveFragment = "notification";
                    loadFragment(new NotificationListFragment());
                    return true;
                case R.id.navigation_cart:
                    currentActiveFragment = "cart";
                    if (Utility.checkIfUserIsLoggedIn(MyApplication.getAppContext())) {
                        startActivity(new Intent(MainActivity.this, CartActivity.class));

                    } else {
                        startActivity(new Intent(MainActivity.this, LoginActivity.class));
                        Toast.makeText(MainActivity.this, "Please log in to continue", Toast.LENGTH_SHORT).show();
                    }
                    return true;
                case R.id.navigation_profile:
                    if (Utility.checkIfUserIsLoggedIn(MyApplication.getAppContext())) {
                        startActivity(new Intent(MainActivity.this, ProfileMenuActivity.class));

                    } else {
                        startActivity(new Intent(MainActivity.this, LoginActivity.class));
                        Toast.makeText(MainActivity.this, "Please log in to continue", Toast.LENGTH_SHORT).show();
                    }                    return true;
            }
            return false;
        }
    };

    private void loadFragment(Fragment fragment) {
        // load fragment
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == this.PLACE_PICKER_REQUEST && resultCode == -1) {
            Place place = PlacePicker.getPlace(data, this);
            StringBuilder stBuilder = new StringBuilder();
            String placename = String.format("%s", new Object[]{place.getName()});
            String latitude = String.valueOf(place.getLatLng().latitude);
            String longitude = String.valueOf(place.getLatLng().longitude);
            String address = String.format("%s", new Object[]{place.getAddress()});
            centralLatitude = Double.valueOf(latitude);
            centralLongitude = Double.valueOf(longitude);
            locationTV.setText(address);
            if (currentActiveFragment.equals("maps")) {
                MapViewFragment.switchToLocation(centralLatitude, centralLongitude);
            } else {
                loadFragment(new CentralListFragment());
            }
        }
    }

    public static void changeAddress(double latitude, double longitude) {
        locationTV.setText(getAddress(latitude, longitude, MyApplication.getAppContext()));
        centralLatitude = latitude;
        centralLongitude = longitude;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.navigation);
        int seletedItemId = bottomNavigationView.getSelectedItemId();
        if (R.id.navigation_maps != seletedItemId) {
            loadFragment(new MapViewFragment());
            bottomNavigationView.setSelectedItemId(R.id.navigation_maps);
        } else {
            super.onBackPressed();
            finish();
        }
    }

    public void showIntro(final Context context) {
        final BottomSheetDialog dialog = new BottomSheetDialog(MainActivity.this);
        dialog.setContentView(R.layout.webview);
        final WebView webView = (WebView) dialog.findViewById(R.id.webView);
        final LinearLayout buttonLL = (LinearLayout) dialog.findViewById(R.id.buttonLL);

        webView.getSettings().setJavaScriptEnabled(true);
        webView.setHorizontalScrollBarEnabled(false);
        webView.clearCache(true);

        FirebaseDatabase.getInstance().getReference().child("AppControls").child("introURL").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String url = dataSnapshot.getValue(String.class);
                if (getApplicationContext().getSharedPreferences("HungrrMobile", 0).getString(Constant.introURL, ".").equals(url)) {

                } else {
                    SharedPreferences.Editor editor = getApplicationContext().getSharedPreferences("HungrrMobile", 0).edit();
                    editor.putString(Constant.introURL, url);
                    editor.commit();
                    webView.loadUrl(url);
                    dialog.show();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        buttonLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

    }

    public void filter(){

        //Maps Filter
        ArrayList<MultiSelectModel> filters = new ArrayList<>();
        filters.add(new MultiSelectModel(1, "wasim"));
        filters.add(new MultiSelectModel(2, "fast"));
        filters.add(new MultiSelectModel(3, "kfc"));
        filters.add(new MultiSelectModel(4, "mess"));
        filters.add(new MultiSelectModel(5, "biriyani"));


        MultiSelectDialog multiSelectDialog = new MultiSelectDialog()
                .title("Select cuisines") //setting title for dialog
                .titleSize(25)
                .positiveText("Done")
                .negativeText("Clear")
                .setMinSelectionLimit(1) //you can set minimum checkbox selection limit (Optional)
                .setMaxSelectionLimit(filters.size()) //you can set maximum checkbox selection limit (Optional)
                .preSelectIDsList(alreadySelectedFilters) //List of ids that you need to be selected
                .multiSelectList(filters) // the multi select model list with ids and name
                .onSubmit(new MultiSelectDialog.SubmitCallbackListener() {
                    @Override
                    public void onSelected(ArrayList<Integer> selectedIds, ArrayList<String> selectedNames, String dataString) {
                        //will return list of selected IDS
                        selectedFilters.clear();
//                        alreadySelectedFilters.clear();
                        alreadySelectedFilters = (ArrayList<Integer>) selectedIds.clone();
                        for (int i = 0; i < selectedIds.size(); i++) {
                            try {
                                selectedFilters.add(selectedNames.get(i));
                                alreadySelectedFilters.add(selectedIds.get(i));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            LogUtils.Log("selected ids : "+selectedIds.get(i));
                            if(i==selectedIds.size()-1){
                                for (int iv :
                                        alreadySelectedFilters) {
                                    LogUtils.Log("selected ids : "+iv);
                                }
                                MapViewFragment.reloadMap();
                            }
                        }

                    }

                    @Override
                    public void onCancel() {
                        selectedFilters.clear();
                        MapViewFragment.reloadMap();
                    }


                });

        multiSelectDialog.show(getSupportFragmentManager(), "multiSelectDialog");
    }

}
